﻿using MatixDealer.Models;
using MatixDealer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

using SampleWebApplication1.Utils;
using static SampleWebApplication1.Utils.Utilities;

namespace MatixDealer.Controllers
{   
    public class RegistrationController : ControllerBase
    {
        private readonly IOptions<AppConfig> appSettings;
        matixContext db;
        RegistrationService _registrationServices;
        IWebHostEnvironment env;
        private IConfiguration _config;
        private readonly ILogger<RegistrationController> _logger;
        Common cmn;

        public RegistrationController(IOptions<AppConfig> appConfig, matixContext dbContext, IWebHostEnvironment env, IConfiguration config, ILogger<RegistrationController> logger)
        {
            appSettings = appConfig;
            db = dbContext;
            _registrationServices = new RegistrationService(dbContext, logger, appSettings);
            this.env = env;
            _config = config;
            _logger = logger;
            cmn = new Common(dbContext, appConfig);
        }

        [HttpPost]
        [Route("api/Matix/InsertOrUpdateUser")]
        public async Task<object> InsertOrUpdateUser([FromBody]UserRegister userRegister)
        {
            try
            {
                if(userRegister != null)
                {
                    var response = await Task.FromResult(_registrationServices.InsertOrUpdateUser(userRegister));
                    if (response != null)
                    {
                        Response.StatusCode = 200;
                        return new
                        {
                            status = response.isSuccess,
                            time = DateTime.Now,
                            data = response.data,
                        };
                    }
                }
                return new
                {
                    status = appSettings.Value.errorMessage,
                    statusCode = 400,
                    time = DateTime.Now,
                };
            }
            catch(Exception ex)
            {
                Response.StatusCode = 500;
                return new { status = appSettings.Value.errorMessage, time = DateTime.Now, data = ex.Message, data2 = ex.StackTrace };
            }
        }

        [HttpPost]
        [Route("api/Matix/AssighManager")]
        public async Task<object> AssighManager([FromBody]AssignManager assignManager)
        {
            try
            {
                if (assignManager.managerId != null && assignManager.assighId.Count != 0)
                {
                    var response = await Task.FromResult(_registrationServices.AssighManager(assignManager));
                    if (response != null)
                    {
                        Response.StatusCode = 200;
                        return new
                        {
                            status = response.isSuccess,
                            time = DateTime.Now,
                            data = response.data,
                        };
                    }
                }
                return new
                {
                    status = appSettings.Value.errorMessage,
                    statusCode = 400,
                    time = DateTime.Now,
                };
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return new { status = appSettings.Value.errorMessage, time = DateTime.Now, data = ex.Message, data2 = ex.StackTrace };
            }
        }

        [HttpPost]
        [Route("api/Matix/UpdateUserStatus")]
        public async Task<object> UpdateUserStatus(int id , string status)
        {
            try
            {
                if(id == 0 && status == null)
                {
                    return new
                    {
                        status = appSettings.Value.errorMessage,
                        statusCode = 400,
                        time = DateTime.Now,
                    };
                }
                else
                {
                    var response = await Task.FromResult(_registrationServices.UpdateUserStatus(id,status));
                    if (response != null)
                    {
                        Response.StatusCode = 200;
                        return new
                        {
                            status = response.isSuccess,
                            time = DateTime.Now,
                            data = response.data,
                        };
                    }
                    return new
                    {
                        status = appSettings.Value.errorMessage,
                        statusCode = 400,
                        time = DateTime.Now,
                    };
                }
            }
            catch(Exception ex)
            {
                Response.StatusCode = 500;
                return new { status = appSettings.Value.errorMessage, time = DateTime.Now, data = ex.Message, data2 = ex.StackTrace };
            }
        }


        [HttpGet]
        [Route("api/Matix/GetDesignationDetail")]
        public async Task<object> GetDesignationDetail()
        {
            try
            {
                var response = await Task.FromResult(_registrationServices.GetDesignationDetail());
                Response.StatusCode = 200;
                return new
                {
                    status = response.isSuccess,
                    time = DateTime.Now,
                    data = response.data,
                };
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return new { status = appSettings.Value.errorMessage, time = DateTime.Now, data = ex.Message, data2 = ex.StackTrace };
            }
        }

    }
}
