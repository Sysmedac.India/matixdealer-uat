﻿using MatixDealer.Models;
using MatixDealer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using SampleWebApplication1.Utils;

namespace MatixDealer.Controllers
{
    public class DocumentController : ControllerBase
    {
        private readonly IOptions<AppConfig> appSettings;
        matixContext db;
        DocumentService _documentServices;
        IWebHostEnvironment env;
        private IConfiguration _config;
        private readonly ILogger<DocumentController> _logger;
        Common cmn;

        public DocumentController(IOptions<AppConfig> appConfig, matixContext dbContext, IWebHostEnvironment env, IConfiguration config, ILogger<DocumentController> logger)
        {
            appSettings = appConfig;
            db = dbContext;
            _documentServices = new DocumentService(dbContext, logger, appSettings);
            this.env = env;
            _config = config;
            _logger = logger;
            cmn = new Common(dbContext, appConfig);
        }

        [HttpPost]
        [Route("api/Matix/InsertOrUpdateDocuments")]
        public async Task<object> InsertOrUpdateDocuments([FromBody] Documents documents)
        {
            try
            {
                if (documents != null)
                {                    
                    var response = await Task.FromResult(_documentServices.InsertOrUpdateDocuments(documents));
                    if (response != null)
                    {
                        Response.StatusCode = 200;
                        return new
                        {
                            status = response.isSuccess,
                            time = DateTime.Now,
                            data = response.data,
                        };
                    }
                }
                return new
                {
                    status = appSettings.Value.errorMessage,
                    statusCode = 400,
                    time = DateTime.Now,
                };
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return new { status = appSettings.Value.errorMessage, time = DateTime.Now, data = ex.Message, data2 = ex.StackTrace };
            }
        }

        [HttpPost]
        [Route("api/Matix/CommonFileUpload")]
        public async Task<Object> CommonFileUpload()
        {
            try
            {
                var files = Request.Form.Files;
                var file = files[0];

                string physicalPath = "";
                string FileName = "";
                string fileName = "";

                Guid gid = Guid.NewGuid();
                fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.ToString();
                FileName = gid.ToString() + "_" + fileName;
                string replacefilename = FileName.Replace(" ", "");
                replacefilename = replacefilename.Replace("(", "").Replace(")", "");
                var dir = "Uploads";

                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                physicalPath = Path.Combine(Directory.GetCurrentDirectory(), dir, replacefilename);

                string toRemove = "/app";

                string result = physicalPath.Replace(toRemove, "");

                if (file.Length > 0)
                {
                    using (var stream = new FileStream(physicalPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
                string path= "http://4.224.241.132:6435/MatixUploads/"+ replacefilename; //DEV
                //string path = "http://172.16.45.55:6435/Uploads/" + replacefilename; //UAT


                return new { status = "Success", statusCode = 200, time = DateTime.Now, data = path };
            }
            catch (Exception ex)
            {
                cmn.LogToFile("API Name : api/FileUpload/CommonFileUpload | " + "Message : " + ex.Message);

                Response.StatusCode = 500;
                return new { status = appSettings.Value.errorMessage, time = DateTime.Now, data = ex.Message, data2 = ex.StackTrace };
            }

        }

        [HttpGet]
        [Route("api/Matix/GetAll_Document_Details")]
        public async Task<object> GetAll_Document_Details(long? DealerId, long? ManagerId, long? StateManagerId)
        {
            try
            {
                var response = _documentServices.GetAllDocumentDetails(DealerId, ManagerId, StateManagerId);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

    }
}
