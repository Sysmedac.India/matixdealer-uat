﻿using MatixDealer.Models;
using MatixDealer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SampleWebApplication1.Utils;

namespace MatixDealer.Controllers
{    
    public class AnnouncementController : ControllerBase
    {
        private readonly IOptions<AppConfig> appSettings;
        matixContext db;
        AnnouncementService _announcementServices;
        IWebHostEnvironment env;
        private IConfiguration _config;
        private readonly ILogger<RegistrationController> _logger;
        Common cmn;

        public AnnouncementController(IOptions<AppConfig> appConfig, matixContext dbContext, IWebHostEnvironment env, IConfiguration config, ILogger<RegistrationController> logger)
        {
            appSettings = appConfig;
            db = dbContext;
            _announcementServices = new AnnouncementService(dbContext, logger, appSettings);
            this.env = env;
            _config = config;
            _logger = logger;
            cmn = new Common(dbContext, appConfig);
        }

        [HttpPost]
        [Route("api/Matix/InsertOrUpdateAnnouncement")]

        public async Task<object> InsertOrUpdateAnnouncement([FromBody]Announcements announcements)
        {
            try
            {
                var response = await Task.FromResult(_announcementServices.InsertOrUpdateAnnouncement(announcements));
                if (response != null)
                {
                    Response.StatusCode = 200;
                    return new
                    {
                        status = response.isSuccess,
                        time = DateTime.Now,
                        data = response.data,
                    };
                }
                    return new
                    {
                        status = appSettings.Value.errorMessage,
                        statusCode = 400,
                        time = DateTime.Now,
                    };  
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return new { status = appSettings.Value.errorMessage, time = DateTime.Now, data = ex.Message, data2 = ex.StackTrace };
            }
        }
    }
}
