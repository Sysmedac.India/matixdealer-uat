﻿using MatixDealer.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
//using SapNwRfc;
//using SapNwRfc.Parameters;
using SampleWebApplication1.Utils;
using System.Net.Http.Headers;
using System.IO;
using System.Net;
using System.Net.Mime;

namespace MatixDealer.Controllers
{
    public class SavicApiController : ControllerBase
    {
        matixContext db = new matixContext();

        [HttpGet]
        [Route("api/Matix/GetAll_Dealers_FromSAVIC")]
        public async Task<object> GetAll_Dealers_FromSAVIC()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response1 = await client.GetAsync("http://matixeccqas.panlocal.mfcl:8000/sap/bc/abap/zrest_api/matrix/?sap-client=220&ID=CM");
                    string responseBody = await response1.Content.ReadAsStringAsync();
                    //JObject json = JObject.Parse(responseBody);
                    var vn = responseBody.ToArray();
                    var json1 = JsonConvert.SerializeObject(responseBody);
                    var deserialized = JsonConvert.DeserializeObject<IEnumerable<Dealer>>(responseBody);
                    foreach (Dealer vDeal in deserialized)
                    {
                        var vCustomer = vDeal.customer;
                        var check = db.Users.Where(x => x.DealerCode == vCustomer).FirstOrDefault();
                        if (check != null)
                        {//Update
                        }
                        else
                        {//Insert
                            User user = new User();
                            user.ConfigUserRoleId = 4;
                            user.MasterDesignationId = 4;
                            user.EmployeeCode = vDeal.customer;
                            user.FirstName = vDeal.name;
                            user.LastName = "";
                            user.MobileNumber = "";
                            user.EmailId = vDeal.emailMaint;
                            user.Address = vDeal.street;
                            user.CountryName = "India";
                            user.CountryCode = vDeal.timeZone;
                            user.StateName = vDeal.description;
                            user.StateCode = vDeal.customer;
                            user.City = vDeal.city;
                            user.PostalCode = vDeal.postalCode;
                            user.Status = "Active";
                            user.LoginUserName = vDeal.customer;
                            user.LoginPassword = vDeal.customer;
                            user.DealerCode = vDeal.customer;
                            user.CreateDate = DateTime.Now;
                            user.CstNo = vDeal.cstNo;
                            user.LstNo = vDeal.lstNo;
                            user.ServiceTaxRegnNo = vDeal.serviceTaxRegnNo;
                            user.Pan = vDeal.pan;
                            user.IfmsCode = vDeal.roomNumber;

                            db.Users.Add(user);
                            db.SaveChanges();
                        }
                    }
                    if (response1.IsSuccessStatusCode)
                    {
                        return new
                        {
                            status = true,
                            statusCode = 200,
                            time = DateTime.Now,
                            data = responseBody,
                        };
                    }

                }

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = "",
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }
        [HttpGet]
        [Route("api/Matix/GetAll_Sales_FromSAVIC")]
        public async Task<object> GetAll_Sales_FromSAVIC()
        {
            try
            {
                DateTime now = DateTime.Now;
                DateTime dateOnly = now.Date;
                var nowdate = dateOnly.ToString("dd.MM.yyyy");

                var predate = dateOnly.AddDays(-1).ToString("dd.MM.yyyy");

                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response1 = await client.GetAsync("http://matixeccqas.panlocal.mfcl:8000/sap/bc/abap/zrest_api/matrix/?sap-client=220&ID=LR&FrmDate=" + predate + "&ToDate=" + nowdate);
                    //  HttpResponseMessage response1 = await client.GetAsync("http://matixeccqas.panlocal.mfcl:8000/sap/bc/abap/zrest_api/matrix/?sap-client=220&ID=LR&FrmDate=01.03.2023&ToDate=31.03.2023");

                    string responseBody = await response1.Content.ReadAsStringAsync();
                    //JObject json = JObject.Parse(responseBody);
                    var vn = responseBody.ToArray();
                    var json1 = JsonConvert.SerializeObject(responseBody);
                    if (responseBody.Contains("No Data found for this period"))
                    {

                    }
                    else
                    {
                        var deserialized = JsonConvert.DeserializeObject<IEnumerable<SalesDetails1>>(responseBody);
                        foreach (SalesDetails1 vSales in deserialized)
                        {
                            var vInvoiceNumber = vSales.invoiceNumber;
                            var check = db.Sales.Where(x => x.InvoiceNumber == vInvoiceNumber).FirstOrDefault();
                            if (check != null)
                            {//Update

                                var _vCustNo = vSales.soldToCode;
                                var _checkUser = db.Users.Where(x => x.DealerCode == _vCustNo).FirstOrDefault();
                                if (_checkUser != null)
                                {

                                    check.InvoiceNumber = vSales.invoiceNumber;
                                    check.InvoiceDate = Convert.ToDateTime(vSales.invoiceDocDate);
                                    check.InvoiceNumber = vSales.invoiceNumber;
                                    check.UserId = _checkUser.Id;
                                    check.StateManagerReadStatus = "NotRead";
                                    check.ManagerReadStatus = "NotRead";
                                    check.DealerReadStatus = "NotRead";
                                    check.ProductCode = vSales.productCode;
                                    check.ProductDesc = vSales.productDesc;
                                    check.BatchNo = vSales.batchNo;
                                    check.ObdNo = vSales.obdNo;
                                    check.ActualGiQuantity = Convert.ToDecimal(vSales.actualGiQuantity);
                                    check.ZgeNo = vSales.zgeNo;
                                    check.SupplyingPlantCode = vSales.supplyingPlantCode;
                                    check.SupplyingPlantName = vSales.supplyingPlantName;
                                    check.SupplyingPlantState = vSales.supplyingPlantState;
                                    check.TptName = vSales.tptName;
                                    check.LrNo = vSales.lrNo;
                                    if (vSales.lrDate != "")
                                        check.LrDate = Convert.ToDateTime(vSales.lrDate);
                                    check.Salescol = vSales.salescol;
                                    check.TruckNo = vSales.truckNo;
                                    check.TruckCapacity = vSales.truckCapacity;
                                    check.SoldToCode = vSales.soldToCode;
                                    check.SoldToName = vSales.soldToName;
                                    check.SoldToLocation = vSales.soldToLocation;
                                    check.SoldToBlock = vSales.soldToBlock;
                                    check.SoldToDistrict = vSales.soldToDistrict;
                                    check.ShipToCode = vSales.shipToCode;
                                    check.ShipToName = vSales.shipToName;
                                    check.ShipToLocation = vSales.shipToLocation;
                                    check.ShipToBlock = vSales.shipToBlock;
                                    check.ShipToDistrict = vSales.shipToDistrict;
                                    check.InvoiceValueWithoutGst = vSales.InvoiceValueWithoutGst;
                                    check.InvoiceValueWithGst = vSales.InvoiceValueWithGst;
                                    check.Gst = vSales.Gst;
                                    check.MaterialGroup = vSales.MaterialGroup;
                                    db.Sales.Update(check);
                                    db.SaveChanges();
                                }
                            }
                            else
                            {//Insert
                                var vCustNo = vSales.soldToCode;
                                var checkUser = db.Users.Where(x => x.DealerCode == vCustNo).FirstOrDefault();
                                if (checkUser != null)
                                {
                                    Sale vSalesDe = new Sale();
                                    vSalesDe.InvoiceNumber = vSales.invoiceNumber;
                                    vSalesDe.InvoiceDate = Convert.ToDateTime(vSales.invoiceDocDate);
                                    vSalesDe.InvoiceNumber = vSales.invoiceNumber;
                                    vSalesDe.UserId = checkUser.Id;
                                    vSalesDe.StateManagerReadStatus = "NotRead";
                                    vSalesDe.ManagerReadStatus = "NotRead";
                                    vSalesDe.DealerReadStatus = "NotRead";
                                    vSalesDe.ProductCode = vSales.productCode;
                                    vSalesDe.ProductDesc = vSales.productDesc;
                                    vSalesDe.BatchNo = vSales.batchNo;
                                    vSalesDe.ObdNo = vSales.obdNo;
                                    vSalesDe.ActualGiQuantity = Convert.ToDecimal(vSales.actualGiQuantity);
                                    vSalesDe.ZgeNo = vSales.zgeNo;
                                    vSalesDe.SupplyingPlantCode = vSales.supplyingPlantCode;
                                    vSalesDe.SupplyingPlantName = vSales.supplyingPlantName;
                                    vSalesDe.SupplyingPlantState = vSales.supplyingPlantState;
                                    vSalesDe.TptName = vSales.tptName;
                                    vSalesDe.LrNo = vSales.lrNo;
                                    if (vSales.lrDate != "")
                                        vSalesDe.LrDate = Convert.ToDateTime(vSales.lrDate);
                                    vSalesDe.Salescol = vSales.salescol;
                                    vSalesDe.TruckNo = vSales.truckNo;
                                    vSalesDe.TruckCapacity = vSales.truckCapacity;
                                    vSalesDe.SoldToCode = vSales.soldToCode;
                                    vSalesDe.SoldToName = vSales.soldToName;
                                    vSalesDe.SoldToLocation = vSales.soldToLocation;
                                    vSalesDe.SoldToBlock = vSales.soldToBlock;
                                    vSalesDe.SoldToDistrict = vSales.soldToDistrict;
                                    vSalesDe.ShipToCode = vSales.shipToCode;
                                    vSalesDe.ShipToName = vSales.shipToName;
                                    vSalesDe.ShipToLocation = vSales.shipToLocation;
                                    vSalesDe.ShipToBlock = vSales.shipToBlock;
                                    vSalesDe.ShipToDistrict = vSales.shipToDistrict;
                                    vSalesDe.InvoiceValueWithoutGst = vSales.InvoiceValueWithoutGst;
                                    vSalesDe.InvoiceValueWithGst = vSales.InvoiceValueWithGst;
                                    vSalesDe.Gst = vSales.Gst;
                                    vSalesDe.MaterialGroup = vSales.MaterialGroup;
                                    db.Sales.Add(vSalesDe);
                                    db.SaveChanges();
                                }

                            }
                        }

                    }
                    if (response1.IsSuccessStatusCode)
                    {
                        return new
                        {
                            status = true,
                            statusCode = 200,
                            time = DateTime.Now,
                            data = responseBody,
                        };
                    }

                }

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = "",
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/connectSAP")]
        public async Task<object> connectSAP()
        {
            try
            {
                //using (var conn = new RfcConnection("Server=172.16.45.136;lang=en;user=savic_01;pwd=savic@123"))
                //{
                //    conn.Open();
                //    using (var func = conn.CallRfcFunction("BAPI_COMPANYCODE_GETLIST"))
                //    {
                //        func.Invoke();
                //    }
                //}


                //RfcConfigParameters rfcConfigParameters = new RfcConfigParameters(1234);

                //HttpContext context = HttpContext;               
                //var parameters = new RfcConfigParameters();

                //parameters.Add(RfcConfigParameters.AppServerHost, "172.16.45.136");
                //parameters.Add(RfcConfigParameters.Client, "220");
                //parameters.Add(RfcConfigParameters.User, "savic_01");
                //parameters.Add(RfcConfigParameters.Password, "savic@123");
                //parameters.Add(RfcConfigParameters.SystemNumber, "00");
                //parameters.Add(RfcConfigParameters.Language, "en");

                //var parameters = new rfc
                //{
                //    { "ASHOST", "172.16.45.136" },
                //    { "SYSNR", "CM" },
                //    { "USER", "savic_01" },
                //    { "PASSWD", "savic@123" },
                //    { "CLIENT", "220" },
                //    // Add any other necessary parameters
                //};


                //  var destination = RfcDestinationManager.GetDestination(parameters);

                //    // Connect to SAP
                //    destination.Ping();

                Console.WriteLine("Connected to SAP successfully.");
                // }
                // SAP connection string
                // string connectionString = "AppServerHost=172.16.45.136; SystemNumber=00; User=savic_01; Password=savic@123; Client=220; Language=EN; PoolSize=5; Trace=8";


                return new
                {
                    status = true,
                    statusCode = 200,
                    time = DateTime.Now,
                    data = "",
                };
            }
            catch (Exception ex)
            {
                // Handling exceptions
                return new
                {
                    status = false,
                    statusCode = 500,
                    time = DateTime.Now,
                    data = ex.Message + ex.InnerException,
                };
            }
        }
        //[HttpGet]
        //[Route("api/Matix/downloadInvoiceLoc")]
        //public async Task<object> downloadInvoiceLoc()
        //{
        //    try
        //    {
        //        //string url = "https://localhost:7260/api/Matix/downloadInvoice";
        //        string url = "http://localhost:6443/api/Matix/downloadInvoice";
        //        using (HttpClient client = new HttpClient())
        //        {
        //            using (HttpResponseMessage response = await client.GetAsync(url))
        //            {
        //            }
        //        }
        //                return new
        //        {

        //            status = true,
        //            statusCode = 200,
        //            time = DateTime.Now,
        //            data = "",
        //        };
        //    }
        //    catch (Exception ex) {
        //        return new
        //        {

        //            status = false,
        //            statusCode = 500,
        //            time = DateTime.Now,
        //            data = ex.ToString(),
        //        };
        //    }
        //}

        [HttpGet]
        [Route("api/Matix/downloadInvoice")]
        public async Task<FileResult> downloadInvoice(string SapDealerNo,string FromDate, string ToDate)
        {
            try
            {
                string folderName = "AccountStatement";
                string newPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), folderName);
                Guid gid = Guid.NewGuid();
                var name = gid.ToString();
                name = name + ".pdf";
                newPath = System.IO.Path.Combine(newPath, name);

                //string url = "http://matixeccqas.panlocal.mfcl:8000/sap/bc/abap/zrest_api/matrix/?sap-client=220&ID=DPDF&SapDealerNo=0000000087&FrmDate=10.05.2023&ToDate=10.10.2023";
                string url = "http://matixeccqas.panlocal.mfcl:8000/sap/bc/abap/zrest_api/matrix/?sap-client=220&ID=DPDF&SapDealerNo="+SapDealerNo+"&FrmDate="+ FromDate + "&ToDate="+ ToDate;
                using (HttpClient client = new HttpClient())
                {
                    using (HttpResponseMessage response = await client.GetAsync(url))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            using (Stream contentStream = await response.Content.ReadAsStreamAsync())
                            {
                                using (FileStream fileStream = System.IO.File.Create(newPath))
                                {
                                    await contentStream.CopyToAsync(fileStream);
                                    fileStream.Close();
                                }
                                contentStream.Close();
                            }                           
                        }
                        else
                        {
                           
                        }
                    }
                }

                string contentType = "application/octet-stream";
                byte[] bytes = System.IO.File.ReadAllBytes(newPath);
                if (System.IO.File.Exists(newPath))
                {
                    bytes = System.IO.File.ReadAllBytes(newPath);
                }
                else
                {
                    return null;
                }

                return File(bytes, contentType, name);
                //return new
                //{

                //    status = true,
                //    statusCode = 200,
                //    time = DateTime.Now,
                //    data = "",
                //};
            }
            catch (Exception ex) {
                return null;
            }
        }
    }
}
