﻿using MatixDealer.Models;
using MatixDealer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SampleWebApplication1.Utils;
using static SampleWebApplication1.Utils.Entity;

namespace MatixDealer.Controllers
{
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IOptions<AppConfig> appSettings;
        matixContext db;
        AdminServices _sampleservices;
        IWebHostEnvironment env;
        private IConfiguration _config;
        private readonly ILogger<AdminController> _logger;
        Common cmn;

        public AdminController(IOptions<AppConfig> appConfig, matixContext dbContext, IWebHostEnvironment env, IConfiguration config, ILogger<AdminController> logger)
        {
            appSettings = appConfig;
            db = dbContext;
            _sampleservices = new AdminServices(dbContext, logger, appSettings);
            this.env = env;
            _config = config;
            _logger = logger;
            cmn = new Common(dbContext, appConfig);
        }

        [HttpGet]
        [Route("api/Matix/GetAll_Users_Details")]
        public async Task<object> GetAll_Users_Details(string? searchText, string? CountryName, string? CityName, string? Status, long UserRoleId)
        {
            try
            {
                var response = _sampleservices.GetAllStateManagerDetails(searchText, CountryName, CityName, Status, UserRoleId);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch(Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/Get_User_ByUserId")]
        public async Task<object> Get_User_ByUserId(long? UserId)
        {
            try
            {
                var response = _sampleservices.GetUserDetails(UserId);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/GetAllManagers_ByUserTeams")]
        public async Task<object> GetAllManagers_using_userTeams(long? UserId, long? StateManagerId)
        {
            try
            {
                var response = _sampleservices.GetAllManagers_UserTeams(UserId, StateManagerId);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/GetAllAssignedUsersById")]
        public async Task<object> GetAllAssignedUsersById(long UserRoleId,long UserId)
        {
            try
            {
                var response = _sampleservices.GetAllUnAssigendDetails(UserRoleId,UserId);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/GetAnnouncementMessage")]
        public async Task<object> GetAnnouncementMessage(long UserRoleId, long UserId)
        {
            try
            {
                var response = _sampleservices.GetAnnouncementMessage(UserRoleId, UserId);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/GetAllInvoiceNo")]
        public async Task<object> GetAllInvoiceNo(long UserId)
        {
            try
            {
                var response = _sampleservices.GetAllInvoiceNumber(UserId);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/GetAllAnnouncement")]
        public async Task<object> Get_AllAnnouncement(DateTime? fromDate, DateTime? toDate)
        {
            try
            {
                var response = _sampleservices.GetAllAnnouncement(fromDate, toDate);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }


        [HttpGet]
        [Route("api/Matix/Get_InvoiceDetails_ForDealers")]
        public async Task<object> Get_InvoiceDetails_ForDealers(string? searchText, string? CountryName, string? CityName, long? ManagerId, string? InvoiceNumber, long? UseriD)
        {
            try
            {
                var response = _sampleservices.GetInvoiceDetailsForDealers(searchText, CountryName, CityName, ManagerId, InvoiceNumber, UseriD);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/DashBoard")]
        public async Task<object> DashBoardApi(DateTime? selectRange, DateTime? fromDate, DateTime? toDate, long? DealerId, long? ManagerId, long? StateManagerId)
        {
            try
            {
                var response = _sampleservices.MatixDashboard(selectRange, fromDate, toDate, DealerId, ManagerId, StateManagerId);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpDelete]
        [Route("api/Matix/Delete_User")]
        public async Task<object> Delete_User(long UserId)
        {
            try
            {
                var response = _sampleservices.DeleteUser(UserId);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpPost]
        [Route("api/Matix/Delete_Multiple_User")]
        public async Task<object> Delete_Multiple_User(AssignManager request)
        {
            try
            {
                var response = _sampleservices.DeleteMultipleUser(request);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/GetAllUserDetailsBy_UserTeams")]
        public async Task<object> GetAllUserDetailsBy_UserTeams(long UserId)
        {
            try
            {
                var response = _sampleservices.GetAllUserDetailsBy_UserTeams(UserId);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/GetAllDealersBy_UserTeams")]
        public async Task<object> GetAllDealersBy_UserTeams()
        {
            try
            {
                var response = _sampleservices.GetAllDealers_UserTeams();

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/GetAllDealers")]
        public async Task<object> GetAllDealers(long UserRoleId, long? UserId, long? ManagerId, long? StateManagerId)
        {
            try
            {
                var response = _sampleservices.GetAllDealers(UserRoleId, UserId, ManagerId, StateManagerId);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/GetAllSalesDetails")]
        public async Task<object> GetAllSalesDetails(long? DealerId, long? ManagerId, long? StateManagerId, DateTime? selectRange, DateTime? fromDate, DateTime? toDate, string? MaterialGroup)
        {
            try
            {
                var response = _sampleservices.GetAllSalesDetails(DealerId, ManagerId, StateManagerId, selectRange, fromDate, toDate, MaterialGroup);

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }

        [HttpGet]
        [Route("api/Matix/GetAllSalesDetailsByMaterialGroup")]
        public async Task<object> GetAllSalesDetailsByMaterialGroup()
        {
            try
            {
                var response = _sampleservices.GetAllSalesDetailsByMaterialGroup();

                return new
                {
                    status = true,
                    time = DateTime.Now,
                    data = response.Result,
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    status = false,
                    time = DateTime.Now,
                    data = ex.StackTrace + ex.Message,
                };
            }
        }
    }
}
