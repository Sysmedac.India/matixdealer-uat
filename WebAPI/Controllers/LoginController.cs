﻿using MatixDealer.Models;
using MatixDealer.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using SampleWebApplication1.Utils;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using static SampleWebApplication1.Utils.Entity;

namespace MatixDealer.Controllers
{
    public class LoginController : ControllerBase
    {
        private readonly IOptions<AppConfig> appSettings;
        private readonly Utilities _utilities;
        matixContext db;
        LoginService _loginServices;
        IWebHostEnvironment env;
        private IConfiguration _config;
        private readonly ILogger<LoginController> _logger;
        Common cmn;

        public LoginController(IOptions<AppConfig> appConfig, matixContext dbContext, IWebHostEnvironment env, IConfiguration config, ILogger<LoginController> logger)
        {
            appSettings = appConfig;
            db = dbContext;
            _loginServices = new LoginService(dbContext, logger, appSettings,config);
            this.env = env;
            _config = config;
            _logger = logger;
            cmn = new Common(dbContext, appConfig);
        }

        //Niyas 17-10-2023 
        [HttpPost]
        [Route("api/Matix/LoginUser")]
        public async Task<object> LoginUser(UserLogin userLogin)
        {
            try
            {
                if (userLogin != null)
                {
                    var response = await Task.FromResult(_loginServices.LoginUser(userLogin));
                    if (response.isSuccess == true)
                    {                     
                        //var token = GenerateJSONWebToken(userLogin,Convert.ToInt64(response.data));
                        Response.StatusCode = 200;
                        return new
                        {
                            status = response.isSuccess,
                            time = DateTime.Now,
                            data = response,
                        };
                    }
                }
                return new
                {
                    status = appSettings.Value.errorMessage,
                    statusCode = 400,
                    time = DateTime.Now,
                };
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return new { status = appSettings.Value.errorMessage, time = DateTime.Now, data = ex.Message, data2 = ex.StackTrace };
            }
        }

        [HttpGet]
        [Route("api/Matix/GetUserDetails")]
        public async Task<object> GetUserDetails()
        {
            try
            {
                string Token = Request.Headers.GetCommaSeparatedValues("token").FirstOrDefault();
                var _token = VerifyToken(Token);
                if (_token.isSuccess == false)
                {
                    Response.StatusCode = 401;
                    return new { status = appSettings.Value.errorMessage, time = DateTime.Now, data = _token.message };
                }

                var response = await Task.FromResult(_loginServices.GetUserDetails());
                Response.StatusCode = 200;
                return new
                {
                    status = response.isSuccess,
                    time = DateTime.Now,
                    data = response.data,
                };
            }
            catch(Exception ex)
            {
                Response.StatusCode = 500;
                return new { status = appSettings.Value.errorMessage, time = DateTime.Now, data = ex.Message, data2 = ex.StackTrace };
            }
        }
        //Niyas 17-10-2023 
        private string GenerateJSONWebToken(UserLogin userLogin,long id)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                        new Claim(ClaimTypes.Name,userLogin.LoginUserName!),
                        new Claim(ClaimTypes.Name,userLogin.LoginPassword!),
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                userLogin.LoginUserName,
                claims,
                expires: DateTime.Now.AddMinutes(2),
                signingCredentials: credentials);

            string finaltoken = new JwtSecurityTokenHandler().WriteToken(token);
            if(finaltoken != null)
            {
                Authentication authentication = new Authentication();
                authentication.AccessToken = finaltoken;
                authentication.UserId = id;
                authentication.CreatedAt = DateTime.Now;
                authentication.ExpiryTime = DateTime.Now.AddMinutes(2);

                db.Authentications.Add(authentication);
                db.SaveChanges();
            }
            return finaltoken;
        }


        public ResponseObj<string> VerifyToken(string Token)
        {
            ResponseObj<string> responseObj = new ResponseObj<string>();
            try
            {
                var _userToken = db.Authentications.Where(x => x.AccessToken == Token).FirstOrDefault();
                if (_userToken == null)
                {
                    responseObj.isSuccess = false;
                    responseObj.message = "TokenNotMatch";
                    return responseObj;
                }

                if (_userToken.AccessToken != Token)
                {
                    responseObj.isSuccess = false;
                    responseObj.message = "TokenNotMatch";
                    return responseObj;
                }

                //DateTime currentDate = DateTime.Now;
                //if (_userToken.ExpiryTime < currentDate)
                //{
                //    responseObj.isSuccess = false;
                //    responseObj.message = "TokenExpire";
                //    return responseObj;
                //}
                responseObj.isSuccess = true;
            }
            catch (Exception ex)
            {
                responseObj.message = ex.Message;
            }
            return responseObj;
        }
    }
}
