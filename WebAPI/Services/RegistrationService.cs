﻿using MatixDealer.Controllers;
using MatixDealer.Models;
using Microsoft.Extensions.Options;
using SampleWebApplication1.Utils;
using static SampleWebApplication1.Utils.Entity;
using static SampleWebApplication1.Utils.Utilities;

namespace MatixDealer.Services
{
    public class RegistrationService
    {
        matixContext db;
        ILogger<RegistrationController> logger;
        private readonly IOptions<AppConfig> appSettings;
        Utilities _Utils;
        Common cmn;
        private matixContext dbContext;
        private ILogger<LoginController> logger1;

        public RegistrationService(matixContext _db, ILogger<RegistrationController> _logger, IOptions<AppConfig> appConfig)
        {
            db = _db;
            logger = _logger;
            appSettings = appConfig;
            _Utils = new Utilities(appConfig, db);
        }

        //public RegistrationService(MatixContext dbContext, ILogger<LoginController> logger1, IOptions<AppConfig> appSettings)
        //{
        //    this.dbContext = dbContext;
        //    this.logger1 = logger1;
        //    this.appSettings = appSettings;
        //}

        public ResponseObj<object> InsertOrUpdateUser(UserRegister userRegister)
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                var encryptPassword = _Utils.EncryptPassword(userRegister.LoginPassword);

               
                if (userRegister.Id == 0)
                {
                    var existEmpCode = db.Users.Where(x => x.EmployeeCode == userRegister.EmployeeCode).FirstOrDefault();

                    if(existEmpCode != null)
                    {
                        responseObj.isSuccess = false;
                        responseObj.message = "Failed";
                        responseObj.responseCode = 400;
                        responseObj.data = "EmployeeCode Exist";
                        return responseObj;
                    }

                    MasterDesignation masterDesignation = new MasterDesignation();
                    masterDesignation.Designation = userRegister.Designation;
                    db.MasterDesignations.Add(masterDesignation);
                    db.SaveChanges();

                    long masterDesginationId = masterDesignation.Id;

                    User user = new User();
                    user.ConfigUserRoleId = userRegister.ConfigUserRoleId;
                    user.MasterDesignationId = masterDesginationId;
                    user.EmployeeCode = userRegister.EmployeeCode;
                    user.FirstName = userRegister.FirstName;
                    user.LastName = userRegister.LastName;
                    user.MobileNumber = userRegister.MobileNumber;
                    user.EmailId = userRegister.EmailId;
                    user.Address = userRegister.Address;
                    user.CountryName = userRegister.CountryName;
                    user.CountryCode = userRegister.CountryCode;
                    user.StateName = userRegister.StateName;
                    user.StateCode = userRegister.StateCode;
                    user.City = userRegister.City;
                    user.PostalCode = userRegister.PostalCode;
                    user.Status = userRegister.Status;
                    //user.LoginUserName = userRegister.LoginUserName;
                    //user.LoginPassword = encryptPassword;
                    user.LoginUserName = userRegister.EmployeeCode;
                    user.LoginPassword = userRegister.EmployeeCode;
                    user.CreatedBy = userRegister.CreatedBy;
                    user.CreateDate = DateTime.Now;

                    db.Users.Add(user);
                    db.SaveChanges();
                }
                else
                {
                    if (userRegister.MasterDesignationId > 0 || userRegister.MasterDesignationId != null)
                    {
                        var check = db.MasterDesignations.Where(x => x.Id == userRegister.MasterDesignationId).FirstOrDefault();
                        if (check != null)
                        {
                            check.Designation = userRegister.Designation;
                            db.MasterDesignations.Update(check);
                            db.SaveChanges();
                        }
                    }
                    if (userRegister.Id != null || userRegister.Id > 0)
                    {
                        var check = db.Users.Where(x => x.Id == userRegister.Id).FirstOrDefault();
                        check.ConfigUserRoleId = userRegister.ConfigUserRoleId;
                        if (userRegister.MasterDesignationId != null)
                            check.MasterDesignationId = userRegister.MasterDesignationId;
                        check.EmployeeCode = userRegister.EmployeeCode;
                        check.FirstName = userRegister.FirstName;
                        check.LastName = userRegister.LastName;
                        check.MobileNumber = userRegister.MobileNumber;
                        check.EmailId = userRegister.EmailId;
                        check.Address = userRegister.Address;
                        check.CountryName = userRegister.CountryName;
                        check.CountryCode = userRegister.CountryCode;
                        check.StateName = userRegister.StateName;
                        check.StateCode = userRegister.StateCode;
                        check.City = userRegister.City;
                        check.PostalCode = userRegister.PostalCode;
                        check.Status = userRegister.Status;
                        check.LoginUserName = userRegister.LoginUserName;
                        check.LoginPassword = encryptPassword;
                        check.UpdatedBy = userRegister.Id;
                        check.UpdateDate = DateTime.Now;

                        db.Users.Update(check);
                        db.SaveChanges();
                    }
                }
                responseObj.isSuccess = true;
                responseObj.message = "Success";
                responseObj.responseCode = 200;
                responseObj.data = userRegister.Id;
                return responseObj;
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500; responseObj.isSuccess = false; responseObj.data = ex.Message;
                return responseObj;
            }
        }


        public ResponseObj<object> AssighManager(AssignManager assignManager)
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                if (assignManager.assighId != null)
                {
                    foreach (var item in assignManager.assighId)
                    {
                        var check = db.UserTeams.Where(x => x.ToUserId == Convert.ToInt64(item.id) && x.FromUsersId == assignManager.managerId).FirstOrDefault();
                        if (check == null)
                        {
                            Models.UserTeam userTeams = new Models.UserTeam();
                            userTeams.FromUsersId = assignManager.managerId;
                            userTeams.ToUserId = Convert.ToInt64(item.id);

                            db.UserTeams.Add(userTeams);
                            db.SaveChanges();
                        }
                    }
                }

                responseObj.isSuccess = true;
                responseObj.message = "Success";
                responseObj.responseCode = 200;
                responseObj.data = assignManager.managerId;
                return responseObj;
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500; responseObj.isSuccess = false; responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public ResponseObj<object> UpdateUserStatus(int id, string status)
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                var check = db.Users.Where(x => x.Id == id).FirstOrDefault();
                if (check == null)
                {
                    responseObj.responseCode = 400;
                    responseObj.isSuccess = false;
                    responseObj.data = "Invalid User";
                    return responseObj;
                }
                else
                {
                    check.Status = status;
                    db.Users.Update(check);
                    db.SaveChanges();
                }
                responseObj.isSuccess = true;
                responseObj.message = "Success";
                responseObj.responseCode = 200;
                responseObj.data = id;
                return responseObj;
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500; responseObj.isSuccess = false; responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public ResponseObj<object> GetDesignationDetail()
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                var Designations = db.MasterDesignations.ToList();

                responseObj.isSuccess = true;
                responseObj.message = "Success";
                responseObj.responseCode = 200;
                responseObj.data = Designations;
                return responseObj;
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500; responseObj.isSuccess = false; responseObj.data = ex.Message;
                return responseObj;
            }
        }

    }
}
