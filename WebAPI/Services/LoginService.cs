﻿using MatixDealer.Controllers;
using MatixDealer.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SampleWebApplication1.Utils;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using static SampleWebApplication1.Utils.Entity;

namespace MatixDealer.Services
{
    public class LoginService
    {
        matixContext db;
        ILogger<LoginController> logger;
        private readonly IOptions<AppConfig> appSettings;
        Utilities _Utils;
        Common cmn;
        private IConfiguration _config;

        public LoginService(matixContext _db, ILogger<LoginController> _logger, IOptions<AppConfig> appConfig, IConfiguration config)
        {
            db = _db;
            logger = _logger;
            appSettings = appConfig;
            _Utils = new Utilities(appConfig, db);
            _config = config;
        }

        public ResponseObj<object> LoginUser(UserLogin userLogin)
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                var check = db.Users.Where(x => x.LoginUserName == userLogin.LoginUserName && x.LoginPassword == userLogin.LoginPassword && x.Status == "Active").FirstOrDefault();
                if (check != null)
                {
                    var uId = check.Id;
                    var roleid = check.ConfigUserRoleId;
                    var username = check.LoginUserName;
                    var token = GenerateJSONWebToken(userLogin, uId);
                    var data = new
                    {
                        token = token,
                        userid = uId,
                        userRoleId = roleid,
                        userName = username
                    };
                    responseObj.isSuccess = true;
                    responseObj.message = "Success";
                    responseObj.responseCode = 200;
                    responseObj.data = data;
                    return responseObj;
                }

                responseObj.isSuccess = false;
                responseObj.message = "Invalid Users";
                responseObj.responseCode = 400;
                responseObj.data = check;
                return responseObj;
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500; responseObj.isSuccess = false; responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public ResponseObj<object> GetUserDetails()
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                var userDetails = db.Users.ToList();

                responseObj.isSuccess = true;
                responseObj.message = "Success";
                responseObj.responseCode = 200;
                responseObj.data = userDetails;
                return responseObj;
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500; responseObj.isSuccess = false; responseObj.data = ex.Message;
                return responseObj;
            }
        }


        private string GenerateJSONWebToken(UserLogin userLogin, long id)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                        new Claim(ClaimTypes.Name,userLogin.LoginUserName!),
                        new Claim(ClaimTypes.Name,userLogin.LoginPassword!),
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                userLogin.LoginUserName,
                claims,
                expires: DateTime.Now.AddMinutes(2),
                signingCredentials: credentials);

            string finaltoken = new JwtSecurityTokenHandler().WriteToken(token);
            if (finaltoken != null)
            {
                Authentication authentication = new Authentication();
                authentication.AccessToken = finaltoken;
                authentication.UserId = id;
                authentication.CreatedAt = DateTime.Now;
                authentication.ExpiryTime = DateTime.Now.AddMinutes(2);

                db.Authentications.Add(authentication);
                db.SaveChanges();
            }
            return finaltoken;
        }
    }
}
