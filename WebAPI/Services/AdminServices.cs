﻿using MatixDealer.Controllers;
using MatixDealer.Models;
using Microsoft.Extensions.Options;
using SampleWebApplication1.Utils;
using System;
using System.Net.NetworkInformation;
using static SampleWebApplication1.Utils.Entity;

namespace MatixDealer.Services
{
    public class AdminServices
    {
        matixContext db;
        ILogger<AdminController> logger;
        private readonly IOptions<AppConfig> appSettings;
        Utilities _Utils;
        Common cmn;

        public AdminServices(matixContext _db, ILogger<AdminController> _logger, IOptions<AppConfig> appConfig)
        {
            db = _db;
            logger = _logger;
            appSettings = appConfig;
            //_Utils = new Utilities(appConfig);
        }

        public async Task<object> GetAllStateManagerDetails(string? searchText, string? CountryName, string? CityName, string? Status, long UserRoleId)
        {
            var responseObj = new ResponseObj<object>();

            try
            {
                var StateManagers = (from x in db.Users
                                     join y in db.ConfigUserRoles on x.ConfigUserRoleId equals y.Id
                                     join z in db.MasterDesignations on x.MasterDesignationId equals z.Id
                                     where y.Id == UserRoleId
                                     select new StateManagerRes
                                     {
                                        MobileNumber =  x.MobileNumber,
                                        Status = x.Status,
                                        StateName =  x.StateName,
                                        CountryName =  x.CountryName,
                                        CreateDate =  x.CreateDate,
                                        UpdateDate = x.UpdateDate,
                                        Address = x.Address,
                                        City = x.City,
                                        CountryCode = x.CountryCode,
                                        LoginUserName =  x.LoginUserName,
                                        EmailId =  x.EmailId,
                                        FirstName =  x.FirstName,
                                        LastName = x.LastName,
                                        PostalCode = x.PostalCode,
                                         DealerCode = x.DealerCode,
                                         StateCode = x.StateCode,
                                        Id = x.Id,
                                        EmployeeCode = x.EmployeeCode,
                                        UserRole = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                        Designation = db.MasterDesignations.Where(a => a.Id == x.MasterDesignationId).Select(n => n.Designation).FirstOrDefault(),
                                         cstNo=x.CstNo,
                                         lstNo=x.LstNo,
                                         serviceTaxRegnNo=x.ServiceTaxRegnNo,
                                         pan=x.Pan,
                                         ifmsCode=x.IfmsCode
                                     }).ToList();

                if(StateManagers.Count > 0)
                {
                    var result = new List<StateManagerRes>();

                    if (searchText != null)
                    {
                        result = StateManagers
                               .Where(item =>
                                   (item.EmailId?.Contains(searchText, StringComparison.OrdinalIgnoreCase) ?? false) ||
                                  (item.EmployeeCode?.Contains(searchText, StringComparison.OrdinalIgnoreCase) ?? false) ||
                                  (item.MobileNumber?.Contains(searchText, StringComparison.OrdinalIgnoreCase) ?? false)).ToList();

                        if (result == null)
                        {
                            responseObj.responseCode = 500;
                            responseObj.isSuccess = false;
                            responseObj.data = "Data not found";
                            return responseObj;
                        }
                        responseObj.responseCode = 200;
                        responseObj.isSuccess = true;
                        responseObj.data = result;
                        return responseObj;
                    }
                    else
                    {
                        if(CountryName != null && CityName != null && Status != null)
                        {
                            result = StateManagers.Where(x => x.CountryName == CountryName && x.City == CityName && x.Status == Status).ToList();
                        }
                        else if (CountryName != null && CityName != null)
                        {
                            result = StateManagers.Where(x => x.CountryName == CountryName && x.City == CityName).ToList();
                        }
                        if (CountryName != null && Status != null)
                        {
                            result = StateManagers.Where(x => x.CountryName == CountryName && x.Status == Status).ToList();
                        }
                        if (CityName != null && Status != null)
                        {
                            result = StateManagers.Where(x => x.City == CityName && x.Status == Status).ToList();
                        }
                        else if (CountryName != null)
                        {
                            result = StateManagers.Where(x => x.CountryName == CountryName).ToList();
                        }
                        else if (CityName != null)
                        {
                            result = StateManagers.Where(x => x.City == CityName).ToList();
                        }
                        else if (Status != null)
                        {
                            result = StateManagers.Where(x => x.Status == Status).ToList();
                        }
                        else
                        {
                            result = StateManagers.ToList();
                        }
                        responseObj.responseCode = 200;
                        responseObj.isSuccess = true;
                        responseObj.data = result;
                        return responseObj;
                    }
                }
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = "Data not found";
                return responseObj;
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public async Task<object> GetAllUnAssigendDetails(long UserRoleId , long UserId)
        {
            var responseObj = new ResponseObj<object>();

            try
            {

                var result = (from x in db.Users
                             where x.ConfigUserRoleId == UserRoleId && !db.UserTeams.Any(z => z.ToUserId == x.Id && z.FromUsersId != UserId)
                             select new StateManagerRes
                             {
                                 MobileNumber = x.MobileNumber,
                                 Status = x.Status,
                                 StateName = x.StateName,
                                 CountryName = x.CountryName,
                                 CreateDate = x.CreateDate,
                                 UpdateDate = x.UpdateDate,
                                 Address = x.Address,
                                 City = x.City,
                                 CountryCode = x.CountryCode,
                                 LoginUserName = x.LoginUserName,
                                 EmailId = x.EmailId,
                                 FirstName = x.FirstName,
                                 LastName = x.LastName,
                                 PostalCode = x.PostalCode,
                                 DealerCode = x.DealerCode,
                                 StateCode = x.StateCode,
                                 Id = x.Id,
                                 EmployeeCode = x.EmployeeCode
                             }).ToList();


                responseObj.responseCode = 200;
                responseObj.isSuccess = true;
                responseObj.data = result;
                return responseObj;
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public async Task<object> GetUserDetails(long? UserId)
        {
            var responseObj = new ResponseObj<object>();

            try
            {
                var _user = (from x in db.Users
                                     join y in db.ConfigUserRoles on x.ConfigUserRoleId equals y.Id
                                     join z in db.MasterDesignations on x.MasterDesignationId equals z.Id
                                     where y.Id == UserId
                                     select new StateManagerRes
                                     {
                                         MobileNumber = x.MobileNumber,
                                         Status = x.Status,
                                         StateName = x.StateName,
                                         CountryName = x.CountryName,
                                         CreateDate = x.CreateDate,
                                         UpdateDate = x.UpdateDate,
                                         Address = x.Address,
                                         City = x.City,
                                         CountryCode = x.CountryCode,
                                         LoginUserName = x.LoginUserName,
                                         EmailId = x.EmailId,
                                         FirstName = x.FirstName,
                                         LastName = x.LastName,
                                         PostalCode = x.PostalCode,
                                         StateCode = x.StateCode,
                                         Id = x.Id,
                                         EmployeeCode = x.EmployeeCode,
                                         UserRole = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                         Designation = db.MasterDesignations.Where(a => a.Id == x.MasterDesignationId).Select(n => n.Designation).FirstOrDefault(),
                                     }).ToList();

                if (_user.Count == 0)
                {
                    responseObj.responseCode = 500;
                    responseObj.isSuccess = false;
                    responseObj.data = "Data not found";
                    return responseObj;

                }
                responseObj.responseCode = 200;
                responseObj.isSuccess = true;
                responseObj.data = _user;
                return responseObj;

            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public async Task<object> GetAllInvoiceNumber(long UserId)
        {
            var responseObj = new ResponseObj<object>();

            try
            {
                var _InvoiceNumber = (from x in db.AccountStatements
                                      join n in db.UserTeams on x.UserId equals n.ToUserId
                                      join y in db.Users on n.ToUserId equals y.Id
                                      where n.FromUsersId == UserId
                                      select new
                                      {
                                          x.Id,
                                          x.InvoiceAmount,
                                          x.InvoiceNumber,
                                          x.EndDate,
                                          x.StartDate,
                                      }).ToList();

                if (_InvoiceNumber.Count == 0)
                {
                    responseObj.responseCode = 500;
                    responseObj.isSuccess = false;
                    responseObj.data = "Data not found";
                    return responseObj;

                }
                responseObj.responseCode = 200;
                responseObj.isSuccess = true;
                responseObj.data = _InvoiceNumber;
                return responseObj;

            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public async Task<object> GetAllAnnouncement(DateTime? fromDate, DateTime? todate)
        {
            var responseObj = new ResponseObj<object>();

            try
            {
                var _InvoiceNumber = (from x in db.Announcements
                                      select new
                                      {
                                         x.Announcement1,
                                         x.Id,
                                         x.AnnouncementByDealer,
                                         x.AnnouncementByCity,
                                         x.AnnouncementByManager,
                                         x.FromDate,
                                         x.ToDate,
                                         x.AnnouncementName,
                                         x.AnnouncementByDealerId,
                                         x.AnnouncementByManagerId,
                                         x.AnnouncementFor
                                      }).ToList();

                if (_InvoiceNumber.Count == 0)
                {
                    responseObj.responseCode = 500;
                    responseObj.isSuccess = false;
                    responseObj.data = "Data not found";
                    return responseObj;

                }

                if (fromDate != null && todate != null)
                {
                    var result = _InvoiceNumber.Where(x => x.FromDate >= fromDate && x.ToDate <= todate).ToList();

                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = result;
                    return responseObj;

                }
                else if (fromDate != null)
                {
                    var result = _InvoiceNumber.Where(x => x.FromDate >= fromDate).ToList();

                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = result;
                    return responseObj;

                }
                else
                {

                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = _InvoiceNumber;
                    return responseObj;
                }
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public async Task<object> GetInvoiceDetailsForDealers(string? searchText, string? CountryName, string? CityName,long? ManagerId, string? InvoiceNumber , long? UserId)
        {
            var responseObj = new ResponseObj<object>();

            try
            {
                var _InvoiceNumber = (from x in db.AccountStatements
                                          //  join n in db.UserTeams on x.UserId equals n.ToUserId
                                          // join y in db.Users on n.ToUserId equals y.Id
                                      join y in db.Users on x.UserId equals y.Id
                                      where y.ConfigUserRoleId == 4
                                      //n.FromUsersId == UserId
                                      select new InvoiceRes
                                      {
                                         EmailId = y.EmailId,
                                         DealerCode = y.DealerCode,
                                         UserId = x.UserId,
                                         City = y.City,
                                         CountryName = y.CountryName,
                                         LoginUserName = y.LoginUserName,
                                         InvoiceId = x.Id,
                                         InvoiceAmount = x.InvoiceAmount,
                                         InvoiceNumber = x.InvoiceNumber,
                                         EndDate = x.EndDate,
                                         startDate = x.StartDate,
                                         InvoiceDate = x.InvoiveDate,
                                         PdfUrl = x.PdfUrl,
                                         FirstName = y.FirstName
                                      }).ToList();

                if (_InvoiceNumber.Count >= 0)
                {
                    var result = new List<InvoiceRes>();

                    if (searchText != null)
                    {
                        result = _InvoiceNumber
                               .Where(item =>
                                  (item.EmailId?.Contains(searchText, StringComparison.OrdinalIgnoreCase) ?? false) ||
                                  (item.DealerCode?.Contains(searchText, StringComparison.OrdinalIgnoreCase) ?? false) ||
                                  (item.InvoiceNumber?.Contains(searchText, StringComparison.OrdinalIgnoreCase) ?? false)).ToList();

                        if (result == null)
                        {
                            responseObj.responseCode = 500;
                            responseObj.isSuccess = false;
                            responseObj.data = "Data not found";
                            return responseObj;
                        }
                        responseObj.responseCode = 200;
                        responseObj.isSuccess = true;
                        responseObj.data = result;
                        return responseObj;
                    }
                    else
                    {
                        if (CountryName != null && CityName != null && ManagerId != null && InvoiceNumber != null)
                        {
                            result = _InvoiceNumber.Where(x => x.CountryName == CountryName && x.City == CityName && x.InvoiceNumber == InvoiceNumber && x.UserId == ManagerId).ToList();
                        }
                        else if (CountryName != null && CityName != null && InvoiceNumber != null)
                        {
                            result = _InvoiceNumber.Where(x => x.CountryName == CountryName && x.City == CityName && x.InvoiceNumber == InvoiceNumber).ToList();
                        }
                        if (CountryName != null && ManagerId != null)
                        {
                            result = _InvoiceNumber.Where(x => x.CountryName == CountryName  && x.UserId == ManagerId).ToList();
                        }
                        if (CityName != null && InvoiceNumber != null)
                        {
                            result = _InvoiceNumber.Where(x => x.City == CityName && x.InvoiceNumber == InvoiceNumber).ToList();
                        }
                        else if (CountryName != null)
                        {
                            result = _InvoiceNumber.Where(x => x.CountryName == CountryName).ToList();
                        }
                        else if (CityName != null)
                        {
                            result = _InvoiceNumber.Where(x => x.City == CityName).ToList();
                        }
                        else if (InvoiceNumber != null)
                        {
                            result = _InvoiceNumber.Where(x => x.InvoiceNumber == InvoiceNumber).ToList();
                        }
                        else if (ManagerId != null)
                        {
                            result = _InvoiceNumber.Where(x => x.UserId == ManagerId).ToList();
                        }
                        else
                        {
                            result = _InvoiceNumber.ToList();
                        }
                        responseObj.responseCode = 200;
                        responseObj.isSuccess = true;
                        responseObj.data = result;
                        return responseObj;
                    }
                }
              
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = "Data not found";
                return responseObj;

            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public async Task<object> DeleteUser(long UserId)
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                var User = db.Users.Where(x => x.Id == UserId).FirstOrDefault();
                if (User != null)
                {
                    var result = db.Users.Remove(User);
                    db.SaveChanges();

                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = result;
                    return responseObj;
                }
                else
                {
                    responseObj.responseCode = 500;
                    responseObj.isSuccess = false;
                    responseObj.data = "Data not found";
                    return responseObj;
                }
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public async Task<object> DeleteMultipleUser(AssignManager request)
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                foreach (var item in request.assighId)
                {
                    var User = db.UserTeams.Where(x => x.ToUserId == Convert.ToInt64(item.id) && x.FromUsersId == request.managerId).FirstOrDefault();

                    if (User != null)
                    {
                        var result = db.UserTeams.Remove(User);
                        db.SaveChanges();
                    }
                }
                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = "Users Deleted successfully";
                    return responseObj;
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public async Task<object> MatixDashboard(DateTime? selectRange, DateTime? fromDate, DateTime? toDate, long? DelerId, long? ManagerId, long? StateManagerId)
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                var stateManagers = db.Users.Where(x => x.ConfigUserRoleId == 2).ToList();

                var Managers = db.Users.Where(x => x.ConfigUserRoleId == 3).ToList();

                var dealers = db.Users.Where(x => x.ConfigUserRoleId == 4).ToList();

                var announcement = db.Announcements.ToList();

                var documents = db.Documents.ToList();

                var accountStatement = db.AccountStatements.ToList();

                var sales = db.Sales.ToList();

                if (DelerId == 0 && ManagerId == 0 && StateManagerId == 0)
                {
                    #region
                    if (selectRange != null)
                    {
                        announcement = db.Announcements.Where(x => x.FromDate >= selectRange).ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= selectRange).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= selectRange).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= selectRange).ToList();
                    }
                    else if (fromDate != null && toDate != null)
                    {
                        announcement = db.Announcements.Where(x => x.FromDate >= fromDate && x.FromDate <= toDate).ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= fromDate && x.StartDate <= toDate).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= fromDate && x.InvoiceDate <= toDate).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= fromDate && x.UploadedDate <= toDate).ToList();
                    }
                    else if (fromDate != null)
                    {
                        announcement = db.Announcements.Where(x => x.FromDate >= fromDate).ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= fromDate).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= fromDate).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= fromDate).ToList();

                    }
                    #endregion
                }
                else if (DelerId > 0 && ManagerId == 0 && StateManagerId == 0)
                {

                    #region
                    if (selectRange != null)
                    {
                        announcement = db.Announcements.Where(x => x.FromDate >= selectRange && x.AnnouncementByDealerId == DelerId || x.AnnouncementFor == "All").ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= selectRange).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= selectRange && x.UserId == DelerId).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= selectRange && x.UserId == DelerId || x.UserId == 0).ToList();
                    }
                    else if (fromDate != null && toDate != null)
                    {
                        announcement = db.Announcements.Where(x => x.FromDate >= fromDate && x.FromDate <= toDate && x.AnnouncementByDealerId == DelerId || x.AnnouncementFor == "All").ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= fromDate && x.StartDate <= toDate).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= fromDate && x.InvoiceDate <= toDate && x.UserId == DelerId).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= fromDate && x.UploadedDate <= toDate && x.UserId == DelerId || x.UserId == 0).ToList();
                    }
                    else if (fromDate != null)
                    {
                        announcement = db.Announcements.Where(x => x.FromDate >= fromDate && x.AnnouncementByDealerId == DelerId || x.AnnouncementFor == "All").ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= fromDate).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= fromDate && x.UserId == DelerId).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= fromDate && x.UserId == DelerId || x.UserId == 0).ToList();
                    }
                    else
                    {
                        sales = db.Sales.Where(x => x.UserId == DelerId).ToList();
                        documents = db.Documents.Where(x => x.UserId == DelerId || x.UserId == 0).ToList();
                    }
                    #endregion
                }
                else if (DelerId == 0 && ManagerId > 0 && StateManagerId == 0)
                {
                    var _responce = (from x in db.Users
                                     where x.ConfigUserRoleId == 4 && db.UserTeams.Any(z => z.ToUserId == x.Id && z.FromUsersId == ManagerId)
                                     select new
                                     {
                                         dealerId = x.Id,

                                     }).Select(n => n.dealerId).ToList();

                    #region
                    if (selectRange != null)
                    {
                        dealers = db.Users.Where(x => x.ConfigUserRoleId == 4 && x.CreateDate >= selectRange && _responce.Contains((long)x.Id)).ToList();
                        announcement = db.Announcements.Where(x => x.FromDate >= selectRange && x.AnnouncementByManagerId == ManagerId || x.AnnouncementFor == "All").ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= selectRange).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= selectRange && _responce.Contains((long)x.UserId)).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= selectRange && _responce.Contains((long)x.UserId) || x.UserId == 0).ToList();

                    }
                    else if (fromDate != null && toDate != null)
                    {
                        dealers = db.Users.Where(x => x.ConfigUserRoleId == 4 && x.CreateDate >= fromDate && x.CreateDate <= toDate && _responce.Contains((long)x.Id)).ToList();
                        announcement = db.Announcements.Where(x => x.FromDate >= fromDate && x.FromDate <= toDate && x.AnnouncementByManagerId == ManagerId || x.AnnouncementFor == "All").ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= fromDate && x.StartDate <= toDate).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= fromDate && x.InvoiceDate <= toDate && _responce.Contains((long)x.UserId)).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= fromDate && x.UploadedDate <= toDate && _responce.Contains((long)x.UserId) || x.UserId == 0).ToList();

                    }
                    else if (fromDate != null)
                    {
                        dealers = db.Users.Where(x => x.ConfigUserRoleId == 4 && x.CreateDate >= fromDate && _responce.Contains((long)x.Id)).ToList();
                        announcement = db.Announcements.Where(x => x.FromDate >= fromDate && x.AnnouncementByManagerId == ManagerId || x.AnnouncementFor == "All").ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= fromDate).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= fromDate && _responce.Contains((long)x.UserId)).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= fromDate && _responce.Contains((long)x.UserId) || x.UserId == 0).ToList();

                    }
                    else
                    {
                        dealers = db.Users.Where(x => x.ConfigUserRoleId == 4 && _responce.Contains((long)x.Id)).ToList();
                        sales = db.Sales.Where(x => _responce.Contains((long)x.UserId)).ToList();
                        documents = db.Documents.Where(x => _responce.Contains((long)x.UserId) || x.UserId == 0).ToList();

                    }
                    #endregion
                }
                else if (DelerId == 0 && ManagerId == 0 && StateManagerId > 0)
                {
                    var StateManagerRes = (from x in db.Users
                                           where x.ConfigUserRoleId == 3 && db.UserTeams.Any(z => z.ToUserId == x.Id && z.FromUsersId == StateManagerId)
                                           select new
                                           {
                                               managerId = x.Id,
                                           }).Select(d => d.managerId).ToList();

                    var _responce = (from x in db.Users
                                     join y in db.UserTeams on x.Id equals y.ToUserId
                                     where x.ConfigUserRoleId == 4 && StateManagerRes.Contains((long)y.FromUsersId)
                                     select new
                                     {
                                         dealerId = x.Id,
                                     }).Select(d => d.dealerId).ToList();
                    #region
                    if (selectRange != null)
                    {
                        dealers = db.Users.Where(x => x.ConfigUserRoleId == 4 && x.CreateDate >= selectRange && _responce.Contains((long)x.Id)).ToList();
                        Managers = db.Users.Where(x => x.ConfigUserRoleId == 3 && x.CreateDate >= selectRange && StateManagerRes.Contains((long)x.Id)).ToList();
                        announcement = db.Announcements.Where(x => x.FromDate >= selectRange).ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= selectRange).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= selectRange && _responce.Contains((long)x.UserId)).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= selectRange && _responce.Contains((long)x.UserId) || x.UserId == 0).ToList();

                    }
                    else if (fromDate != null && toDate != null)
                    {
                        dealers = db.Users.Where(x => x.ConfigUserRoleId == 4 && x.CreateDate >= fromDate && x.CreateDate <= toDate && _responce.Contains((long)x.Id)).ToList();
                        Managers = db.Users.Where(x => x.ConfigUserRoleId == 3 && x.CreateDate >= fromDate && x.CreateDate <= toDate && StateManagerRes.Contains((long)x.Id)).ToList();
                        announcement = db.Announcements.Where(x => x.FromDate >= fromDate && x.FromDate <= toDate).ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= fromDate && x.StartDate <= toDate).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= fromDate && x.InvoiceDate <= toDate && _responce.Contains((long)x.UserId)).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= fromDate && x.UploadedDate <= toDate && _responce.Contains((long)x.UserId) || x.UserId == 0).ToList();

                    }
                    else if (fromDate != null)
                    {
                        dealers = db.Users.Where(x => x.ConfigUserRoleId == 4 && x.CreateDate >= fromDate && _responce.Contains((long)x.Id)).ToList();
                        Managers = db.Users.Where(x => x.ConfigUserRoleId == 3 && StateManagerRes.Contains((long)x.Id)).ToList();
                        announcement = db.Announcements.Where(x => x.FromDate >= fromDate).ToList();
                        accountStatement = db.AccountStatements.Where(x => x.StartDate >= fromDate).ToList();
                        sales = db.Sales.Where(x => x.InvoiceDate >= fromDate && _responce.Contains((long)x.UserId)).ToList();
                        documents = db.Documents.Where(x => x.UploadedDate >= fromDate && _responce.Contains((long)x.UserId) || x.UserId == 0).ToList();

                    }
                    else
                    {
                        dealers = db.Users.Where(x => x.ConfigUserRoleId == 4 && _responce.Contains((long)x.Id)).ToList();
                        Managers = db.Users.Where(x => x.ConfigUserRoleId == 3 && StateManagerRes.Contains((long)x.Id)).ToList();
                        sales = db.Sales.Where(x => _responce.Contains((long)x.UserId)).ToList();
                        documents = db.Documents.Where(x => _responce.Contains((long)x.UserId) || x.UserId == 0).ToList();

                    }
                    #endregion
                }
                else
                { }

                var result = new
                {
                    StateManagersCount = stateManagers.Count > 0 ? stateManagers.Count : 0,
                    ManagersCount = Managers.Count > 0 ? Managers.Count : 0,
                    dealersCount = dealers.Count > 0 ? dealers.Count : 0,
                    announcement = announcement.Count > 0 ? announcement.Count : 0,
                    accountStatement = accountStatement.Count > 0 ? accountStatement.Count : 0,
                    SalesCount = sales.Count > 0 ? sales.Count : 0,
                    documentsCount = documents.Count > 0 ? documents.Count : 0
                };

                responseObj.responseCode = 200;
                responseObj.isSuccess = true;
                responseObj.data = result;
                return responseObj;
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }
        public async Task<object> GetAllUserDetailsBy_UserTeams(long UserId)
        {
            var responseObj = new ResponseObj<object>();

            try
            {
                var _responce = (from n in db.UserTeams
                                 join x in db.Users on n.ToUserId equals x.Id
                                 join y in db.ConfigUserRoles on x.ConfigUserRoleId equals y.Id
                                 join z in db.MasterDesignations on x.MasterDesignationId equals z.Id
                                 where n.FromUsersId == UserId
                                 select new
                                 {
                                    x.MobileNumber,
                                    x.Status,
                                    x.StateName,
                                    x.CountryName,
                                    x.CreateDate,
                                    x.UpdateDate,
                                    x.Address,
                                    x.City,
                                    x.CountryCode,
                                    x.LoginUserName,
                                    x.EmailId,
                                    x.FirstName,
                                    x.LastName,
                                    x.PostalCode,
                                    x.DealerCode,
                                    x.StateCode,
                                    x.Id,
                                    n.FromUsersId,
                                    n.ToUserId,
                                    x.EmployeeCode,
                                    UserRole = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                    Designation = db.MasterDesignations.Where(a => a.Id == x.MasterDesignationId).Select(n => n.Designation).FirstOrDefault(),
                                 }).ToList();

                if(_responce.Count == 0)
                        {
                    responseObj.responseCode = 500;
                    responseObj.isSuccess = false;
                    responseObj.data = "Data not found";
                    return responseObj;
                }
                responseObj.responseCode = 200;
                responseObj.isSuccess = true;
                responseObj.data = _responce;
                return responseObj;

            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }


        public async Task<object> GetAllDealers_UserTeams()
        {
            var responseObj = new ResponseObj<object>();

            try
            {
                var _responce = (from n in db.UserTeams
                                 join x in db.Users on n.ToUserId equals x.Id
                                 join y in db.ConfigUserRoles on x.ConfigUserRoleId equals y.Id
                                 join z in db.MasterDesignations on x.MasterDesignationId equals z.Id
                                 where x.ConfigUserRoleId == 4
                                 select new
                                 {
                                     x.MobileNumber,
                                     x.Status,
                                     x.StateName,
                                     x.CountryName,
                                     x.CreateDate,
                                     x.UpdateDate,
                                     x.Address,
                                     x.City,
                                     x.CountryCode,
                                     x.LoginUserName,
                                     x.EmailId,
                                     x.FirstName,
                                     x.LastName,
                                     x.PostalCode,
                                     x.StateCode,
                                     UserId = x.Id,
                                     n.FromUsersId,
                                     n.ToUserId,
                                     x.EmployeeCode,
                                     UserRole = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                     Designation = db.MasterDesignations.Where(a => a.Id == x.MasterDesignationId).Select(n => n.Designation).FirstOrDefault(),
                                     ManagerName = (from m in db.Users
                                                    where m.Id == n.FromUsersId && m.ConfigUserRoleId == 3
                                                    orderby m.Id descending
                                                    select new
                                                    {
                                                        m.FirstName,
                                                        m.LastName
                                                    }).FirstOrDefault(),
                                 }).ToList();

                if (_responce.Count == 0)
                {
                    responseObj.responseCode = 500;
                    responseObj.isSuccess = false;
                    responseObj.data = "Data not found";
                    return responseObj;
                }
                responseObj.responseCode = 200;
                responseObj.isSuccess = true;
                responseObj.data = _responce;
                return responseObj;

            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public async Task<object> GetAllManagers_UserTeams(long? UserId, long? StateManagerId)
        {
            var responseObj = new ResponseObj<object>();

            try
            {
                if (UserId == 0 && StateManagerId == 0)
                {
                    var _responce = (
                                     from x in db.Users
                                         //  join n in db.UserTeams on x.Id equals n.ToUserId
                                     join y in db.ConfigUserRoles on x.ConfigUserRoleId equals y.Id
                                     join z in db.MasterDesignations on x.MasterDesignationId equals z.Id
                                     where x.ConfigUserRoleId == 3
                                     select new
                                     {
                                         x.MobileNumber,
                                         x.Status,
                                         x.StateName,
                                         x.CountryName,
                                         x.CreateDate,
                                         x.UpdateDate,
                                         x.Address,
                                         x.City,
                                         x.CountryCode,
                                         x.LoginUserName,
                                         x.EmailId,
                                         x.FirstName,
                                         x.LastName,
                                         x.PostalCode,
                                         x.StateCode,
                                         Id = x.Id,
                                         x.EmployeeCode,
                                         UserRole = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                         Designation = db.MasterDesignations.Where(a => a.Id == x.MasterDesignationId).Select(n => n.Designation).FirstOrDefault(),
                                         StateManagerName = (from m in db.Users
                                                             join n in db.UserTeams on x.Id equals n.ToUserId
                                                             where m.Id == n.FromUsersId && m.ConfigUserRoleId == 2
                                                             orderby m.Id descending
                                                             select new
                                                             {
                                                                 m.FirstName,
                                                                 m.LastName,
                                                                 m.Id
                                                             }).FirstOrDefault(),
                                     }).ToList();

                    if (_responce.Count == 0)
                    {
                        responseObj.responseCode = 500;
                        responseObj.isSuccess = false;
                        responseObj.data = "Data not found";
                        return responseObj;
                    }
                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = _responce;
                    return responseObj;
                }

                else if (StateManagerId > 0 && UserId == 0)
                {
                    var _responce = (from x in db.Users
                                     where x.ConfigUserRoleId == 3 && db.UserTeams.Any(z => z.ToUserId == x.Id && z.FromUsersId == StateManagerId)
                                     select new
                                     {
                                         x.MobileNumber,
                                         x.Status,
                                         x.StateName,
                                         x.CountryName,
                                         x.CreateDate,
                                         x.UpdateDate,
                                         x.Address,
                                         x.City,
                                         x.CountryCode,
                                         x.LoginUserName,
                                         x.EmailId,
                                         x.FirstName,
                                         x.LastName,
                                         x.PostalCode,
                                         x.StateCode,
                                         Id = x.Id,
                                         x.EmployeeCode,
                                         UserRole = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                         Designation = db.MasterDesignations.Where(a => a.Id == x.MasterDesignationId).Select(n => n.Designation).FirstOrDefault(),
                                         StateManagerName = (from m in db.Users
                                                             join n in db.UserTeams on x.Id equals n.ToUserId
                                                             where m.Id == n.FromUsersId && m.ConfigUserRoleId == 2
                                                             orderby m.Id descending
                                                             select new
                                                             {
                                                                 m.FirstName,
                                                                 m.LastName,
                                                                 m.Id
                                                             }).FirstOrDefault(),
                                     }).ToList();

                    if (_responce.Count == 0)
                    {
                        responseObj.responseCode = 500;
                        responseObj.isSuccess = false;
                        responseObj.data = "Data not found";
                        return responseObj;
                    }


                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = _responce;
                    return responseObj;
                }
                else
                {
                    var _responce = (
                 from x in db.Users
                     //  join n in db.UserTeams on x.Id equals n.ToUserId
                 join y in db.ConfigUserRoles on x.ConfigUserRoleId equals y.Id
                 join z in db.MasterDesignations on x.MasterDesignationId equals z.Id
                 where x.ConfigUserRoleId == 3 && x.Id == UserId
                 select new
                 {
                     x.MobileNumber,
                     x.Status,
                     x.StateName,
                     x.CountryName,
                     x.CreateDate,
                     x.UpdateDate,
                     x.Address,
                     x.City,
                     x.CountryCode,
                     x.LoginUserName,
                     x.EmailId,
                     x.FirstName,
                     x.LastName,
                     x.PostalCode,
                     x.StateCode,
                     Id = x.Id,
                     x.EmployeeCode,
                     UserRole = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                     Designation = db.MasterDesignations.Where(a => a.Id == x.MasterDesignationId).Select(n => n.Designation).FirstOrDefault(),
                     StateManagerName = (from m in db.Users
                                         join n in db.UserTeams on x.Id equals n.ToUserId
                                         where m.Id == n.FromUsersId && m.ConfigUserRoleId == 2
                                         orderby m.Id descending
                                         select new
                                         {
                                             m.FirstName,
                                             m.LastName,
                                             m.Id
                                         }).FirstOrDefault(),
                 }).ToList();

                    if (_responce.Count == 0)
                    {
                        responseObj.responseCode = 500;
                        responseObj.isSuccess = false;
                        responseObj.data = "Data not found";
                        return responseObj;
                    }
                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = _responce;
                    return responseObj;
                }
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public async Task<object> GetAllDealers(long UserRoleId, long? UserId, long? ManagerId, long? StateManagerId)
        {
            var responseObj = new ResponseObj<object>();

            try
            {
                if (UserId == 0 && ManagerId == 0 && StateManagerId == 0)
                {
                    var _responce = (from x in db.Users
                                     join y in db.ConfigUserRoles on x.ConfigUserRoleId equals y.Id
                                     join z in db.MasterDesignations on x.MasterDesignationId equals z.Id
                                     where y.Id == UserRoleId
                                     select new
                                     {
                                         x.MobileNumber,
                                         x.Status,
                                         x.StateName,
                                         x.CountryName,
                                         x.CreateDate,
                                         x.UpdateDate,
                                         x.Address,
                                         x.City,
                                         x.CountryCode,
                                         x.LoginUserName,
                                         x.EmailId,
                                         x.FirstName,
                                         x.LastName,
                                         x.PostalCode,
                                         x.StateCode,
                                         x.Id,
                                         x.EmployeeCode,
                                         x.CstNo,
                                         x.LstNo,
                                         x.ServiceTaxRegnNo,
                                         x.Pan,
                                         UserRole = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                         Designation = db.MasterDesignations.Where(a => a.Id == x.MasterDesignationId).Select(n => n.Designation).FirstOrDefault(),
                                         ManagerName = (from m in db.UserTeams
                                                        join n in db.Users on m.FromUsersId equals n.Id
                                                        where x.Id == m.ToUserId && n.ConfigUserRoleId == 3
                                                        orderby m.Id descending
                                                        select new
                                                        {
                                                            n.FirstName,
                                                            n.LastName,
                                                            n.Id
                                                        }).FirstOrDefault(),

                                     }).ToList();

                    if (_responce.Count == 0)
                    {
                        responseObj.responseCode = 500;
                        responseObj.isSuccess = false;
                        responseObj.data = "Data not found";
                        return responseObj;
                    }
                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = _responce;
                    return responseObj;
                }
                else if (ManagerId > 0 && UserId == 0 && StateManagerId == 0)
                {
                    var _responce = (from x in db.Users
                                     where x.ConfigUserRoleId == UserRoleId && db.UserTeams.Any(z => z.ToUserId == x.Id && z.FromUsersId == ManagerId)
                                     select new
                                     {
                                         x.MobileNumber,
                                         x.Status,
                                         x.StateName,
                                         x.CountryName,
                                         x.CreateDate,
                                         x.UpdateDate,
                                         x.Address,
                                         x.City,
                                         x.CountryCode,
                                         x.LoginUserName,
                                         x.EmailId,
                                         x.FirstName,
                                         x.LastName,
                                         x.PostalCode,
                                         x.StateCode,
                                         x.Id,
                                         x.EmployeeCode,
                                         x.CstNo,
                                         x.LstNo,
                                         x.ServiceTaxRegnNo,
                                         x.Pan,
                                         UserRole = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                         Designation = db.MasterDesignations.Where(a => a.Id == x.MasterDesignationId).Select(n => n.Designation).FirstOrDefault(),
                                         ManagerName = (from m in db.UserTeams
                                                        join n in db.Users on m.FromUsersId equals n.Id
                                                        where x.Id == m.ToUserId && n.ConfigUserRoleId == 3
                                                        orderby m.Id descending
                                                        select new
                                                        {
                                                            n.FirstName,
                                                            n.LastName,
                                                            n.Id
                                                        }).FirstOrDefault(),
                                     }).ToList();


                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = _responce;
                    return responseObj;
                }
                else if (ManagerId == 0 && UserId == 0 && StateManagerId > 0)
                {
                    var StateManagerRes = (from x in db.Users
                                           where x.ConfigUserRoleId == 3 && db.UserTeams.Any(z => z.ToUserId == x.Id && z.FromUsersId == StateManagerId)
                                           select new
                                           {
                                               managerId = x.Id,
                                           }).Select(d => d.managerId).ToList();

                    var _responce = (from x in db.Users
                                     join y in db.UserTeams on x.Id equals y.ToUserId
                                     where x.ConfigUserRoleId == UserRoleId && StateManagerRes.Contains((long)y.FromUsersId)
                                     select new
                                     {
                                         x.MobileNumber,
                                         x.Status,
                                         x.StateName,
                                         x.CountryName,
                                         x.CreateDate,
                                         x.UpdateDate,
                                         x.Address,
                                         x.City,
                                         x.CountryCode,
                                         x.LoginUserName,
                                         x.EmailId,
                                         x.FirstName,
                                         x.LastName,
                                         x.PostalCode,
                                         x.StateCode,
                                         x.Id,
                                         x.EmployeeCode,
                                         x.CstNo,
                                         x.LstNo,
                                         x.ServiceTaxRegnNo,
                                         x.Pan,
                                         UserRole = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                         Designation = db.MasterDesignations.Where(a => a.Id == x.MasterDesignationId).Select(n => n.Designation).FirstOrDefault(),
                                         ManagerName = (from m in db.UserTeams
                                                        join n in db.Users on m.FromUsersId equals n.Id
                                                        where x.Id == m.ToUserId && n.ConfigUserRoleId == 3
                                                        orderby m.Id descending
                                                        select new
                                                        {
                                                            n.FirstName,
                                                            n.LastName,
                                                            n.Id
                                                        }).FirstOrDefault(),
                                     }).ToList();


                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = _responce;
                    return responseObj;
                }
                else
                {
                    var _responce = (from x in db.Users
                                     join y in db.ConfigUserRoles on x.ConfigUserRoleId equals y.Id
                                     join z in db.MasterDesignations on x.MasterDesignationId equals z.Id
                                     where y.Id == UserRoleId && x.Id == UserId
                                     select new
                                     {
                                         x.MobileNumber,
                                         x.Status,
                                         x.StateName,
                                         x.CountryName,
                                         x.CreateDate,
                                         x.UpdateDate,
                                         x.Address,
                                         x.City,
                                         x.CountryCode,
                                         x.LoginUserName,
                                         x.EmailId,
                                         x.FirstName,
                                         x.LastName,
                                         x.PostalCode,
                                         x.StateCode,
                                         x.Id,
                                         x.EmployeeCode,
                                         x.CstNo,
                                         x.LstNo,
                                         x.ServiceTaxRegnNo,
                                         x.Pan,
                                         UserRole = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                         Designation = db.MasterDesignations.Where(a => a.Id == x.MasterDesignationId).Select(n => n.Designation).FirstOrDefault(),
                                         ManagerName = (from m in db.UserTeams
                                                        join n in db.Users on m.FromUsersId equals n.Id
                                                        where x.Id == m.ToUserId && n.ConfigUserRoleId == 3
                                                        orderby m.Id descending
                                                        select new
                                                        {
                                                            n.FirstName,
                                                            n.LastName,
                                                            n.Id
                                                        }).FirstOrDefault(),

                                     }).ToList();

                    if (_responce.Count == 0)
                    {
                        responseObj.responseCode = 500;
                        responseObj.isSuccess = false;
                        responseObj.data = "Data not found";
                        return responseObj;
                    }
                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = _responce;
                    return responseObj;
                }

            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }


        public async Task<object> GetAnnouncementMessage(long UserRoleId, long userId)
        {
            var responseObj = new ResponseObj<object>();

            try
            {
                List<Announcement> res = new List<Announcement>();
                List<Announcement> res1 = new List<Announcement>();

                var announcementMessage = db.Announcements.Where(x => x.ToDate >= DateTime.Now && x.AnnouncementFor == "All").OrderBy(t => t.Id).ToList();

                if(announcementMessage.Count > 0)
                {
                    foreach (var item in announcementMessage)
                    {
                        if (item.AnnouncementFor == "All")
                        {
                            if (item.AnnouncementByManagerId != 0 && UserRoleId == 3)
                            {
                                res = db.Announcements.Where(x => x.ToDate >= DateTime.Now && x.AnnouncementByManagerId == userId && x.AnnouncementFor == "All").ToList();
                            }
                            else if (item.AnnouncementByDealerId != 0 && UserRoleId == 4)
                            {
                                res = db.Announcements.Where(x => x.ToDate >= DateTime.Now && x.AnnouncementByDealerId == userId && x.AnnouncementFor == "All").ToList();
                            }
                            else
                            {
                                res = db.Announcements.Where(x => x.ToDate >= DateTime.Now && x.AnnouncementFor == "All").ToList();
                            }
                        }
                    }
                }

              
                if (UserRoleId == 2)
                {
                     res1 = db.Announcements.Where(x => x.AnnouncementFor == "StateManager" && x.ToDate >= DateTime.Now).ToList();
                }
                else if (UserRoleId == 3)
                {
                     res1 = db.Announcements.Where(x => x.AnnouncementByManagerId == userId && x.AnnouncementFor == "Manager" && x.ToDate >= DateTime.Now).ToList();
                }
                else if (UserRoleId == 4)
                {
                     res1 = db.Announcements.Where(x => x.AnnouncementByDealerId == userId && x.AnnouncementFor == "Dealers" && x.ToDate >= DateTime.Now).ToList();
                }
                else
                {
                    
                }

                var mergedArray = res.Concat(res1).Distinct().ToList().OrderByDescending(n => n.ToDate).Distinct().ToArray();

                responseObj.responseCode = 200;
                responseObj.isSuccess = true;
                responseObj.data = mergedArray;
                return responseObj;

            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }


        public async Task<object> GetAllSalesDetails(long? DealerId, long? ManagerId, long? StateManagerId, DateTime? selectRange, DateTime? fromDate, DateTime? toDate, string? MaterialGroup)
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                var filteredData = new List<SalesDetails>();

                if (DealerId == 0 && ManagerId == 0 && StateManagerId == 0)
                {

                    var _salesDetails = (from x in db.Users
                                         join z in db.Sales on x.Id equals z.UserId
                                         where z.MaterialGroup == MaterialGroup
                                         select new SalesDetails
                                         {
                                             Code = x.EmployeeCode,
                                             Name = x.FirstName,
                                             City = x.City,
                                             InvoiceNumber = z.InvoiceNumber,
                                             InvoiceDate = z.InvoiceDate,
                                             InvoiceValue = z.InvoiceAmount,
                                             StateManagerReadStatus = z.StateManagerReadStatus,
                                             ManagerReadStatus = z.ManagerReadStatus,
                                             DealerReadStatus = z.DealerReadStatus,
                                             Manager = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                             ProductCode = z.ProductCode,
                                             ProductDesc = z.ProductDesc,
                                             BatchNo = z.BatchNo,
                                             ObdNo = z.ObdNo,
                                             IfmsCode = x.IfmsCode,
                                             ActualGiQuantity = Convert.ToString(z.ActualGiQuantity),
                                             ZgeNo = z.ZgeNo,
                                             SupplyingPlantCode = z.SupplyingPlantCode,
                                             SupplyingPlantName = z.SupplyingPlantName,
                                             SupplyingPlantState = z.SupplyingPlantState,
                                             TptName = z.TptName,
                                             LrNo = z.LrNo,
                                             LrDate = Convert.ToString(z.LrDate),
                                             Salescol = z.Salescol,
                                             TruckNo = z.TruckNo,
                                             TruckCapacity = z.TruckCapacity,
                                             SoldToCode = z.SoldToCode,
                                             SoldToName = z.SoldToName,
                                             SoldToLocation = z.SoldToLocation,
                                             SoldToBlock = z.SoldToBlock,
                                             SoldToDistrict = z.SoldToDistrict,
                                             ShipToCode = z.ShipToCode,
                                             ShipToName = z.ShipToName,
                                             ShipToLocation = z.ShipToLocation,
                                             ShipToBlock = z.ShipToBlock,
                                             ShipToDistrict = z.ShipToDistrict,
                                             InvoiceValueWithoutGst = z.InvoiceValueWithoutGst,
                                             InvoiceValueWithGst = z.InvoiceValueWithGst,
                                             Gst = z.Gst,
                                             MaterialGroup = z.MaterialGroup
                                         }).ToList();

                    if (selectRange != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= selectRange).ToList();
                    }
                    else if (fromDate != null && toDate != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= fromDate && x.InvoiceDate <= toDate).ToList();
                    }
                    else if (fromDate != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= fromDate).ToList();
                    }
                     else
                    {
                        filteredData = _salesDetails.ToList();
                    }

                    #region
                    //if (_salesDetails.Count > 0)
                    //{
                    //    var result = new List<SalesDetails>();

                    //    if (searchText != null)
                    //    {
                    //        result = _salesDetails
                    //               .Where(item =>
                    //                   (item.Code?.Contains(searchText, StringComparison.OrdinalIgnoreCase) ?? false) ||
                    //                  (item.Name?.Contains(searchText, StringComparison.OrdinalIgnoreCase) ?? false) ||
                    //                  (item.City?.Contains(searchText, StringComparison.OrdinalIgnoreCase) ?? false) ||
                    //                  (item.Manager?.Contains(searchText, StringComparison.OrdinalIgnoreCase) ?? false) ||
                    //                  (item.InvoiceNumber?.Contains(searchText, StringComparison.OrdinalIgnoreCase) ?? false)).ToList();

                    //        if (result == null)
                    //        {
                    //            responseObj.responseCode = 400;
                    //            responseObj.isSuccess = false;
                    //            responseObj.data = "Data not found";
                    //            return responseObj;
                    //        }
                    //        responseObj.responseCode = 200;
                    //        responseObj.isSuccess = true;
                    //        responseObj.data = result;
                    //        return responseObj;
                    //    }
                    //    else
                    //    {
                    //        if (CityName != null && Region != null && Manager != null && InvoiceNo != null)
                    //        {
                    //            result = _salesDetails.Where(x => x.City == CityName && x.Manager == Manager && x.InvoiceNumber == InvoiceNo).ToList();
                    //        }
                    //        else if (CityName != null && Region != null && Manager != null)
                    //        {
                    //            result = _salesDetails.Where(x => x.City == CityName && x.Manager == Manager).ToList();
                    //        }
                    //        if (CityName != null && Manager != null)
                    //        {
                    //            result = _salesDetails.Where(x => x.City == CityName && x.Manager == Manager).ToList();
                    //        }
                    //        if (CityName != null && Region != null)
                    //        {
                    //            result = _salesDetails.Where(x => x.City == CityName).ToList();
                    //        }
                    //        else if (CityName != null)
                    //        {
                    //            result = _salesDetails.Where(x => x.City == CityName).ToList();
                    //        }
                    //        else if (Manager != null)
                    //        {
                    //            result = _salesDetails.Where(x => x.Manager == Manager).ToList();
                    //        }
                    //        else if (InvoiceNo != null)
                    //        {
                    //            result = _salesDetails.Where(x => x.InvoiceNumber == InvoiceNo).ToList();
                    //        }
                    //        else
                    //        {
                    //            result = _salesDetails.ToList();
                    //        }
                    //        responseObj.responseCode = 200;
                    //        responseObj.isSuccess = true;
                    //        responseObj.data = result;
                    //        return responseObj;
                    //    }
                    //}
                    #endregion
                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = filteredData;
                    return responseObj;
                }
                else if (DealerId > 0 && ManagerId == 0 && StateManagerId == 0)
                {
                    var _salesDetails = (from x in db.Users
                                         join z in db.Sales on x.Id equals z.UserId
                                         where z.UserId == DealerId && z.MaterialGroup == MaterialGroup
                                         select new SalesDetails
                                         {
                                             Code = x.EmployeeCode,
                                             Name = x.FirstName,
                                             City = x.City,
                                             InvoiceNumber = z.InvoiceNumber,
                                             IfmsCode = x.IfmsCode,
                                             InvoiceDate = z.InvoiceDate,
                                             InvoiceValue = z.InvoiceAmount,
                                             StateManagerReadStatus = z.StateManagerReadStatus,
                                             ManagerReadStatus = z.ManagerReadStatus,
                                             DealerReadStatus = z.DealerReadStatus,
                                             Manager = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                             ProductCode = z.ProductCode,
                                             ProductDesc = z.ProductDesc,
                                             BatchNo = z.BatchNo,
                                             ObdNo = z.ObdNo,
                                             ActualGiQuantity = Convert.ToString(z.ActualGiQuantity),
                                             ZgeNo = z.ZgeNo,
                                             SupplyingPlantCode = z.SupplyingPlantCode,
                                             SupplyingPlantName = z.SupplyingPlantName,
                                             SupplyingPlantState = z.SupplyingPlantState,
                                             TptName = z.TptName,
                                             LrNo = z.LrNo,
                                             LrDate = Convert.ToString(z.LrDate),
                                             Salescol = z.Salescol,
                                             TruckNo = z.TruckNo,
                                             TruckCapacity = z.TruckCapacity,
                                             SoldToCode = z.SoldToCode,
                                             SoldToName = z.SoldToName,
                                             SoldToLocation = z.SoldToLocation,
                                             SoldToBlock = z.SoldToBlock,
                                             SoldToDistrict = z.SoldToDistrict,
                                             ShipToCode = z.ShipToCode,
                                             ShipToName = z.ShipToName,
                                             ShipToLocation = z.ShipToLocation,
                                             ShipToBlock = z.ShipToBlock,
                                             ShipToDistrict = z.ShipToDistrict,
                                             InvoiceValueWithoutGst = z.InvoiceValueWithoutGst,
                                             InvoiceValueWithGst = z.InvoiceValueWithGst,
                                             Gst = z.Gst,
                                             MaterialGroup = z.MaterialGroup
                                         }).ToList();

                    if (selectRange != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= selectRange).ToList();
                    }
                    else if (fromDate != null && toDate != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= fromDate && x.InvoiceDate <= toDate).ToList();
                    }
                    else if (fromDate != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= fromDate).ToList();
                    }
                    else
                    {
                        filteredData = _salesDetails.ToList();
                    }

                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = filteredData;
                    return responseObj;
                }
                else if (DealerId == 0 && ManagerId > 0 && StateManagerId == 0)
                {
                    var ManagerRes = (from x in db.Users
                                      where x.ConfigUserRoleId == 4 && db.UserTeams.Any(z => z.ToUserId == x.Id && z.FromUsersId == ManagerId)
                                      select new
                                      {
                                          DealerId = x.Id,
                                      }).Select(d => d.DealerId).ToList();

                    var _salesDetails = (from x in db.Users
                                         join z in db.Sales on x.Id equals z.UserId
                                         where x.ConfigUserRoleId == 4 && ManagerRes.Contains((long)z.UserId) && z.MaterialGroup == MaterialGroup
                                         select new SalesDetails
                                         {
                                             Code = x.EmployeeCode,
                                             Name = x.FirstName,
                                             City = x.City,
                                             InvoiceNumber = z.InvoiceNumber,
                                             InvoiceDate = z.InvoiceDate,
                                             InvoiceValue = z.InvoiceAmount,
                                             StateManagerReadStatus = z.StateManagerReadStatus,
                                             ManagerReadStatus = z.ManagerReadStatus,
                                             DealerReadStatus = z.DealerReadStatus,
                                             Manager = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                             ProductCode = z.ProductCode,
                                             ProductDesc = z.ProductDesc,
                                             BatchNo = z.BatchNo,
                                             IfmsCode = x.IfmsCode,
                                             ObdNo = z.ObdNo,
                                             ActualGiQuantity = Convert.ToString(z.ActualGiQuantity),
                                             ZgeNo = z.ZgeNo,
                                             SupplyingPlantCode = z.SupplyingPlantCode,
                                             SupplyingPlantName = z.SupplyingPlantName,
                                             SupplyingPlantState = z.SupplyingPlantState,
                                             TptName = z.TptName,
                                             LrNo = z.LrNo,
                                             LrDate = Convert.ToString(z.LrDate),
                                             Salescol = z.Salescol,
                                             TruckNo = z.TruckNo,
                                             TruckCapacity = z.TruckCapacity,
                                             SoldToCode = z.SoldToCode,
                                             SoldToName = z.SoldToName,
                                             SoldToLocation = z.SoldToLocation,
                                             SoldToBlock = z.SoldToBlock,
                                             SoldToDistrict = z.SoldToDistrict,
                                             ShipToCode = z.ShipToCode,
                                             ShipToName = z.ShipToName,
                                             ShipToLocation = z.ShipToLocation,
                                             ShipToBlock = z.ShipToBlock,
                                             ShipToDistrict = z.ShipToDistrict,
                                             InvoiceValueWithoutGst = z.InvoiceValueWithoutGst,
                                             InvoiceValueWithGst = z.InvoiceValueWithGst,
                                             Gst = z.Gst,
                                             MaterialGroup = z.MaterialGroup
                                         }).ToList();

                    if (selectRange != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= selectRange).ToList();
                    }
                    else if (fromDate != null && toDate != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= fromDate && x.InvoiceDate <= toDate).ToList();
                    }
                    else if (fromDate != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= fromDate).ToList();
                    }
                    else
                    {
                        filteredData = _salesDetails.ToList();
                    }


                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = filteredData;
                    return responseObj;
                }
                else if (DealerId == 0 && ManagerId == 0 && StateManagerId > 0)
                {
                    var StateManagerRes = (from x in db.Users
                                           where x.ConfigUserRoleId == 3 && db.UserTeams.Any(z => z.ToUserId == x.Id && z.FromUsersId == StateManagerId)
                                           select new
                                           {
                                               managerId = x.Id,
                                           }).Select(d => d.managerId).ToList();

                    var _responce = (from x in db.Users
                                     join y in db.UserTeams on x.Id equals y.ToUserId
                                     where x.ConfigUserRoleId == 4 && StateManagerRes.Contains((long)y.FromUsersId)
                                     select new
                                     {
                                         dealerId = x.Id,
                                     }).Select(d => d.dealerId).ToList();

                    var _salesDetails = (from x in db.Users
                                         join z in db.Sales on x.Id equals z.UserId
                                         where x.ConfigUserRoleId == 4 && _responce.Contains((long)z.UserId) && z.MaterialGroup == MaterialGroup
                                         select new SalesDetails
                                         {
                                             Code = x.EmployeeCode,
                                             Name = x.FirstName,
                                             City = x.City,
                                             InvoiceNumber = z.InvoiceNumber,
                                             InvoiceDate = z.InvoiceDate,
                                             InvoiceValue = z.InvoiceAmount,
                                             StateManagerReadStatus = z.StateManagerReadStatus,
                                             ManagerReadStatus = z.ManagerReadStatus,
                                             DealerReadStatus = z.DealerReadStatus,
                                             Manager = db.ConfigUserRoles.Where(a => a.Id == x.ConfigUserRoleId).Select(n => n.RoleName).FirstOrDefault(),
                                             ProductCode = z.ProductCode,
                                             ProductDesc = z.ProductDesc,
                                             BatchNo = z.BatchNo,
                                             IfmsCode = x.IfmsCode,
                                             ObdNo = z.ObdNo,
                                             ActualGiQuantity = Convert.ToString(z.ActualGiQuantity),
                                             ZgeNo = z.ZgeNo,
                                             SupplyingPlantCode = z.SupplyingPlantCode,
                                             SupplyingPlantName = z.SupplyingPlantName,
                                             SupplyingPlantState = z.SupplyingPlantState,
                                             TptName = z.TptName,
                                             LrNo = z.LrNo,
                                             LrDate = Convert.ToString(z.LrDate),
                                             Salescol = z.Salescol,
                                             TruckNo = z.TruckNo,
                                             TruckCapacity = z.TruckCapacity,
                                             SoldToCode = z.SoldToCode,
                                             SoldToName = z.SoldToName,
                                             SoldToLocation = z.SoldToLocation,
                                             SoldToBlock = z.SoldToBlock,
                                             SoldToDistrict = z.SoldToDistrict,
                                             ShipToCode = z.ShipToCode,
                                             ShipToName = z.ShipToName,
                                             ShipToLocation = z.ShipToLocation,
                                             ShipToBlock = z.ShipToBlock,
                                             ShipToDistrict = z.ShipToDistrict,
                                             InvoiceValueWithoutGst = z.InvoiceValueWithoutGst,
                                             InvoiceValueWithGst = z.InvoiceValueWithGst,
                                             Gst = z.Gst,
                                             MaterialGroup = z.MaterialGroup
                                         }).ToList();


                    if (selectRange != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= selectRange).ToList();
                    }
                    else if (fromDate != null && toDate != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= fromDate && x.InvoiceDate <= toDate).ToList();
                    }
                    else if (fromDate != null)
                    {
                        filteredData = _salesDetails.Where(x => x.InvoiceDate >= fromDate).ToList();
                    }
                    else
                    {
                        filteredData = _salesDetails.ToList();
                    }

                    responseObj.responseCode = 200;
                    responseObj.isSuccess = true;
                    responseObj.data = filteredData;
                    return responseObj;
                }
                else
                {
                    responseObj.responseCode = 400;
                    responseObj.isSuccess = false;
                    responseObj.data = "Error occured";
                    return responseObj;
                }

            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }


        public async Task<object> GetAllSalesDetailsByMaterialGroup()
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                var filteredData = new List<SalesDetails>();


                    var _salesDetails = (from x in db.Users
                                         join z in db.Sales on x.Id equals z.UserId
                                         where z.MaterialGroup != "0"
                                         select new SalesDetails
                                         {
                                             MaterialGroup = z.MaterialGroup
                                         }).Distinct().ToList();

                var groupedList = _salesDetails.GroupBy(x => x.MaterialGroup)
                                    .Select(group => new
                                    {
                                        MaterialGroup = group.Key,
                                        AverageQuantity = group.Select(x => x.MaterialGroup)
                                    }).Distinct().ToList();

                responseObj.responseCode = 200;
                responseObj.isSuccess = true;
                responseObj.data = groupedList;
                return responseObj;

            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

    }
}
