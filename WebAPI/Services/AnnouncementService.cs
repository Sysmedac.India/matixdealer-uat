﻿using MatixDealer.Controllers;
using MatixDealer.Models;
using Microsoft.Extensions.Options;
using SampleWebApplication1.Utils;
using static SampleWebApplication1.Utils.Entity;

namespace MatixDealer.Services
{
    public class AnnouncementService
    {
        matixContext db;
        ILogger<RegistrationController> logger;
        private readonly IOptions<AppConfig> appSettings;
        Utilities _Utils;
        Common cmn;
        private matixContext dbContext;
        private ILogger<LoginController> logger1;

        public AnnouncementService(matixContext _db, ILogger<RegistrationController> _logger, IOptions<AppConfig> appConfig)
        {
            db = _db;
            logger = _logger;
            appSettings = appConfig;
            _Utils = new Utilities(appConfig,db);
        }
        public ResponseObj<object> InsertOrUpdateAnnouncement(Announcements announcement)
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                if(announcement.Id == 0)
                {
                    Announcement obj = new Announcement();
                    obj.FromDate = announcement.FromDate;
                    obj.ToDate = announcement.ToDate;
                    obj.AnnouncementByManager = announcement.AnnouncementByManager;
                    obj.AnnouncementByCity = announcement.AnnouncementByCity;
                    obj.AnnouncementByDealer = announcement.AnnouncementByDealer;
                    obj.AnnouncementName = announcement.AnnouncementName;
                    obj.Announcement1 = announcement.Announcement1;
                    obj.Status = announcement.Status;
                    obj.AnnouncementByDealerId = Convert.ToInt32(announcement.AnnouncementByDealerId);
                    obj.AnnouncementByManagerId = Convert.ToInt32(announcement.AnnouncementByManagerId);
                    obj.AnnouncementFor = announcement.AnnouncementFor;

                    db.Announcements.Add(obj);
                    db.SaveChanges();

                    long? announcementId = obj.Id;

                    responseObj.isSuccess = true;
                    responseObj.message = "Success";
                    responseObj.responseCode = 200;
                    responseObj.data = announcementId;
                    return responseObj;
                }
                else
                {
                    var check = db.Announcements.Where(x => x.Id == announcement.Id).FirstOrDefault();
                    if(check != null)
                    {
                        check.FromDate = announcement.FromDate;
                        check.ToDate = announcement.ToDate;
                        check.AnnouncementByManager = announcement.AnnouncementByManager;
                        check.AnnouncementByCity = announcement.AnnouncementByCity;
                        check.AnnouncementByDealer = announcement.AnnouncementByDealer;
                       // check.AnnouncementName = announcement.AnnouncementName;
                        check.Announcement1 = announcement.Announcement1;
                        check.Status = announcement.Status;
                        check.AnnouncementByDealerId = Convert.ToInt32(announcement.AnnouncementByDealerId);
                        check.AnnouncementByManagerId = Convert.ToInt32(announcement.AnnouncementByManagerId);
                        check.AnnouncementFor = announcement.AnnouncementFor;

                        db.Announcements.Update(check);
                        db.SaveChanges();

                        long? announcementId = check.Id;

                        responseObj.isSuccess = true;
                        responseObj.message = "Success";
                        responseObj.responseCode = 200;
                        responseObj.data = announcementId;
                        return responseObj;
                    }

                    responseObj.isSuccess = false;
                    responseObj.message = "Invalid Id";
                    responseObj.responseCode = 400;
                    return responseObj;
                }
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500; responseObj.isSuccess = false; responseObj.data = ex.Message;
                return responseObj;
            }
        }
    }
}
