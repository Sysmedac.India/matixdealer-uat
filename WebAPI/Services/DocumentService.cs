﻿using MatixDealer.Controllers;
using MatixDealer.Models;
using Microsoft.Extensions.Options;
using SampleWebApplication1.Utils;
using System.Reflection.Metadata;
using static SampleWebApplication1.Utils.Entity;
using Document = MatixDealer.Models.Document;

namespace MatixDealer.Services
{
    public class DocumentService
    {
        matixContext db;
        ILogger<DocumentController> logger;
        private readonly IOptions<AppConfig> appSettings;
        Utilities _Utils;
        Common cmn;
        private matixContext dbContext;
        private ILogger<LoginController> logger1;

        public DocumentService(matixContext _db, ILogger<DocumentController> _logger, IOptions<AppConfig> appConfig)
        {
            db = _db;
            logger = _logger;
            appSettings = appConfig;
            _Utils = new Utilities(appConfig, db);
        }

        //public DocumentService(MatixContext dbContext, ILogger<LoginController> logger1, IOptions<AppConfig> appSettings)
        //{
        //    this.dbContext = dbContext;
        //    this.logger1 = logger1;
        //    this.appSettings = appSettings;
        //}

        public ResponseObj<object> InsertOrUpdateDocuments(Documents documents)
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                if (documents != null)
                {
                    foreach (var item1 in documents.DocumentDetails)
                    {
                        if (documents.UserId != null && item1.Id == 0)
                        {
                            foreach (var item in documents.DocumentDetails)
                            {
                                Document document = new Document();
                                document.UserId = documents.UserId;
                                document.DocumentName = item.DocumentName;
                                document.DocumentUrl = item.DocumentUrl;
                                document.UploadedBy = document.UserId;
                                document.UploadedDate = DateTime.Now;

                                db.Documents.Add(document);
                                db.SaveChanges();
                                long documentId = document.Id;
                            }
                        }
                        else
                        {
                            if (documents.UserId > 0 || documents.UserId != null)
                            {
                                foreach (var item in documents.DocumentDetails)
                                {
                                    var check = db.Documents.Where(x => x.Id == item.Id && x.UserId == documents.UserId).FirstOrDefault();
                                    if (check != null)
                                    {
                                        check.UserId = documents.UserId;
                                        check.DocumentName = item.DocumentName;
                                        check.DocumentUrl = item.DocumentUrl;
                                        check.UploadedBy = documents.UserId;
                                        check.UploadedDate = DateTime.Now;

                                        db.Documents.Update(check);
                                        db.SaveChanges();
                                        long documentId = check.Id;
                                    }
                                }
                            }
                        }
                    }
                    responseObj.isSuccess = true;
                    responseObj.message = "Success";
                    responseObj.responseCode = 200;
                    responseObj.data = documents.UserId;
                    return responseObj;
                }
                else
                {
                    responseObj.isSuccess = false;
                    responseObj.message = "Invalid UserId Or DocumentId";
                    responseObj.responseCode = 400;
                    responseObj.data = documents.UserId;
                    return responseObj;
                }
            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500; responseObj.isSuccess = false; responseObj.data = ex.Message;
                return responseObj;
            }
        }

        public async Task<object> GetAllDocumentDetails(long? DealerId, long? ManagerId, long? StateManagerId)
        {
            var responseObj = new ResponseObj<object>();
            try
            {
                var DocDetails = new List<DocumentDetails>();

                var _documentDetails1 = (from y in db.Documents
                                        where y.UserId == 0
                                        select new DocumentDetails
                                        {
                                            Status = "",
                                            StateName = "",
                                            FirstName = "",
                                            City = "",
                                            DealerCode="",
                                            UserId = 0,
                                            AnnouncementUserId = y.UserId,
                                            DocumentName = y.DocumentName,
                                            DocumentUrl = y.DocumentUrl
                                        }).ToList();

                if (DealerId == 0 && ManagerId == 0 && StateManagerId == 0)
                {

                    DocDetails = (from y in db.Documents
                                             join x in db.Users on y.UserId equals x.Id
                                             select new DocumentDetails
                                             {
                                                 Status = x.Status,
                                                 StateName = x.StateName,
                                                 FirstName = x.FirstName,
                                                 City = x.City,
                                                 DealerCode = x.DealerCode,
                                                 UserId = x.Id,
                                                 AnnouncementUserId = y.UserId,
                                                 DocumentName = y.DocumentName,
                                                 DocumentUrl = y.DocumentUrl
                                             }).ToList();
                }
                else if (DealerId > 0 && ManagerId == 0 && StateManagerId == 0)
                {
                    DocDetails = (from y in db.Documents
                                  join x in db.Users on y.UserId equals x.Id
                                  where y.UserId == DealerId
                                  select new DocumentDetails
                                  {
                                      Status = x.Status,
                                      StateName = x.StateName,
                                      FirstName = x.FirstName,
                                      City = x.City,
                                      DealerCode = x.DealerCode,
                                      UserId = x.Id,
                                      AnnouncementUserId = y.UserId,
                                      DocumentName = y.DocumentName,
                                      DocumentUrl = y.DocumentUrl
                                  }).ToList();
                }
                else if (DealerId == 0 && ManagerId > 0 && StateManagerId == 0)
                {
                    var ManagerRes = (from x in db.Users
                                      where x.ConfigUserRoleId == 4 && db.UserTeams.Any(z => z.ToUserId == x.Id && z.FromUsersId == ManagerId)
                                      select new
                                      {
                                          DealerId = x.Id,
                                      }).Select(d => d.DealerId).ToList();

                    var _dealerDetails = (from x in db.Users
                                         join z in db.UserTeams on x.Id equals z.FromUsersId
                                         where x.ConfigUserRoleId == 4 && ManagerRes.Contains((long)z.ToUserId)
                                         select new 
                                         {
                                            
                                   
                                         }).ToList();

                    DocDetails = (from y in db.Documents
                                  join x in db.Users on y.UserId equals x.Id
                                  where ManagerRes.Contains((long)y.UserId)
                                  select new DocumentDetails
                                  {
                                      Status = x.Status,
                                      StateName = x.StateName,
                                      FirstName = x.FirstName,
                                      City = x.City,
                                      DealerCode = x.DealerCode,
                                      UserId = x.Id,
                                      AnnouncementUserId = y.UserId,
                                      DocumentName = y.DocumentName,
                                      DocumentUrl = y.DocumentUrl
                                  }).ToList();

                }
                else if (DealerId == 0 && ManagerId == 0 && StateManagerId > 0)
                {
                    var StateManagerRes = (from x in db.Users
                                           where x.ConfigUserRoleId == 3 && db.UserTeams.Any(z => z.ToUserId == x.Id && z.FromUsersId == StateManagerId)
                                           select new
                                           {
                                               managerId = x.Id,
                                           }).Select(d => d.managerId).ToList();

                    var _responce = (from x in db.Users
                                     join y in db.UserTeams on x.Id equals y.ToUserId
                                     where x.ConfigUserRoleId == 4 && StateManagerRes.Contains((long)y.FromUsersId)
                                     select new
                                     {
                                         dealerId = x.Id,
                                     }).Select(d => d.dealerId).ToList();

                    DocDetails = (from y in db.Documents
                                  join x in db.Users on y.UserId equals x.Id
                                  where _responce.Contains((long)y.UserId)
                                  select new DocumentDetails
                                  {
                                      Status = x.Status,
                                      StateName = x.StateName,
                                      FirstName = x.FirstName,
                                      City = x.City,
                                      DealerCode = x.DealerCode,
                                      UserId = x.Id,
                                      AnnouncementUserId = y.UserId,
                                      DocumentName = y.DocumentName,
                                      DocumentUrl = y.DocumentUrl
                                  }).ToList();

                }
                else
                {
                    responseObj.responseCode = 400;
                    responseObj.isSuccess = false;
                    responseObj.data = "Error occured";
                    return responseObj;
                }

                var mergedArray = _documentDetails1.Concat(DocDetails).Distinct().ToList().OrderByDescending(x => x.DealerCode).Distinct().ToArray();

                responseObj.responseCode = 200;
                responseObj.isSuccess = true;
                responseObj.data = mergedArray;
                return responseObj;


            }
            catch (Exception ex)
            {
                responseObj.responseCode = 500;
                responseObj.isSuccess = false;
                responseObj.data = ex.Message;
                return responseObj;
            }
        }

    }
}
