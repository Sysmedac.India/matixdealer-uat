﻿using System;
using System.Collections.Generic;

namespace MatixDealer.Models
{
    public partial class Document
    {
        public long Id { get; set; }
        public long? UserId { get; set; }
        public string? DocumentName { get; set; }
        public string? DocumentUrl { get; set; }
        public DateTime? UploadedDate { get; set; }
        public long? UploadedBy { get; set; }
    }
}
