﻿using System;
using System.Collections.Generic;

namespace MatixDealer.Models
{
    public partial class Sale
    {
        public long Id { get; set; }
        public long? UserId { get; set; }
        public string? InvoiceNumber { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public string? StateManagerReadStatus { get; set; }
        public string? ManagerReadStatus { get; set; }
        public string? DealerReadStatus { get; set; }
        public string? ProductCode { get; set; }
        public string? ProductDesc { get; set; }
        public string? BatchNo { get; set; }
        public string? ObdNo { get; set; }
        public decimal? ActualGiQuantity { get; set; }
        public string? ZgeNo { get; set; }
        public string? SupplyingPlantCode { get; set; }
        public string? SupplyingPlantName { get; set; }
        public string? SupplyingPlantState { get; set; }
        public string? TptName { get; set; }
        public string? LrNo { get; set; }
        public DateTime? LrDate { get; set; }
        public string? Salescol { get; set; }
        public string? TruckNo { get; set; }
        public string? TruckCapacity { get; set; }
        public string? SoldToCode { get; set; }
        public string? SoldToName { get; set; }
        public string? SoldToLocation { get; set; }
        public string? SoldToBlock { get; set; }
        public string? SoldToDistrict { get; set; }
        public string? ShipToCode { get; set; }
        public string? ShipToName { get; set; }
        public string? ShipToLocation { get; set; }
        public string? ShipToBlock { get; set; }
        public string? ShipToDistrict { get; set; }
        public decimal? InvoiceValueWithoutGst { get; set; }
        public decimal? InvoiceValueWithGst { get; set; }
        public decimal? Gst { get; set; }
        public string? MaterialGroup { get; set; }

        public virtual User? User { get; set; }
    }
}
