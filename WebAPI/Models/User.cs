﻿using System;
using System.Collections.Generic;

namespace MatixDealer.Models
{
    public partial class User
    {
        public User()
        {
            AccountStatements = new HashSet<AccountStatement>();
            Authentications = new HashSet<Authentication>();
            Sales = new HashSet<Sale>();
        }

        public long Id { get; set; }
        public long? ConfigUserRoleId { get; set; }
        public long? MasterDesignationId { get; set; }
        public string? EmployeeCode { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? MobileNumber { get; set; }
        public string? EmailId { get; set; }
        public string? Address { get; set; }
        public string? CountryName { get; set; }
        public string? CountryCode { get; set; }
        public string? StateName { get; set; }
        public string? StateCode { get; set; }
        public string? City { get; set; }
        public string? PostalCode { get; set; }
        public string? Status { get; set; }
        public string? LoginUserName { get; set; }
        public string? LoginPassword { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string? DealerCode { get; set; }
        public string? CstNo { get; set; }
        public string? LstNo { get; set; }
        public string? ServiceTaxRegnNo { get; set; }
        public string? Pan { get; set; }
        public string? IfmsCode { get; set; }

        public virtual ConfigUserRole? ConfigUserRole { get; set; }
        public virtual MasterDesignation? MasterDesignation { get; set; }
        public virtual ICollection<AccountStatement> AccountStatements { get; set; }
        public virtual ICollection<Authentication> Authentications { get; set; }
        public virtual ICollection<Sale> Sales { get; set; }
    }
}
