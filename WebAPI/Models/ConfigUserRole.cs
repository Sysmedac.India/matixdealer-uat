﻿using System;
using System.Collections.Generic;

namespace MatixDealer.Models
{
    public partial class ConfigUserRole
    {
        public ConfigUserRole()
        {
            Users = new HashSet<User>();
        }

        public long Id { get; set; }
        public string? RoleName { get; set; }
        public string? AccessMenu { get; set; }
        public long? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string? Status { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
