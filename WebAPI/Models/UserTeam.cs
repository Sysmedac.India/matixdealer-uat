﻿using System;
using System.Collections.Generic;

namespace MatixDealer.Models
{
    public partial class UserTeam
    {
        public long Id { get; set; }
        public long? FromUsersId { get; set; }
        public long? ToUserId { get; set; }
    }
}
