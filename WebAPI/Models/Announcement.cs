﻿using System;
using System.Collections.Generic;

namespace MatixDealer.Models
{
    public partial class Announcement
    {
        public long Id { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string? AnnouncementByManager { get; set; }
        public string? AnnouncementByCity { get; set; }
        public string? AnnouncementByDealer { get; set; }
        public string? Announcement1 { get; set; }
        public string? Status { get; set; }
        public string? AnnouncementName { get; set; }
        public long? AnnouncementByManagerId { get; set; }
        public string? AnnouncementFor { get; set; }
        public long? AnnouncementByDealerId { get; set; }
    }
}
