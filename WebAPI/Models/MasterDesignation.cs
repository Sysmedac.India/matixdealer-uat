﻿using System;
using System.Collections.Generic;

namespace MatixDealer.Models
{
    public partial class MasterDesignation
    {
        public MasterDesignation()
        {
            Users = new HashSet<User>();
        }

        public long Id { get; set; }
        public string? Designation { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
