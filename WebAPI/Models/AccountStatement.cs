﻿using System;
using System.Collections.Generic;

namespace MatixDealer.Models
{
    public partial class AccountStatement
    {
        public long Id { get; set; }
        public long? UserId { get; set; }
        public string? InvoiceNumber { get; set; }
        public DateTime? InvoiveDate { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string? PdfUrl { get; set; }

        public virtual User? User { get; set; }
    }
}
