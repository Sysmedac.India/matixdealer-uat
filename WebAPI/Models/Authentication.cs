﻿using System;
using System.Collections.Generic;

namespace MatixDealer.Models
{
    public partial class Authentication
    {
        public long Id { get; set; }
        public string? AccessToken { get; set; }
        public long? UserId { get; set; }
        public string? UserType { get; set; }
        public DateTime? ExpiryTime { get; set; }
        public DateTime? CreatedAt { get; set; }

        public virtual User? User { get; set; }
    }
}
