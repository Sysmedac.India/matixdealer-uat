﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MatixDealer.Models
{
    public partial class matixContext : DbContext
    {
        public matixContext()
        {
        }

        public matixContext(DbContextOptions<matixContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccountStatement> AccountStatements { get; set; } = null!;
        public virtual DbSet<Announcement> Announcements { get; set; } = null!;
        public virtual DbSet<Authentication> Authentications { get; set; } = null!;
        public virtual DbSet<ConfigUserRole> ConfigUserRoles { get; set; } = null!;
        public virtual DbSet<Document> Documents { get; set; } = null!;
        public virtual DbSet<MasterDesignation> MasterDesignations { get; set; } = null!;
        public virtual DbSet<Sale> Sales { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;
        public virtual DbSet<UserTeam> UserTeams { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=68.178.168.248;port=8802;database=matix;user id=matix1;password=Matix@123;sslmode=none", Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.34-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.UseCollation("utf8mb4_0900_ai_ci")
                .HasCharSet("utf8mb4");

            modelBuilder.Entity<AccountStatement>(entity =>
            {
                entity.ToTable("AccountStatement");

                entity.HasIndex(e => e.UserId, "UserFkId_idx");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.InvoiceAmount).HasPrecision(10, 2);

                entity.Property(e => e.InvoiceNumber).HasMaxLength(200);

                entity.Property(e => e.InvoiveDate).HasColumnType("datetime");

                entity.Property(e => e.PdfUrl).HasMaxLength(250);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AccountStatements)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("UserFkId");
            });

            modelBuilder.Entity<Announcement>(entity =>
            {
                entity.ToTable("Announcement");

                entity.Property(e => e.Announcement1)
                    .HasMaxLength(1000)
                    .HasColumnName("Announcement");

                entity.Property(e => e.AnnouncementByCity).HasMaxLength(250);

                entity.Property(e => e.AnnouncementByDealer).HasMaxLength(250);

                entity.Property(e => e.AnnouncementByManager).HasMaxLength(250);

                entity.Property(e => e.AnnouncementFor).HasMaxLength(45);

                entity.Property(e => e.AnnouncementName).HasMaxLength(100);

                entity.Property(e => e.FromDate).HasColumnType("datetime");

                entity.Property(e => e.Status).HasMaxLength(20);

                entity.Property(e => e.ToDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Authentication>(entity =>
            {
                entity.ToTable("Authentication");

                entity.HasIndex(e => e.UserId, "User_FK_idx");

                entity.Property(e => e.AccessToken).HasMaxLength(500);

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.ExpiryTime).HasColumnType("datetime");

                entity.Property(e => e.UserType).HasMaxLength(150);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Authentications)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("User_FK");
            });

            modelBuilder.Entity<ConfigUserRole>(entity =>
            {
                entity.ToTable("ConfigUserRole");

                entity.Property(e => e.AccessMenu).HasMaxLength(1000);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.RoleName).HasMaxLength(100);

                entity.Property(e => e.Status).HasMaxLength(20);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<Document>(entity =>
            {
                entity.Property(e => e.DocumentName).HasMaxLength(1000);

                entity.Property(e => e.DocumentUrl).HasMaxLength(250);

                entity.Property(e => e.UploadedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<MasterDesignation>(entity =>
            {
                entity.ToTable("MasterDesignation");

                entity.Property(e => e.Designation).HasMaxLength(200);
            });

            modelBuilder.Entity<Sale>(entity =>
            {
                entity.HasIndex(e => e.UserId, "fk_user_Sale_idx");

                entity.Property(e => e.ActualGiQuantity)
                    .HasPrecision(10, 2)
                    .HasColumnName("actualGiQuantity");

                entity.Property(e => e.BatchNo)
                    .HasMaxLength(100)
                    .HasColumnName("batchNo");

                entity.Property(e => e.DealerReadStatus).HasMaxLength(45);

                entity.Property(e => e.Gst).HasColumnName("gst");

                entity.Property(e => e.InvoiceAmount).HasPrecision(25, 2);

                entity.Property(e => e.InvoiceDate).HasColumnType("datetime");

                entity.Property(e => e.InvoiceNumber).HasMaxLength(100);

                entity.Property(e => e.InvoiceValueWithGst).HasColumnName("invoiceValueWithGst");

                entity.Property(e => e.InvoiceValueWithoutGst).HasColumnName("invoiceValueWithoutGst");

                entity.Property(e => e.LrDate)
                    .HasColumnType("datetime")
                    .HasColumnName("lrDate");

                entity.Property(e => e.LrNo)
                    .HasMaxLength(45)
                    .HasColumnName("lrNo");

                entity.Property(e => e.ManagerReadStatus).HasMaxLength(45);

                entity.Property(e => e.MaterialGroup)
                    .HasMaxLength(100)
                    .HasColumnName("materialGroup");

                entity.Property(e => e.ObdNo)
                    .HasMaxLength(100)
                    .HasColumnName("obdNo");

                entity.Property(e => e.ProductCode)
                    .HasMaxLength(100)
                    .HasColumnName("productCode");

                entity.Property(e => e.ProductDesc)
                    .HasMaxLength(250)
                    .HasColumnName("productDesc");

                entity.Property(e => e.Salescol).HasMaxLength(45);

                entity.Property(e => e.ShipToBlock)
                    .HasMaxLength(100)
                    .HasColumnName("shipToBlock");

                entity.Property(e => e.ShipToCode)
                    .HasMaxLength(45)
                    .HasColumnName("shipToCode");

                entity.Property(e => e.ShipToDistrict)
                    .HasMaxLength(100)
                    .HasColumnName("shipToDistrict");

                entity.Property(e => e.ShipToLocation)
                    .HasMaxLength(100)
                    .HasColumnName("shipToLocation");

                entity.Property(e => e.ShipToName)
                    .HasMaxLength(100)
                    .HasColumnName("shipToName");

                entity.Property(e => e.SoldToBlock)
                    .HasMaxLength(100)
                    .HasColumnName("soldToBlock");

                entity.Property(e => e.SoldToCode)
                    .HasMaxLength(45)
                    .HasColumnName("soldToCode");

                entity.Property(e => e.SoldToDistrict)
                    .HasMaxLength(100)
                    .HasColumnName("soldToDistrict");

                entity.Property(e => e.SoldToLocation)
                    .HasMaxLength(100)
                    .HasColumnName("soldToLocation");

                entity.Property(e => e.SoldToName)
                    .HasMaxLength(100)
                    .HasColumnName("soldToName");

                entity.Property(e => e.StateManagerReadStatus).HasMaxLength(45);

                entity.Property(e => e.SupplyingPlantCode)
                    .HasMaxLength(100)
                    .HasColumnName("supplyingPlantCode");

                entity.Property(e => e.SupplyingPlantName)
                    .HasMaxLength(250)
                    .HasColumnName("supplyingPlantName");

                entity.Property(e => e.SupplyingPlantState)
                    .HasMaxLength(100)
                    .HasColumnName("supplyingPlantState");

                entity.Property(e => e.TptName)
                    .HasMaxLength(100)
                    .HasColumnName("tptName");

                entity.Property(e => e.TruckCapacity)
                    .HasMaxLength(45)
                    .HasColumnName("truckCapacity");

                entity.Property(e => e.TruckNo)
                    .HasMaxLength(45)
                    .HasColumnName("truckNo");

                entity.Property(e => e.ZgeNo)
                    .HasMaxLength(100)
                    .HasColumnName("zgeNo");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Sales)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("fk_user_Sale");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.ConfigUserRoleId, "ConfigUserRole_idx");

                entity.HasIndex(e => e.MasterDesignationId, "MasterDesgination_idx");

                entity.Property(e => e.Address).HasMaxLength(100);

                entity.Property(e => e.City).HasMaxLength(250);

                entity.Property(e => e.CountryCode).HasMaxLength(10);

                entity.Property(e => e.CountryName).HasMaxLength(100);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.CstNo).HasMaxLength(45);

                entity.Property(e => e.DealerCode).HasMaxLength(45);

                entity.Property(e => e.EmailId).HasMaxLength(100);

                entity.Property(e => e.EmployeeCode).HasMaxLength(100);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.IfmsCode).HasMaxLength(100);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.LoginPassword).HasMaxLength(250);

                entity.Property(e => e.LoginUserName).HasMaxLength(150);

                entity.Property(e => e.LstNo).HasMaxLength(45);

                entity.Property(e => e.MobileNumber).HasMaxLength(20);

                entity.Property(e => e.Pan).HasMaxLength(45);

                entity.Property(e => e.PostalCode).HasMaxLength(20);

                entity.Property(e => e.ServiceTaxRegnNo).HasMaxLength(45);

                entity.Property(e => e.StateCode).HasMaxLength(10);

                entity.Property(e => e.StateName).HasMaxLength(100);

                entity.Property(e => e.Status).HasMaxLength(20);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ConfigUserRole)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.ConfigUserRoleId)
                    .HasConstraintName("ConfigUserRole");

                entity.HasOne(d => d.MasterDesignation)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.MasterDesignationId)
                    .HasConstraintName("MasterDesgination");
            });

            modelBuilder.Entity<UserTeam>(entity =>
            {
                entity.ToTable("UserTeam");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
