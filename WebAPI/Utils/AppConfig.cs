﻿namespace SampleWebApplication1.Utils
{
    public class AppConfig
    {
        public string? successMessage { get; set; }
        public string? errorMessage { get; set; }
    }
}
