﻿namespace SampleWebApplication1.Utils
{
    public class AppSettings
    {
        public string? Secret { get; set; }
        public int expirationInMinutes { get; set; }
    }
}
