﻿namespace SampleWebApplication1.Utils
{
    public class Entity
    {
        public class ResponseObj<T> where T : class
        {
            public int responseCode { get; set; }
            public bool isSuccess { get; set; }
            public object? data { get; set; }
            public string? message { get; set; }
        }

        public class InvoiceRes
        {
            public string? CountryName { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? startDate { get; set; }
            public DateTime? InvoiceDate { get; set; }
            public string? City { get; set; }
            public string? CountryCode { get; set; }
            public string? LoginUserName { get; set; }
            public string? EmailId { get; set; }
            public long? UserId { get; set; }
            public long? InvoiceId { get; set; }
            public string? DealerCode { get; set; }
            public decimal? InvoiceAmount { get; set; }
            public string? InvoiceNumber { get; set; }
            public string? UserRole { get; set; }
            public string? FirstName { get; set; }
            public string? PdfUrl { get; set; }
        }


        public class StateManagerRes
        {
            public string? MobileNumber { get; set; }
            public string? Status { get; set; }
            public string? StateName { get; set; }
            public string? CountryName { get; set; }
            public DateTime? CreateDate { get; set; }
            public DateTime? UpdateDate { get; set; }
            public string? Address { get; set; }
            public string? City { get; set; }
            public string? CountryCode { get; set; }
            public string? LoginUserName { get; set; }
            public string? EmailId { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? PostalCode { get; set; }
            public string? StateCode { get; set; }
            public long? Id { get; set; }
            public string? EmployeeCode { get; set; }
            public string? UserRole { get; set; }
            public string? Designation { get; set; }
            public string? DealerCode { get; set; }
            public string? cstNo { get; set; }
            public string? lstNo { get; set; }
            public string? serviceTaxRegnNo { get; set; }
            public string? pan { get; set; }
            public string? ifmsCode { get; set; }
        }

        public class AnnouncementMessage
        {
            public string? Message { get; set; }
        }

        public class DocumentDetails
        {
            public long? AnnouncementUserId { get; set; }
            public long? UserId { get; set; }
            public string? Status { get; set; }
            public string? StateName { get; set; }
            public string? FirstName { get; set; }
            public string? City{ get; set; }
            public string? DealerCode { get; set; }
            public string? DocumentName { get; set; }
            public string? DocumentUrl { get; set; }
        }

        public class SalesDetails
        {
            public string? Code { get; set; }
            public string? Name { get; set; }
            public string? City { get; set; }
            public string? Manager { get; set; }
            public string? InvoiceNumber { get; set; }
            public DateTime? InvoiceDate { get; set; }
            public decimal? InvoiceValue { get; set; }
            public string? StateManagerReadStatus { get; set; }
            public string? ManagerReadStatus { get; set; }
            public string? DealerReadStatus { get; set; }
            public string? ProductCode { get; set; }
            public string? ProductDesc { get; set; }
            public string? BatchNo { get; set; }
            public string? ObdNo { get; set; }
            public string? ActualGiQuantity { get; set; }
            public string? ActualGiDate { get; set; }
            public string? ShipmentNo { get; set; }
            public string? ShipmentCostNo { get; set; }
            public string? ZtrsNo { get; set; }
            public string? ZgeNo { get; set; }
            public string? SupplyingPlantCode { get; set; }
            public string? SupplyingPlantName { get; set; }
            public string? SupplyingPlantDistrict { get; set; }
            public string? SupplyingPlantState { get; set; }
            public string? ReceivingPlantCode { get; set; }
            public string? ReceivingPlantName { get; set; }
            public string? ReceivingPlantAddress { get; set; }
            public string? ReceivingPlantDistrict { get; set; }
            public string? ReceivingPlantState { get; set; }
            public string? RecPlantBlock { get; set; }
            public string? TotalFreight { get; set; }
            public string? TptCode { get; set; }
            public string? TptName { get; set; }
            public string? RrNumber { get; set; }
            public string? RrDate { get; set; }
            public string? LrNo { get; set; }
            public string? LrDate { get; set; }
            public string? RoNo { get; set; }
            public string? RoDate { get; set; }
            public string? TruckNo { get; set; }
            public string? TruckCapacity { get; set; }
            public string? Zterm { get; set; }
            public string? PaymentTerms { get; set; }
            public string? SoldToCode { get; set; }
            public string? SoldToRoomNumber { get; set; }
            public string? SoldToLocation { get; set; }
            public string? SoldToBlock { get; set; }
            public string? SoldToDistrict { get; set; }
            public string? ShipToCode { get; set; }
            public string? ShipToName { get; set; }
            public string? ShipToRoomNumber { get; set; }
            public string? ShipToLocation { get; set; }
            public string? ShipToBlock { get; set; }
            public string? ShipToDistrict { get; set; }
            public string? IrnNumber { get; set; }
            public string? EwayBill { get; set; }
            public string? Salescol { get; set; }
            public string? SoldToName { get; set; }
           public decimal? InvoiceValueWithoutGst { get; set; }
            public decimal? InvoiceValueWithGst { get; set; }
            public string? IfmsCode { get; set; }
            public decimal? Gst { get; set; }
            public string? MaterialGroup { get; set; }


        }


        public class MultipleUsers
        {
            public long? UserId { get; set; }
        }
    }

    public class UserRegister
    {
        public long Id { get; set; }
        public long? ConfigUserRoleId { get; set; }
        public long? MasterDesignationId { get; set; }
        public string? EmployeeCode { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? MobileNumber { get; set; }
        public string? EmailId { get; set; }
        public string? Address { get; set; }
        public string? CountryName { get; set; }
        public string? CountryCode { get; set; }
        public string? StateName { get; set; }
        public string? StateCode { get; set; }
        public string? City { get; set; }
        public string? PostalCode { get; set; }
        public string? Status { get; set; }
        public string? LoginUserName { get; set; }
        public string? LoginPassword { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? UpdatedBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Designation { get; set; }
        //public long? FromUsersId { get; set;}
        //public long? ToUserId { get; set;}
    }

    public class Announcements
    {
        public long Id { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string? AnnouncementByManager { get; set; }
        public string? AnnouncementByCity { get; set; }
        public string? AnnouncementByDealer { get; set; }
        public string? AnnouncementName { get; set; }
        public ulong? AnnouncementByManagerId { get; set; }
        public ulong? AnnouncementByDealerId { get; set; }
        public string? AnnouncementFor { get; set; }
        public string? Announcement1 { get; set; }
        public string? Status { get; set; }
    }

    public class UserLogin
    {
        public string LoginUserName { get; set; }
        public string LoginPassword { get; set; }

    }

    public  class Documents
    {
        public long UserId { get; set; }
        public List<DocumentDetailsList> DocumentDetails { get; set; }
    }

    public class DocumentDetailsList
    {
        public long? Id { get; set; }
        public string? DocumentName { get; set; }
        public string? DocumentUrl { get; set; }
    }

    public class AssignManager
    {
        public int? managerId { get; set; }
        public List<Assighids> assighId { get; set; }

    }

    public class Assighids
    {
        public string id { get; set; }
    }

    public class Dealer
    {
        public string customer { get; set; }
        public string title { get; set; }
        public string name { get; set; }
        public string searchTerm { get; set; }
        public string roomNumber { get; set; }
        public string customerGrp1 { get; set; }
        public string street { get; set; }
        public string district { get; set; }
        public string postalCode { get; set; }
        public string city { get; set; }
        public string region { get; set; }
        public string description { get; set; }
        public string transportZone { get; set; }
        public string timeZone { get; set; }
        public string language { get; set; }
        public string telephone { get; set; }
        public string emailMaint { get; set; }
        public string taxNumber3 { get; set; }
        public string cstNo { get; set; }
        public string lstNo { get; set; }
        public string serviceTaxRegnNo { get; set; }
        public string pan { get; set; }        
    }

    public class SalesDetails1
    {
        public string descriptionOfDeliveryType { get; set; }
        public string productCode { get; set; }
        public string productDesc { get; set; }
        public string batchNo { get; set; }
        public string obdNo { get; set; }
        public string actualGiQuantity { get; set; }
        public string actualGiDate { get; set; }
        public string shipmentNo { get; set; }
        public string shipmentCostNo { get; set; }
        public string ztrsNo { get; set; }
        public string zgeNo { get; set; }
        public string supplyingPlantCode { get; set; }
        public string supplyingPlantName { get; set; }
        public string supplyingPlantDistrict { get; set; }
        public string supplyingPlantState { get; set; }
        public string receivingPlantCode { get; set; }
        public string receivingPlantName { get; set; }
        public string receivingPlantAddress { get; set; }
        public string receivingPlantDistrict { get; set; }
        public string receivingPlantState { get; set; }
        public string recPlantBlock { get; set; }
        public string totalFreight { get; set; }
        public string tptCode { get; set; }
        public string tptName { get; set; }
        public string rrNumber { get; set; }
        public string rrDate { get; set; }
        public string lrNo { get; set; }
        public string lrDate { get; set; }
        public string roNo { get; set; }
        public string roDate { get; set; }
        public string truckNo { get; set; }
        public string truckCapacity { get; set; }
        public string invoiceNumber { get; set; }
        public string invoiceDocDate { get; set; }
        public string zterm { get; set; }
        public string paymentTerms { get; set; }
        public string soldToCode { get; set; }
        public string soldToRoomNumber { get; set; }
        public string soldToLocation { get; set; }
        public string soldToBlock { get; set; }
        public string soldToDistrict { get; set; }
        public string shipToCode { get; set; }
        public string shipToName { get; set; }
        public string shipToRoomNumber { get; set; }
        public string shipToLocation { get; set; }
        public string shipToBlock { get; set; }
        public string shipToDistrict { get; set; }
        public string irnNumber { get; set; }
        public string ewayBill { get; set; }
        public string salescol { get; set; }
        public string soldToName { get; set; }
        public decimal? InvoiceValueWithoutGst { get; set; }
        public decimal? InvoiceValueWithGst { get; set; }
        public decimal? Gst { get; set; }
        public string? MaterialGroup { get; set; }
    }
}
