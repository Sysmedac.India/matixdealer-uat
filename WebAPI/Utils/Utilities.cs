﻿using MatixDealer.Models;
using Microsoft.Extensions.Options;
using static SampleWebApplication1.Utils.Entity;

namespace SampleWebApplication1.Utils
{
    public class Utilities
    {
        private readonly IOptions<AppConfig> appSettings;
        matixContext db;

        public Utilities(IOptions<AppConfig> appConfig, matixContext dbContext)
        {
            appSettings = appConfig;
            db = dbContext;
        }

        //Password Encryption using Base64
        public string EncryptPassword(string password)
        {

            byte[] encData_byte = new byte[password.Length];
            encData_byte = System.Text.Encoding.UTF8.GetBytes(password);
            string encodedData = Convert.ToBase64String(encData_byte);

            return encodedData;
        }

        //Password Decryption using Base64
        public string DecryptPassword(string password)
        {
            System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
            System.Text.Decoder utf8Decode = encoder.GetDecoder();
            var chk = Convert.FromBase64String(password);
            byte[] todecode_byte = Convert.FromBase64String(password);
            int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            string result = new String(decoded_char);
            return result;
        }
        public class JWTSettings
        {
            public string SecretKey { get; set; }
        }
        
    }
}
