﻿using MatixDealer.Models;
using Microsoft.Extensions.Options;
using static SampleWebApplication1.Utils.Entity;

namespace SampleWebApplication1.Utils
{
    public class Common
    {

        matixContext db;
        private readonly IOptions<AppConfig> appSettings;

        public Common(matixContext _db, IOptions<AppConfig> appConfig)
        {
            db = _db;
            appSettings = appConfig;
        }

        public void LogToFile(string message)
        {
            string formattedDate = DateTime.Now.ToString("dd_MM_yyyy");
            string filePath = $"{formattedDate}_log_file.txt";
            string fullFilePath = Path.Combine(Environment.CurrentDirectory, "Logs", filePath);
            string logMessage = $"{message}\n{DateTime.Now}";

            if (File.Exists(fullFilePath))
            {
                using (StreamWriter writer = new StreamWriter(fullFilePath, true))
                {
                    writer.WriteLine(logMessage);
                }
            }
            else
            {
                using (StreamWriter writer = new StreamWriter(fullFilePath))
                {
                    writer.WriteLine(logMessage);
                }
            }
        }       
    }
}
