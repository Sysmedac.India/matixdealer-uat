import * as React from "react";
import { httpDelete, httpGet, httpPost, httpPut} from "../services/HttpService";
import { ROUTES } from "../configs/constants";


// Write API's functions below
export const insertLoginUser = (LoginUserName: any, LoginPassword: any) => {
    return httpPost(ROUTES.api.userLogin + "?LoginUserName=" + LoginUserName + "&LoginPassword=" + LoginPassword);
};

export const getAllInvoiceDetails = (searchText: any, CountryName: any, CityName: any, managerId: any, InvoiceNumber : any, UserRoleId: any) => {
    return httpGet(ROUTES.api.gridBindInvoiceDetails + "?searchText=" + searchText +
        "&CountryName=" + CountryName +
        "&CityName=" + CityName +
        "&ManagerId=" + managerId +
        "&InvoiceNumber=" + InvoiceNumber +
        "&UserRoleId=" + UserRoleId
    );
};

export const getStateManagerDetails  = (searchText: any, CountryName: any, CityName: any, Status: any, UserRoleId: any) => {
    return httpGet(ROUTES.api.gridBindStateManager + "?searchText=" + searchText +
        "&CountryName=" + CountryName +
        "&CityName=" + CityName +
        "&Status=" + Status +
        "&UserRoleId=" + UserRoleId
    );
};

export const RemoveManager = (formdata:any) => {
    return httpPost(ROUTES.api.deleteManager,formdata);
};

export const getusersByUserTeams = (userId: any) => {
    return httpGet(ROUTES.api.GetUsersByUserTeams + "?UserId=" +  userId);
};

export const getAllStatemanager = (UserId:any,StateManagerId:any) => {
    return httpGet(ROUTES.api.getAllstatemanagername+ "?UserId=" +  UserId+"&StateManagerId="+StateManagerId);
};
export const getAllAnnouncements = (fromDate:any,toDate:any) => {
    return httpGet(ROUTES.api.getAllAnnouncement + "?fromDate=" +  fromDate + "&toDate=" +toDate); 
};
export const getAllsales = (DealerId:any,ManagerId:any,StateManagerId:any,DayRange:any,StartDate:any,EndDate:any,MaterialGroup:any) => {
    return httpGet(ROUTES.api.getsales+"?DealerId="+DealerId+"&ManagerId="+ManagerId+"&StateManagerId="+StateManagerId+"&selectRange="+DayRange+"&fromDate="+StartDate+"&toDate="+EndDate+"&MaterialGroup="+MaterialGroup);
};
// export const getAllsales = (searchText: any, CityName: any, Region: any, Manager: any,InvoiceNo:any) => {
//     return httpGet(ROUTES.api.getsales + "?searchText=" + searchText +
//         // "&CountryName=" + CountryName +
//         "&CityName=" + CityName +
//         "&Region=" + Region +
//         "&Manager=" + Manager+
//         "&InvoiceNo="+InvoiceNo
//     );
// };


// export const getInvoiceGrid = (fromDate:any,toDate:any) => {
//     return httpGet(ROUTES.api.getInvoiceDetail +"?fromDate=" + fromDate + "&toDate=" +toDate);
// }

export const getDealersDetails = () => {
    return httpGet(ROUTES.api.GetDealersDetails + "?UserId=" +  4);
};

export const getAllDealers = (UserId:any,ManagerId:any,StateManagerId:any) => {
    return httpGet(ROUTES.api.getAllDealers + "?UserRoleId=" +  4+"&UserId="+UserId+"&ManagerId="+ManagerId+"&StateManagerId="+StateManagerId);
};

export const insertAssingmanager = (formdata:any) => {
    return httpPost(ROUTES.api.assignManager ,formdata);
};

export const addManager = (formdata:any) => {
    return httpPost(ROUTES.api.createManager ,formdata);
};

export const activeStatusManager = (id:any ,status:any) => {
    return httpPost(ROUTES.api.activeManager + "?id=" + id + "&status=" + status);
};

export const dashboardDetails = (selectRange:any,fromDate:any,toDate:any,DealerId:any,ManagerId:any,StateManagerId:any) => {
    return httpGet(ROUTES.api.getdashboardDetails + "?selectRange=" + selectRange + "&fromDate=" + fromDate + "&toDate=" + toDate+"&DealerId="+DealerId+"&ManagerId="+ManagerId+"&StateManagerId="+StateManagerId);
}
export const getDocumentAccessList = (DealerId:any,ManagerId:any,StateManagerId:any) => {
    return httpGet(ROUTES.api.getAllDocuments+"?DealerId="+DealerId+"&ManagerId="+ManagerId+"&StateManagerId="+StateManagerId)
}
export const addAnnouncement = (formdata:any) => {
    return httpPost(ROUTES.api.addAnnouncement, formdata)
}
export const uploadFile = (formData: any) => {
    return httpPost(ROUTES.api.commonUpload, formData)
}
export const insertDocumentAccess = (formData: any) => {
    return httpPost(ROUTES.api.insertDocumentAccess, formData)
}

export const getDesignationList = () => {
    return httpGet(ROUTES.api.getDesignation);
  };

export const getAllAssignedUsers = (UserRoleId:any ,UserId:any) => {
    return httpGet(ROUTES.api.getAllAssignedUsersById + "?UserRoleId=" + UserRoleId + "&UserId=" + UserId);
};
 export const getScrollAnnouncement = (UserRoleId:any ,UserId:any) => {
    return httpGet(ROUTES.api.getscrollannouncement + "?UserRoleId=" + UserRoleId + "&UserId=" + UserId);
 }


 export const GetAllSalesDetailsByMaterialGroup = () => {
    return httpGet(ROUTES.api.GetAllSalesDetailsByMaterialGroup );
};
