import React, { Suspense, lazy } from 'react';
import { Route, Routes } from 'react-router-dom';
import AxiosProvider from '../providers/AxiosProvider';
import LoaderControl from "../components/helpers/loaderControl";
import { ROUTES } from '../configs/constants';
import AuthLayout from '../components/layouts/authLayout';
import DashboardLayout from '../components/layouts/dashboardLayout';


const Dashboard = lazy(() => import('../pages/dashboard'));
const Login = lazy(() => import('../pages/auth/login'));
const Manager = lazy(() => import('../pages/manager/manager'));
const Dealer = lazy(() => import('../pages/dealer/dealer'));
const Sales = lazy(() => import('../pages/sales/sales'));
const Announcements = lazy(() => import('../pages/announcement/announcement'));
const AccountStatement = lazy(() => import('../pages/accountstatement/account-statement'));
const DocumentAccess = lazy(() => import('../pages/documentaccess/document-access'));
const NotFound = lazy(() => import('../pages/notFound'));
const StateManager = lazy(() => import('../pages/statemanager'));
// const Management = lazy (()=> import('../pages/managment') )

function RouteApp() {
    return (<>
        <AxiosProvider />
        <Routes>
            {/* Login and SingUp, mobile and email verify pages */}
            <Route path={ROUTES.LOGIN} element={<AuthLayout />}>
                <Route path={ROUTES.LOGIN} element={<Suspense fallback={<LoaderControl />}><Login /></Suspense>} />
            </Route>
            <Route path={ROUTES.HOME} element={<DashboardLayout />}>
            <Route path={ROUTES.DASHBOARD.HOME} element={<Suspense fallback={<LoaderControl />}><Dashboard /></Suspense>} />
            <Route path={ROUTES.DASHBOARD.STATE_MANAGER} element={<Suspense fallback={<LoaderControl />}><StateManager /></Suspense>} />
            <Route path={ROUTES.DASHBOARD.MANAGER} element={<Suspense fallback={<LoaderControl />}><Manager /></Suspense>} />
            <Route path={ROUTES.DASHBOARD.DEALER} element={<Suspense fallback={<LoaderControl />}><Dealer /></Suspense>} />
            <Route path={ROUTES.DASHBOARD.SALES} element={<Suspense fallback={<LoaderControl />}><Sales /></Suspense>} />
            <Route path={ROUTES.DASHBOARD.ANNOUNCEMENT} element={<Suspense fallback={<LoaderControl />}><Announcements /></Suspense>} />
            <Route path={ROUTES.DASHBOARD.ACCOUNT_STATEMENT} element={<Suspense fallback={<LoaderControl />}><AccountStatement /></Suspense>} />
            <Route path={ROUTES.DASHBOARD.DOCUMENT_ACCESS} element={<Suspense fallback={<LoaderControl />}><DocumentAccess /></Suspense>} />
            </Route>  
            
            
            <Route path={"*"} element={<Suspense fallback={<LoaderControl />}><NotFound /></Suspense>} />
        </Routes>
    </>)
}

export default RouteApp;
