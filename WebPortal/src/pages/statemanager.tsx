
import { useEffect, useState } from "react";
import DatePickerControl from "../components/helpers/DatePickerControl";
import { FormControl, InputLabel, MenuItem, Select, TextField, Dialog, Paper, FormControlLabel, Checkbox, OutlinedInput, InputAdornment, IconButton, Button, Pagination, PaginationItem, FormHelperText } from "@mui/material";
import SubdirectoryArrowRightIcon from '@mui/icons-material/SubdirectoryArrowRight';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { ArrowDownwardRounded, Search } from "@mui/icons-material";
import '../assets/scss/page.css';
import { CheckBox, MoreVertRounded } from "@mui/icons-material";
import CheckBoxTableIcon from '@mui/icons-material/CheckBox';
import { blue, green, red } from "@mui/material/colors";
import DeleteForeverRoundedIcon from '@mui/icons-material/DeleteForeverRounded';
import { activeStatusManager, addManager, getAllAssignedUsers, getDesignationList, getStateManagerDetails } from "../models/model";
import { RemoveManager } from "../models/model";
import { insertAssingmanager, getusersByUserTeams } from "../models/model";
import CancelIcon from '@mui/icons-material/Cancel';
import EditIcon from '@mui/icons-material/Edit';
import ExportExcel from'../assets/images/Export Excel.png';
import { downloadExcel } from "react-export-table-to-excel";
//import swal from "sweetalert";
import Swal from 'sweetalert2';
import { validateEmail, validateMobile } from "../services/ValidationService";
export default function StateManager() {
    // const [_formData, _setFormData] = useState<any>({
    //     startDate: dayjs(moment().format('YYYY/MM/DD')) || null,
    //     endDate: dayjs(moment().format('YYYY/MM/DD')) || null,
    //     day: '',
    // });

    const [_list, _setList] = useState<any>([]);
    const [searchInput, setSearchInput] = useState("");
    const [searchremove, setSearchremove] = useState("");
    const [assigncity, setAssigncity] = useState("");
    const [dealer, setDealer] = useState('');
    const [_assignList, _setAssignList] = useState<any>([])
    const [_editList, _setEditList] = useState<any>([])
    const [dialog, setDialog] = useState(false)
    const [assign, setAssign] = useState(false)
    const [_popup, _setPopup] = useState<any>(false);
    const [_checkvalue, _setCheckvalue] = useState<any>({});
    const [_formCheckvalue, _setFormCheckvalue] = useState<any>({});
    const [selectedCity, setSelectedCity] = useState('');
    const [selectedManager, setSelectedManager] = useState('');
    const [selectedstatus, setSelectedstatus] = useState('');
    const [selectedregion, setSelectedregion] = useState('');
    const [selectedname, setSelectedname] = useState('');
    const [assingname, setAsigname] = useState('');
    const [isChecked, setisChecked] = useState<string[]>([]);
    const [asignChecked, setAsignChecked] = useState<string[]>([]);
    const [delmsg, setDelmsg] = useState('');
    const [_createManager, _setCreateManager] = useState<any>(false);
    const [_buttonLoading, _setbuttonLoading] = useState(false);
    const [ischecked, setIsChecked] = useState(false);
    const [_managerId, _setManagerId] = useState<any>('');
    const [_userteamslist, _setUserTeamsList] = useState<any>([]);
    const [_titleresponse, _settitleresponse] = useState<any>([]);
    const [_type, _setType] = useState<any>("create");
    const [_userId, _setUserId] = useState("");
    const [_page, _setPage] = useState(1);
    const [_rowsPerPage] = useState(10);
    const [_totalCount, _setTotalCount] = useState(0);


    const [checkedUserIds, setCheckedUserIds] = useState<number[]>([]);
    const [openremoveCheckedUserIds, setOpenremoveCheckedUserIds] = useState<number[]>([]);
    const [assignCheckedUserIds, setAssignCheckedUserIds] = useState<number[]>([]);
    const label = { inputProps: { 'aria-label': 'Checkbox demo' } };
    const [_assignManagerList, _setAssignManagerList] = useState<any>([])


    const activeClick = (e: any) => {
        debugger;
        setIsChecked(e.target.checked);
    };


    const gettitle = () => {

        getDesignationList()
            .then((response) => {
                debugger;
                _settitleresponse(response.data.data);
                console.log(response.data);
            })
            .catch((error) => {
                console.log(error.response);
            });
    };

    const handleChangePage = (event: React.ChangeEvent<unknown>, value: number) => {
        _setPage(value);
    };

    const cancelStateManager = () => {
        _setCreateManager(false)
        _setFormData({
            ..._formData,
            userId: 0,
            employeeCode: "",
            firstName: "",
            mobileNumber: "",
            emailId: "",
            stateName: "",
            city: "",
            designation: "",
            status: "",
        });
        _setType('')
    }

    const activeChecked = (val: any, status: any) => {
        debugger;
        activeStatusManager(val, status).then(response => {
            gridBindDetails();
        })
    }

    const [_formData, _setFormData] = useState<any>({
        userId: 0,
        employeeCode: "",
        firstName: "",
        mobileNumber: "",
        emailId: "",
        stateName: "",
        city: "",
        designation: "",
        status: "",
    });

    const changeFormData = (name: string, value: any) => {
        _setFormData({ ..._formData, [name]: value });
    };

    const validate = {
        employeeCode: { error: false, message: "" },
        firstName: { error: false, message: "" },
        mobileNumber: { error: false, message: "" },
        city: { error: false, message: "" },
        stateName: { error: false, message: "" },
        emailId: { error: false, message: "" },
        designation: { error: false, message: "" }
    };

    const [_managervalidate, _setManagervalidate] = useState(validate);

    const InserManager = () => {
        debugger;
        //  var userid = user.id;
        const _validate: any = Object.assign({}, validate);
        let valid = true;
        if (_formData.employeeCode === "" || _formData.employeeCode === 0) {
            _validate.employeeCode.error = true;
            _validate.employeeCode.message = "Required Field";
            valid = false;
        }
        if (_formData.firstName === "" || _formData.firstName === 0) {
            _validate.firstName.error = true;
            _validate.firstName.message = "Required Field";
            valid = false;
        }
        if (_formData.mobileNumber === "" || _formData.mobileNumber === 0) {
            _validate.mobileNumber.error = true;
            _validate.mobileNumber.message = "Required Field";
            valid = false;
        }
        if (!validateMobile(_formData.mobileNumber)) {
            _validate.mobileNumber.error = true;
            _validate.mobileNumber.message = "Please Enter Valid Mobile Number";
            valid = false;
        }

        if (_formData.city === "" || _formData.city === 0) {
            _validate.city.error = true;
            _validate.city.message = "Required Field";
            valid = false;
        }
        if (_formData.stateName === "" || _formData.stateName === 0) {
            _validate.stateName.error = true;
            _validate.stateName.message = "Required Field";
            valid = false;
        }
        if (_formData.designation === "" || _formData.designation === 0) {
            _validate.designation.error = true;
            _validate.designation.message = "Required Field";
            valid = false;
        }
        // if (_formData.emailId === "" || _formData.emailId === 0) {
        //     _validate.emailId.error = true;
        //     _validate.emailId.message = "Required Field";
        //     valid = false;
        // }

        if (!validateEmail(_formData.emailId)) {
            _validate.emailId.error = true;
            _validate.emailId.message = "Please Enter Valid Email";
            valid = false;
        }

        _setManagervalidate(_validate);
        if (!valid) {
            _setbuttonLoading(false);
            return;
        }
        _setbuttonLoading(true);

        add()

        _setFormData({
            ..._formData,
            userId: 0,
            employeeCode: "",
            firstName: "",
            mobileNumber: "",
            emailId: "",
            stateName: "",
            city: "",
            designation: "",
            status: "",
        });
    };

    const add = () => {
        const data = {
            userId: 0,
            employeeCode: _formData.employeeCode,
            firstName: _formData.firstName,
            mobileNumber: _formData.mobileNumber,
            emailId: _formData.emailId,
            countryName: _formData.stateName,
            city: _formData.city,
            designation: _formData.designation,
            status: _formData.status,
            loginPassword: _formData.employeeCode,
            configUserRoleId: "2",
        };
        console.log(data);
        addManager(data).then((response: any) => {
            debugger;
            if (response.data.status === true) {
                _setCreateManager(false)
                Swal.fire({
                    title: 'Success!',
                    text: 'State Manager Created Successfully',
                    icon: 'success', // Specify the icon type here
                    confirmButtonText: 'OK',
                });
                gridBindDetails();
            } else {
                //  swal("Something went wrong!", { icon: "warning" });
            }
        });
        _setbuttonLoading(false);
    };


    const updateManager = () => {
        debugger;
        //  var userid = user.id;
        const _validate: any = Object.assign({}, validate);
        let valid = true;
        if (_formData.employeeCode === "" || _formData.employeeCode === 0) {
            _validate.employeeCode.error = true;
            _validate.employeeCode.message = "Required Field";
            valid = false;
        }
        if (_formData.firstName === "" || _formData.firstName === 0) {
            _validate.firstName.error = true;
            _validate.firstName.message = "Required Field";
            valid = false;
        }
        if (_formData.mobileNumber === "" || _formData.mobileNumber === 0) {
            _validate.mobileNumber.error = true;
            _validate.mobileNumber.message = "Required Field";
            valid = false;
        }
        if (!validateMobile(_formData.mobileNumber)) {
            _validate.mobileNumber.error = true;
            _validate.mobileNumber.message = "Please Enter Valid Mobile Number";
            valid = false;
        }

        if (_formData.city === "" || _formData.city === 0) {
            _validate.city.error = true;
            _validate.city.message = "Required Field";
            valid = false;
        }
        if (_formData.stateName === "" || _formData.stateName === 0) {
            _validate.stateName.error = true;
            _validate.stateName.message = "Required Field";
            valid = false;
        }
        if (_formData.designation === "" || _formData.designation === 0) {
            _validate.designation.error = true;
            _validate.designation.message = "Required Field";
            valid = false;
        }
        // if (_formData.emailId === "" || _formData.emailId === 0) {
        //     _validate.emailId.error = true;
        //     _validate.emailId.message = "Required Field";
        //     valid = false;
        // }

        if (!validateEmail(_formData.emailId)) {
            _validate.emailId.error = true;
            _validate.emailId.message = "Please Enter Valid Email";
            valid = false;
        }

        _setManagervalidate(_validate);
        if (!valid) {
            _setbuttonLoading(false);
            return;
        }
        _setbuttonLoading(true);

        update()

        _setFormData({
            ..._formData,
            userId: 0,
            employeeCode: "",
            firstName: "",
            mobileNumber: "",
            emailId: "",
            stateName: "",
            city: "",
            designation: "",
            status: "",
        });
    };

    const update = () => {
        const data = {
            id: _userId,
            employeeCode: _formData.employeeCode,
            firstName: _formData.firstName,
            mobileNumber: _formData.mobileNumber,
            emailId: _formData.emailId,
            countryName: _formData.stateName,
            city: _formData.city,
            designation: _formData.designation,
            status: _formData.status,
            loginPassword: _formData.employeeCode,
            configUserRoleId: "2",
        };
        console.log(data);
        addManager(data).then((response: any) => {
            debugger;
            if (response.data.status === true) {
                _setCreateManager(false)
                Swal.fire({
                    title: 'Success!',
                    text: 'State Manager Updated Successfully',
                    icon: 'success', // Specify the icon type here
                    confirmButtonText: 'OK',
                });
                gridBindDetails();
            } else {
                //  swal("Something went wrong!", { icon: "warning" });
            }
        });
        _setbuttonLoading(false);
    };



    const handleCityChange = (event: any) => {
        setSelectedCity(event.target.value); // Update the selectedCity state
    };
    const handleManagerChange = (event: any) => {
        setSelectedManager(event.target.value); // Update the selectedCity state
    };
    const handleStatusChange = (event: any) => {
        setSelectedstatus(event.target.value);
    };
    const handleregionChange = (event: any) => {
        setSelectedregion(event.target.value);
    }
    const handleassignName = (event: any) => {
        setAsigname(event.target.value)
    }
    const handleassingcity = (event: any) => {
        setAssigncity(event.target.value)
    }
    const handlenameChange = (event: any) => {
        setSelectedname(event.target.value);
    }
    const handleActionClick = () => {
        _setPopup(true)
    }

    const openremove = (val: any) => {
        getusersByUserTeams(val)
            .then(res => {
                debugger;
                if (res?.data?.data?.isSuccess === true) {
                    _setUserTeamsList(res?.data?.data?.data);
                    _setManagerId(val)
                    console.log(_userteamslist);
                    // const userIds = res?.data?.data?.data.map((item: any) => item.userId) || [];
                    // setAssignCheckedUserIds(userIds);
                    // setCheckedUserIds(userIds.filter((id: any) => openremoveCheckedUserIds.includes(id)));
                }
                else {
                    _setUserTeamsList([]);
                }
            })
            .catch(err => {

            });
        setDialog(true)
    }
    const openassign = (val: any) => {
        setAssign(true)
        assignDetails(val)
       // assignManager(val)
    }

    const assignManager = (val:any) => {
        debugger;
        getAllAssignedUsers(3,val)
        .then(response => {
            debugger;
          if(response?.data?.data?.isSuccess === true){

            console.log("success")
          }
        })
        .catch((err) => console.log(err));
    }

    const createManager = () => {
        _setCreateManager(true)
    }
    const handleCheckbox = (e: any) => {
        const { value, checked } = e.target;
        console.log("data", value);
        debugger;
        if (checked) {
            setisChecked([...isChecked, value]);
        }
        else {
            setisChecked(isChecked.filter((e) => e !== value));
        }
    }


    const handleCheckbox2 = (e: any, index: any) => {
        const { value, checked } = e.target;
        console.log("data", value);
        debugger;
        const tempData: any = _assignList;
        tempData[index].asignChecked = checked;
        _setAssignList(tempData);
        //_assignList[index]?.asignChecked=value;
        if (checked) {
            setAsignChecked([...asignChecked, value]);

        }
        else {
            setAsignChecked(asignChecked.filter((e) => e !== value));
        }
    }
    const alldelete = () => {
        debugger
        const data = {
            managerId: _managerId,
            assighId: isChecked.map(value => ({ id: value }))
        };
        RemoveManager(data).then(response => {
            debugger;
            if (response.data.data.isSuccess === true) {
                Swal.fire({
                    title: 'Success!',
                    text: ' State Manager Removed Successfully',
                    icon: 'success', 
                    confirmButtonText: 'OK',
                });
                console.log("success");
            }
            else {
                console.log("bad request");
            }
        })
        setDialog(false);

    };

    const Insertassign = () => {
        debugger;
        const data = {
            managerId: _managerId,
            assighId: asignChecked.map(value => ({ id: value }))
        };
        insertAssingmanager(data).then(response => {
            debugger;
            if (response.data.status === true) {
                // Swal.fire('Manager Assigned Successfully');
                Swal.fire({
                    title: 'Success!',
                    text: 'State Manager Assigned Successfully',
                    icon: 'success', 
                    confirmButtonText: 'OK',
                });
                console.log("success");
            }
            else {
                console.log("bad request");

            }
        })
        setAssign(false)
    }

    const gridBindDetails = () => {
        getStateManagerDetails('', '', '', '', 2)
            .then(response => {
                debugger;
                _setList(response?.data?.data?.data);
                _setTotalCount([...response?.data?.data?.data]?.length);
                console.log("list");
                console.log(_list);
            })
            
            .catch((err) => console.log(err));
           
    }

    const assignDetails = (val: any) => {
        debugger;
    //    getStateManagerDetails('', '', '', '', 3)
    getAllAssignedUsers(3,val)
            .then(response => {
                debugger;
                var tempData = response?.data?.data?.data;
                var result = [];
                getusersByUserTeams(val)
                    .then(res1 => {
                        debugger;
                        if (res1?.data?.data?.isSuccess === true) {
                            const userIds: any = res1?.data?.data?.data.map((item: any) => item.id) || [];
                            var ManagersSelList = tempData?.length > 0 ? [
                                ...tempData?.map((pItem: any) => {
                                    if (userIds?.includes(pItem?.id)) {
                                        return {
                                            mobileNumber: pItem?.mobileNumber,
                                            status: pItem?.status,
                                            stateName: pItem?.stateName,
                                            countryName: pItem?.countryName,
                                            createDate: pItem?.createDate,
                                            updateDate: pItem?.updateDate,
                                            address: pItem?.address,
                                            city: pItem?.city,
                                            countryCode: pItem?.countryCode,
                                            loginUserName: pItem?.loginUserName,
                                            emailId: pItem?.emailId,
                                            firstName: pItem?.firstName,
                                            lastName: pItem?.lastName,
                                            postalCode: pItem?.postalCode,
                                            stateCode: pItem?.stateCode,
                                            id: pItem?.id,
                                            employeeCode: pItem?.employeeCode,
                                            userRole: pItem?.userRole,
                                            designation: pItem?.designation,
                                            asignChecked: true
                                        }
                                    }
                                    else {
                                        return {
                                            mobileNumber: pItem?.mobileNumber,
                                            status: pItem?.status,
                                            stateName: pItem?.stateName,
                                            countryName: pItem?.countryName,
                                            createDate: pItem?.createDate,
                                            updateDate: pItem?.updateDate,
                                            address: pItem?.address,
                                            city: pItem?.city,
                                            countryCode: pItem?.countryCode,
                                            loginUserName: pItem?.loginUserName,
                                            emailId: pItem?.emailId,
                                            firstName: pItem?.firstName,
                                            lastName: pItem?.lastName,
                                            postalCode: pItem?.postalCode,
                                            stateCode: pItem?.stateCode,
                                            id: pItem?.id,
                                            employeeCode: pItem?.employeeCode,
                                            userRole: pItem?.userRole,
                                            designation: pItem?.designation,
                                            asignChecked: false
                                        }
                                    }

                                })
                            ] : []
                            console.log(ManagersSelList)
                            _setAssignList(ManagersSelList);
                        }
                        else {
                            //_setUserTeamsList([]);
                            _setAssignList(tempData);
                        }
                    })
                    .catch(err => {
                        _setAssignList(tempData);
                    });


                _setManagerId(val)
                // console.log("assignList")
                // console.log(_assignList);
            })
            .catch((err) => console.log(err));
        getusersByUserTeams(val)
            .then(res => {
                debugger;
                if (res?.data?.data?.isSuccess === true) {
                    const userIds = res?.data?.data?.data.map((item: any) => item.id) || [];
                    setAssignCheckedUserIds(userIds);
                    setCheckedUserIds(userIds.filter((id: any) => openremoveCheckedUserIds.includes(id)));
                }
                else {
                    _setUserTeamsList([]);
                }
            })
            .catch(err => {
            });
    }


    const searchDetails = (input: any) => {
        debugger;
        getStateManagerDetails(input, '', '', '', 3)
            .then(response => {
                debugger;
                //_setAssignList(response?.data?.data);
                console.log(_assignList);
            })
            .catch((err) => console.log(err));
    }

    const actionClick = (type: any, item?: any) => {
        _setCreateManager(true)
        _setType(type);
        _setUserId(item?.id)
        _setFormData({
            id: item.id,
            employeeCode: item.employeeCode,
            firstName: item.firstName,
            mobileNumber: item.mobileNumber,
            emailId: item.emailId,
            stateName: item.countryName,
            city: item.city,
            designation: item.designation,
            status: item.status,
        });
    }

    useEffect(() => {
        gridBindDetails();
        gettitle();
    }, []);

    const closeAssignManagerPopup = () => {
        setAssign(false);
        _setAssignList([]);
    }

    const exportEXCEL = () => {
        debugger;
               const header = ["Employee Id", "Name", "Region", "City","Designation","PhoneNo","Email"];
            
               let body: any = _list.map((e: any) => { 
                const fullName = e?.firstName + ' ' + e?.lastName;
                return [e?.employeeCode, fullName, e?.countryName, e?.city,e?.designation,e?.mobileNumber,e?.emailId] })
            
               // var curDate = moment()?.format("YYYY-MM-DD")
            
               var filename = 'SatateManagers'
            
               downloadExcel({
            
                 fileName: filename,
            
                 sheet: "StateManagerList",
            
                 tablePayload: {
            
                   header,
            
                   body: body,
            
                 },
            
               });
            
             }
             useEffect(()=>{
                debugger;
            //     const useritem = localStorage.getItem('_token');
            // const tokenData = useritem ? JSON.parse(JSON.parse(useritem)) : null;
            //  const userId = tokenData?.token?.data?.userid || null;

// console.log(userId);
             },[])
             
             const storedTokenString = localStorage.getItem('_token');
const storedToken = storedTokenString ? JSON.parse(storedTokenString) : null;
const userId = storedToken?.token?.data?.userid || null;
const userRoleid=storedToken?.token?.data?.userRoleId || null;
console.log(userId);

            
    const sortedData = _list.slice().sort((a:any, b:any) => b.id - a.id);
    return (<>
        {_createManager === false && (
            <div>
                <div className="my-3 mt-4">
                    <div className="col-md-12 mx-auto mt-4">
                        <div className="row align-items-center">
                            <div className="col-md-1"></div>
                            <div className="col-md-11"><div className="row">
                                <div className="mt-2 col-md-3">
                                    {/* <TextField
                            className="bg-white"
                            id="outlined-basic"
                            fullWidth
                            size="small"
                            label="Search"
                            variant="outlined"
                            onChange={(e) => {
                                setSearchInput(e.target.value);
                            }}
                        /> */}
                                    <FormControl className="bg-light" size="small" fullWidth variant="outlined" >
                                        <InputLabel htmlFor="outlined-adornment-password">Search</InputLabel>
                                        <OutlinedInput className="bg-white"
                                            label="Search"
                                            onChange={(e) => setSearchInput(e.target.value)}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton aria-label="toggle password visibility" edge="end" type="submit">
                                                        <Search />
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by city</InputLabel>
                                        <Select label="Select Range" value={selectedCity}
                                            onChange={handleCityChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={item.city}>{item.city}</MenuItem>))}
                                            {/* {_list && _list.length > 0 && _list
                                                .filter((item: any) => item?.city !== null)
                                                .map((item: any, index: any) => (
                                                    item.city ? (
                                                        <MenuItem value={item.cty}>
                                                            {item.city}
                                                        </MenuItem>
                                                    ) : null
                                                ))} */}
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by Manager</InputLabel>
                                        <Select label="Select Range" value={selectedManager} onChange={handleManagerChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={`${item.firstName} ${item.lastName}`}>{item.firstName} {item.lastName}</MenuItem>))}
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                            {/* <MenuItem value={'15days'}>Last 15 days</MenuItem>
                                            <MenuItem value={'30days'}>Last 30 days</MenuItem>
                                            <MenuItem value={'3months'}>Last 3 months</MenuItem> */}
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl size="small" fullWidth className="bg-white">
                                        <InputLabel>Filter by Status</InputLabel>
                                        <Select label="Select Range" value={selectedstatus} onChange={handleStatusChange} >
                                            <MenuItem value={'Active'} >Active</MenuItem>
                                            <MenuItem value={'InActive'} >InActive</MenuItem>
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div><div className="mt-2 col-md-1 p-0">
                                <Button type="button" style={{ background:"linear-gradient(to bottom, #9FD8FD 50%,  #8CCEFF 50%)",border:"2px solid  #0098E1" }} className="btn text-dark text-capitalize"
                                    onClick={createManager}>
                                    + create
                                </Button>
                            </div>
                            <div className="mt-2 col-md-1">
                            <img className="p-2" style={{ height: '55px' }} src={ExportExcel}
                             onClick={exportEXCEL} 
                             />
                            </div>
                            </div>
                            </div>
                            
                            <div className="mt-2 col-md-1">
                            </div>
                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-md-1">
                        </div>
                        <div className="col-md-10">
                            <div className="tablebx" style={{ overflowX: "auto" }}>

                                <Paper sx={{ width: '100%', mb: 1 }} elevation={0}>
                                    <TableContainer className="rounded">
                                        <Table className="border " size="small">
                                            <TableHead className=" py-4" style={{ borderBottom: '0.1rem  solid gray', height: "4rem",backgroundColor:" #0098E1" }}>
                                                <TableRow>
                                                    <TableCell className=" fs16 py-2 fw-bold text-white border-3 border-white " align={"center"}>S.No.</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Employee Id</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Name</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>City</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>State</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Designation</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Mobile</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Email</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Action</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                             {/* {userRoleid===2 && (<>
                                                {sortedData?.length > 0 ? sortedData
                                                    .filter((content: any) => {
                                                        const lowerSearchInput = searchInput.toLowerCase();
                                                        const lowerSelectedCity = selectedCity.toLowerCase();
                                                        const lowerSelectedManager = selectedManager.toLowerCase();
                                                        const lowerSelectedstatus = selectedstatus.toLowerCase();

                                                        return (
                                                            
                                                            userId && // Check if userId exists
                                                            content.id === userId &&
                                                            (lowerSearchInput === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchInput))) &&
                                                            (lowerSelectedCity === '' || content.city?.toLowerCase() === lowerSelectedCity || lowerSelectedCity === 'selectall') &&
                                                            (lowerSelectedstatus === '' || content.status?.toLowerCase() === lowerSelectedstatus || lowerSelectedstatus === 'selectall') &&
                                                            (lowerSelectedManager === '' || (content.firstName + ' ' + content.lastName).toLowerCase() === lowerSelectedManager || lowerSelectedManager === 'selectall')
                                                        );
                                                    })?.slice(
                                                        (_page - 1) * _rowsPerPage,
                                                        (_page - 1) * _rowsPerPage + _rowsPerPage,
                                                    )
                                                    ?.map((item: any, index: number) => (
                                                        <TableRow key={index} className={index % 2 === 0 ? 'white-row' : 'grey-row'}>
                                                            <TableCell align={"center"}>{(index + 1) + ((_page - 1) * 10)}</TableCell>
                                                            <TableCell align="center">{item.employeeCode}</TableCell>
                                                            <TableCell align="center">{item.firstName} {item.lastName}</TableCell>
                                                            <TableCell align="center">{item.city}</TableCell>
                                                            <TableCell align="center">{item.countryName}</TableCell>
                                                            <TableCell align="center">{item.designation}</TableCell>
                                                            <TableCell align="center">{item.mobileNumber}</TableCell>
                                                            <TableCell align="center">{item.emailId}</TableCell>
                                                            <TableCell>
                                                                <div className="p-1" role="button" data-bs-toggle="dropdown">
                                                                    <MoreVertRounded className="text-muted" />
                                                                </div>
                                                                <div className="dropdown-menu dropdown-menu-bottom">
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => actionClick("update", item)}>
                                                                        <span className="mt-1" >Edit</span>
                                                                        <span className="mx-2">
                                                                            <EditIcon style={{ color: "black" }} />
                                                                        </span>
                                                                    </div>
                                                                    <hr className="" />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => openassign(item.id)}>
                                                                        <span>Assign</span>
                                                                        <span> <SubdirectoryArrowRightIcon sx={{ color: green[800] }}></SubdirectoryArrowRightIcon></span>
                                                                    </div>
                                                                    <hr className="" />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button">
                                                                        <span> Active</span>
                                                                        <span>
                                                                            <Checkbox
                                                                                className="p-0"
                                                                                checked={item?.status === "Active"}
                                                                                onChange={(event) => {
                                                                                    activeChecked(item?.id, event.target.checked ? "Active" : "InActive");
                                                                                }}
                                                                            />
                                                                        </span>
                                                                    </div>
                                                                    <hr className="" />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => openremove(item?.id)}>
                                                                        <span>Remove</span>
                                                                        <span> <DeleteForeverRoundedIcon sx={{ color: red[800] }}></DeleteForeverRoundedIcon> </span>
                                                                    </div>
                                                                </div>
                                                            </TableCell>
                                                        </TableRow>
                                                    ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">No Data Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                                   </> )} */}

                                                
                                                   
                                                    {sortedData?.length > 0 ? sortedData
                                                    .filter((content: any) => {
                                                        const lowerSearchInput = searchInput.toLowerCase();
                                                        const lowerSelectedCity = selectedCity.toLowerCase();
                                                        const lowerSelectedManager = selectedManager.toLowerCase();
                                                        const lowerSelectedstatus = selectedstatus.toLowerCase();

                                                        return (
                                                            
                                                            // userId && // Check if userId exists
                                                            // content.id === userId &&
                                                            (lowerSearchInput === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchInput))) &&
                                                            (lowerSelectedCity === '' || content.city?.toLowerCase() === lowerSelectedCity || lowerSelectedCity === 'selectall') &&
                                                            (lowerSelectedstatus === '' || content.status?.toLowerCase() === lowerSelectedstatus || lowerSelectedstatus === 'selectall') &&
                                                            (lowerSelectedManager === '' || (content.firstName + ' ' + content.lastName).toLowerCase() === lowerSelectedManager || lowerSelectedManager === 'selectall')
                                                        );
                                                    })?.slice(
                                                        (_page - 1) * _rowsPerPage,
                                                        (_page - 1) * _rowsPerPage + _rowsPerPage,
                                                    )
                                                    ?.map((item: any, index: number) => (
                                                        <TableRow key={index} className={index % 2 === 0 ? 'grey-row' : 'white-row'}>
                                                            <TableCell align={"center"}className="fs16 border-3 border-white">{(index + 1) + ((_page - 1) * 10)}</TableCell>
                                                            <TableCell align="center"className="fs16 border-3 border-white">{item.employeeCode}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.firstName} {item.lastName}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.city}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.stateName}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.designation}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.mobileNumber}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.emailId}</TableCell>
                                                            <TableCell className="fs16 border-3 border-white">
                                                                <div className="p-1" role="button" data-bs-toggle="dropdown">
                                                                    <MoreVertRounded className="text-muted" />
                                                                </div>
                                                                <div className="dropdown-menu dropdown-menu-bottom">
                                                                    {/* <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button">Edit &nbsp;&nbsp;<EditOutlinedIcon style={{ fontSize: "medium" }}></EditOutlinedIcon><hr /></div> */}
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => actionClick("update", item)}>
                                                                        <span className="mt-1" >Edit</span>
                                                                        <span className="mx-2">
                                                                            <EditIcon style={{ color: "black" }} />
                                                                        </span>
                                                                    </div>
                                                                    <hr className="" />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => openassign(item.id)}>
                                                                        <span>Assign</span>
                                                                        <span> <SubdirectoryArrowRightIcon sx={{ color: green[800] }}></SubdirectoryArrowRightIcon></span>
                                                                    </div>
                                                                    <hr className="" />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button">
                                                                        <span> Active</span>
                                                                        <span>
                                                                            <Checkbox
                                                                                className="p-0"
                                                                                checked={item?.status === "Active"}
                                                                                onChange={(event) => {
                                                                                    activeChecked(item?.id, event.target.checked ? "Active" : "InActive");
                                                                                }}
                                                                            />
                                                                        </span>
                                                                    </div>
                                                                    <hr className="" />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => openremove(item?.id)}>
                                                                        <span>Remove</span>
                                                                        <span> <DeleteForeverRoundedIcon sx={{ color: red[800] }}></DeleteForeverRoundedIcon> </span>
                                                                    </div>
                                                                </div>
                                                            </TableCell>
                                                        </TableRow>
                                                    ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">No Data Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                                   
                                                   
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    <div className="d-flex justify-content-end align-items-center mt-3 pb-4">
                                        <Pagination count={Math.ceil(_totalCount / _rowsPerPage)} page={_page} onChange={handleChangePage}
                                            variant="outlined" size={"small"} color={"primary"} shape="rounded"
                                            renderItem={(item) => (
                                                <PaginationItem sx={{ mx: '4px' }}
                                                    {...item}
                                                />
                                            )} />
                                    </div>
                                </Paper>

                            </div>
                        </div>
                        <div className="col-md-1">
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-md-6"></div>
                        <div className="col-md-6"></div>
                    </div>

                    {/* Remove Manager */}
                    <Dialog open={dialog} onClose={() => setDialog(false)} >
                        <div className='p-2' style={{ width: "36rem" }}>
                            <div className="container full-condent">
                                <div className="row py-3">
                                    <div className=" col mt-2 fw-bold">Remove Manager</div>
                                    <div className=" col mt-2 text-end" onClick={() => setDialog(false)}><CancelIcon /></div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <TextField id="outlined-basic" fullWidth label="Search Manager" size="small" variant="outlined"
                                            onChange={(e) => {
                                                setSearchremove(e.target.value);
                                            }}
                                        />
                                    </div>
                                    <div className="col-md-4">
                                        <FormControl fullWidth className="bg-white" size="small">
                                            <InputLabel>Filter by Region</InputLabel>
                                            <Select label="Select Range" value={selectedregion} onChange={handleregionChange}>
                                                {_userteamslist.map((item: any) => (
                                                    <MenuItem value={item.countryName}>{item.countryName}</MenuItem>

                                                ))}
                                                <MenuItem value={'selectall'}>All</MenuItem>
                                                {/* <MenuItem value={'15days'}>Last 15 days</MenuItem>
                                                <MenuItem value={'30days'}>Last 30 days</MenuItem>
                                                <MenuItem value={'3months'}>Last 3 months</MenuItem> */}
                                            </Select>
                                        </FormControl>
                                    </div>
                                    <div className="col-md-4">
                                        <FormControl fullWidth className="bg-white" size="small">
                                            <InputLabel>Filter by Manager</InputLabel>
                                            <Select label="Select Range" value={selectedname} onChange={handlenameChange}>
                                                {_userteamslist.map((item: any) => (
                                                    <MenuItem value={`${item.firstName} ${item.lastName}`}>{item.firstName}{item.lastName}</MenuItem>
                                                ))}<MenuItem value={'selectall'}>All</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </div> <br />

                                <Paper sx={{ width: '100%', mb: 1 }} elevation={0}>
                                    <TableContainer>
                                        <Table className="border">
                                            <TableHead className="" style={{ borderBottom: '0.1rem  solid gray' ,backgroundColor:" #0098E1"  }}>
                                                <TableRow>
                                                    <TableCell className=" py-2 fw-bold text-white border-3 border-white" align={"center"}><input type="checkbox" style={{height:"18px",width:"18px"}}/></TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Manager Name</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Region</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>City</TableCell>
                                                </TableRow>
                                            </TableHead>

                                            <TableBody>
                                                {_userteamslist?.length > 0 ? _userteamslist
                                                    .filter((content: any) => {
                                                        const lowerSearchremove = searchremove.toLowerCase();
                                                        const lowerSelectedregion = selectedregion.toLowerCase();
                                                        const lowerSelectedname = selectedname.toLowerCase();

                                                        return (

                                                            (lowerSearchremove === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchremove))) &&
                                                            (lowerSelectedregion === '' || content.countryName?.toLowerCase() === lowerSelectedregion || lowerSelectedregion === 'selectall') &&
                                                            (lowerSelectedname === '' || (content.firstName + ' ' + content.lastName).toLowerCase() === lowerSelectedname || lowerSelectedname === 'selectall')
                                                        );
                                                    })
                                                    ?.map((item: any, index: number) => (

                                                        <TableRow key={item.id}  className={index % 2 === 0 ? 'grey-row' : 'white-row'}>
                                                            <TableCell align="center" className="p-0 border-3 border-white">
                                                                <Checkbox
                                                                    value={item.id} checked={item.isChecked} onChange={(e) => handleCheckbox(e)} />
                                                            </TableCell>
                                                            <TableCell align="center" className="fs16 p-0 border-3 border-white">{item.firstName} {item.lastName}</TableCell>
                                                            <TableCell align="center" className="fs16 p-0 border-3 border-white">{item.countryName}</TableCell>
                                                            <TableCell align="center" className="fs16 p-0 border-3 border-white">{item.city}</TableCell>
                                                        </TableRow>
                                                    ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">No Data Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    <div className=" text-end mt-2">
                                        <button type="button" className="btn btn-danger text-end text-white" onClick={alldelete}>Remove</button>
                                    </div>
                                </Paper>
                            </div>
                        </div>
                    </Dialog>

                    {/* Assign Manager */}
                    <Dialog open={assign} onClose={closeAssignManagerPopup} >

                        <div className='p-2' style={{ width: "36rem" }}>
                            <div className="container full-condent">
                                <div className="row py-3">
                                    <div className=" col mt-2 fw-bold">Assign Manager</div>
                                    <div className=" col mt-2 text-end" onClick={closeAssignManagerPopup}><CancelIcon /></div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <TextField id="outlined-basic" size="small"
                                            onChange={(e) => {
                                                setDealer(e.target.value);
                                            }}
                                            fullWidth label="Search Manager" variant="outlined" />
                                    </div>
                                    <div className="col-md-4">
                                        <FormControl fullWidth className="bg-white" size="small">
                                            <InputLabel>Filter by Manager</InputLabel>
                                            <Select label="Select Range" value={assingname} onChange={handleassignName}>
                                                {_assignList.map((item: any) => (
                                                    <MenuItem value={item.firstName}>{item.firstName}</MenuItem>
                                                ))}<MenuItem value={'selectall'}>All</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                    <div className="col-md-4">
                                        <FormControl fullWidth className="bg-white" size="small">
                                            <InputLabel>Filter by City</InputLabel>
                                            <Select label="Select Range" value={assigncity} onChange={handleassingcity}>
                                                {_assignList.map((item: any) => (<MenuItem value={item.city}>{item.city}</MenuItem>
                                                ))}<MenuItem value={'selectall'}>All</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </div><br />
                                <Paper sx={{ width: '100%', mb: 1 }} elevation={0}>
                                    <TableContainer>
                                        <Table className="border ">
                                            <TableHead className="" style={{ borderBottom: '0.1rem  solid gray',backgroundColor:" #00aae7 " }}>
                                                <TableRow className="">
                                                    <TableCell className="py-2 fw-bold text-white border-3 border-white " align={"center"}><input type="checkbox" style={{height:"18px",width:"18px"}}/></TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Manager Name</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Region</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>City</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody className="mt-2">
                                                {_assignList?.length > 0 ? _assignList
                                                    .filter((content: any) => {
                                                        const lowerSearchassigncity = assigncity.toLowerCase();
                                                        const lowersearchassignname = assingname.toLowerCase();
                                                        const lowersearchdealer = dealer.toLowerCase();

                                                        return (

                                                            (lowersearchdealer === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowersearchdealer))) &&
                                                            (lowersearchassignname === '' || content.firstName?.toLowerCase() === lowersearchassignname || lowersearchassignname === 'selectall') &&
                                                            (lowerSearchassigncity === '' || content.city.toLowerCase() === lowerSearchassigncity || lowerSearchassigncity === 'selectall')
                                                        );
                                                    })
                                                    ?.map((item: any, index: number) => (

                                                        <TableRow key={item.id} className={index % 2 === 0 ? 'grey-row' : 'white-row'}>
                                                            <TableCell align="center" className="p-0 border-3 border-white">
                                                                <Checkbox
                                                                    value={item.id}
                                                                    checked={item.asignChecked}
                                                                    //  checked={item.asignChecked || checkedUserIds.includes(item.userId)}
                                                                    onChange={(e) => handleCheckbox2(e, index)} />
                                                            </TableCell>
                                                            <TableCell align="center" className="fs16 p-0 border-3 border-white">{item.firstName} {item.lastName}</TableCell>
                                                            <TableCell align="center" className="fs16 p-0 border-3 border-white">{item.countryName}</TableCell>
                                                            <TableCell align="center" className="fs16 p-0 border-3 border-white">{item.city}</TableCell>
                                                        </TableRow>
                                                    ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">No Data Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </Paper>
                            </div>

                            <div className="text-end mt-2">
                                <button type="button" className="btn btn-success text-end assignButton" onClick={Insertassign}>Assign</button>
                            </div>
                        </div>
                    </Dialog>

                </div>

            </div>)
        }

        {_createManager === true && (
            <div>
                <div className="row mt-4">
                    <div className="col-md-1"></div>
                    <div className=" col-md-3 fw-bold">Create State Manager</div>
                </div>

                <div className="row mt-4">
                    <div className="col-md-1"></div>
                    <div className="col-md-2">
                        <div className="fs16">Employee ID</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.employeeCode}
                            onChange={(event) => {
                                changeFormData("employeeCode", event.target.value);
                            }}
                            error={_managervalidate?.employeeCode?.error}
                            helperText={_managervalidate?.employeeCode?.message}
                        />
                    </div>
                    <div className="col-md-2">
                        <div className="fs16">Name</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.firstName}
                            onChange={(event) => {
                                changeFormData("firstName", event.target.value);
                            }}
                            error={_managervalidate?.firstName?.error}
                            helperText={_managervalidate?.firstName?.message}
                        />
                    </div>
                    <div className="col-md-2">
                        <div className="fs16">Region</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.stateName}
                            onChange={(event) => {
                                changeFormData("stateName", event.target.value);
                            }}
                            error={_managervalidate?.stateName?.error}
                            helperText={_managervalidate?.stateName?.message}
                        />
                    </div>
                    <div className="col-md-2">
                        <div className="fs16">City</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.city}
                            onChange={(event) => {
                                changeFormData("city", event.target.value);
                            }}
                            error={_managervalidate?.city?.error}
                            helperText={_managervalidate?.city?.message}
                        />
                    </div>
                </div>

                <div className="row mt-4">
                    <div className="col-md-1"></div>
                    <div className="col-md-2">
                        <div className="fs16">Designation</div>
                        {/* <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.designation}
                            onChange={(event) => {
                                changeFormData("designation", event.target.value);
                            }}
                            error={_managervalidate?.designation?.error}
                            helperText={_managervalidate?.designation?.message}
                        /> */}
                        <FormControl
                            className='mt-2 w-100'
                            error={_managervalidate?.designation?.error}>
                            <Select
                                size='small'
                                value={_formData.designation}
                                onChange={(event) => changeFormData("designation", event.target.value)}>
                                {_titleresponse?.map((item: any, index: any) => (
                                    <MenuItem value={item.designation}>{item.designation}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                        <FormHelperText>{_managervalidate?.designation?.message}</FormHelperText>
                    </div>
                    <div className="col-md-2">
                        <div className="fs16"> Phone Number</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.mobileNumber}
                            onChange={(event: any) => {
                                !isNaN(event.target.value)
                                    ? changeFormData("mobileNumber", event.target.value)
                                    : changeFormData("mobileNumber", "")
                            }}
                            error={_managervalidate?.mobileNumber?.error}
                            helperText={_managervalidate?.mobileNumber?.message}

                        />
                    </div>
                    <div className="col-md-2">
                        <div className="fs16">Email</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.emailId}
                            onChange={(event) => {
                                changeFormData("emailId", event.target.value);
                            }}
                            error={_managervalidate?.emailId?.error}
                            helperText={_managervalidate?.emailId?.message}
                        />
                    </div>

                    <div className="col-md-2"> </div>

                </div>
                <div className="row mt-4 px-5">
                    <div className="col-md-1"></div>
                    <div className="col-md-8 d-flex justify-content-end">
                        <div className="row d-flex">
                            <div className="col-md-5 text-end my-auto">
                                <span>
                                    {/* <Checkbox
                                 checked={_formData.status} 
                                 onChange={(event) => {
                                    changeFormData("status", event.target.value);
                                  }}
                                /> */}
                                    <Checkbox
                                        checked={_formData.status === "Active"}
                                        onChange={(event) => {
                                            changeFormData("status", event.target.checked ? "Active" : "InActive");
                                        }}
                                    />
                                </span>
                                <span className="mx-1">Active</span></div>
                            <div className="col-md-4">
                                <Button className="border rounded text-muted" onClick={cancelStateManager}>Cancel</Button>
                            </div>
                            <div className="col-md-3">
                                <Button className="border-2  rounded text-dark px-2" style={{  background:"linear-gradient(to bottom, #9FD8FD 50%,  #8CCEFF 50%)",border:"2px solid  #0098E1" , width: "5rem" }}
                                    onClick={_type === "create" ? InserManager : updateManager}
                                //   onClick={InserManager}
                                >
                                    {_type === "create" ? "Submit" : "Update"}
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-1"></div>
                </div>
            </div>)

        }
    </>);
}


