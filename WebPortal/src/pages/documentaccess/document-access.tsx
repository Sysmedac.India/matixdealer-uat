import { useEffect, useState } from "react";
import { Box, Button, FormControl, FormHelperText, InputLabel, MenuItem, Paper, Select, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TextField, FormControlLabel, Checkbox, OutlinedInput, InputAdornment, IconButton, Pagination, PaginationItem } from "@mui/material";
import { MoreVertRounded, DriveFileRenameOutlineOutlined, RemoveCircleOutlineRounded, Cancel, AddCircle, CheckBox, CheckBoxOutlineBlank } from "@mui/icons-material";
import { SkeletonProviderTables } from "../../providers/SkeletonProvider";
import { getDealersDetails, getAllDealers, getDocumentAccessList, insertDocumentAccess, uploadFile } from "../../models/model";
import DragDropUpload from "../../components/helpers/DragDropUpload";
import { ArrowDownwardRounded, Search } from "@mui/icons-material";
import ExportExcel from '../../assets/images/Export Excel.png';
import { downloadExcel } from "react-export-table-to-excel";
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import Swal from 'sweetalert2';
import EditIcon from '@mui/icons-material/Edit';
import { useStateValue } from '../../providers/StateProvider';

export default function DocumentAccess() {
    const [selectedDealer, setSelectedDealer] = useState('');
    const [searchInput, setSearchInput] = useState("");
    const [_tableList, _setTableList] = useState<any>([]);
    const [_dealerList, _setDealerList] = useState<any>([]);
    const [_documentList, _setDocumentList] = useState<any>([{ id: 0, documentName: "", documentUrl: "" }]);
    const [_loading, _setLoading] = useState(false);
    const [_page, _setPage] = useState(1);
    const [_rowsPerPage] = useState(10);
    const [newLabelVisible, setNewLabelVisible] = useState(true);
    const [hiddenRows, setHiddenRows] = useState<{ [key: number]: boolean }>({});
    const [_totalCount, _setTotalCount] = useState(0);
    const [_docUrl,_setDocUrl] = useState<any>("");
    const [_formData, _setFormData] = useState<any>({
        dealerId: '',
        dealer: '',
        city: '',
        region: '',
        search: '',
    });
    const [{ user }, dispatch]: any = useStateValue();
    const handleRowClick = (rowKey: number) => {

        setHiddenRows((prevHiddenRows) => ({
            ...prevHiddenRows,
            [rowKey]: true,
        }));
        localStorage.setItem('hiddenRows', JSON.stringify({ ...hiddenRows, [rowKey]: true }));
    };
    useEffect(() => {
        const savedHiddenRows = localStorage.getItem('hiddenRows');
        if (savedHiddenRows) {
            setHiddenRows(JSON.parse(savedHiddenRows));
        }
    }, []);
    const changeFormData = (key: string, value: any) => {
        debugger;
        _setFormData({ ..._formData, [key]: value });
        // if (key === "dealerId" && value === "all") {
        //      const allFirstNames = _dealerList.map((item: any) => item.firstName);
        //     _setFormData({ ..._formData, dealerId: allFirstNames });
        // } else {

        //     _setFormData({ ..._formData, [key]: value });
        // }
    }
    const handleDealerchange = (event: any) => {
        setSelectedDealer(event.target.value); // Update the selectedCity state
    };

    const handleChangePage = (event: React.ChangeEvent<unknown>, value: number) => {
        _setPage(value);
    };
    const validate = {
        dealerId: { error: false, message: '' },
        documentName: { error: false, message: '' },
        documentUrl: { error: false, message: '' },
    }
    const [_validate, _setValidate] = useState<any>(validate);

    const handleDocumentChange = (index: number, key: string, value: any) => {
        let _tempArr = [..._documentList];
        _tempArr[index][key] = value;
        _setDocumentList([..._tempArr]);
    }

    const deleteDocList = (index: number) => {
        let _tempArr = [..._documentList];
        _tempArr?.splice(index, 1);
        _setDocumentList([..._tempArr]);
    }
    const addMoreDoc = () => {
        _setDocumentList([..._documentList, { id: 0, documentName: "", documentUrl: "" }]);
    }

    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    // const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    //     setPage(newPage);
    // };
    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };


    //downloadDocument(url)

    const downloadDocument = (url:any) => {
        window.open(url)
     }


    const insertItem = () => {
        debugger;
        let valid = false;
        let validation = Object.assign({}, validate);
        if (_formData?.dealerId < 0) {
            validation.dealerId.error = true;
            validation.dealerId.message = 'Required field';
            valid = true;
        }
        if (valid) {
            _setValidate(validation);
            return;
        }

        const body = {
            userId: _formData?.dealerId,
            documentDetails: _documentList,
        }
        _setLoading(true);
        insertDocumentAccess(body)

            .then((resp) => {
                debugger;
                console.log(resp?.data)
                if (resp?.data?.status === true) {
                    debugger;
                    Swal.fire({
                        title: 'Success!',
                        text: ' Document Uploaded Successfully',
                        icon: 'success',
                        confirmButtonText: 'OK',
                    });
                    getDocumentList()

                    _setDocumentList([{ id: 0, userId: 4, documentName: "", documentUrl: "" }]);
                    _setFormData({ dealer: '', city: '', region: '', search: '', })
                }
            })
            .catch((err) => console.log(err))
            .finally(() => _setLoading(false))

    }

    const getDocumentList = () => {
        var ManagerId=0;var DealerId=0;
        var StateManagerId=0;
        if(user.accessToken.data.userRoleId==4)
        {DealerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==3)
        {ManagerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==2)
        {StateManagerId=user.accessToken.data.userid;}

        getDocumentAccessList(DealerId,ManagerId,StateManagerId)
            .then((resp: any) => {
                console.log(resp?.data)
                if (resp?.data?.data?.responseCode === 200) {
                    debugger;
                    _setTableList([...resp?.data?.data?.data])
                    _setTotalCount([...resp?.data?.data?.data]?.length);
                }
            })
            .catch((err) => { console.log(err) })
    }

    const receiveUploadFiles = (file: any, index: number) => {
        const formData = new FormData();
        formData.append('', file[0]);
        uploadFile(formData)
            .then((resp) => {
                console.log(resp?.data)
                if (resp?.data?.statusCode === 200) {
                    handleDocumentChange(index, 'documentUrl', resp?.data?.data)
                }
            })
            .catch(err => { console.log(err.response); })
    }

    // const getDealerList = () => {
    //     getDealersDetails()
    //         .then((resp) => {
    //             console.log(resp?.data)
    //             if (resp?.data?.data?.responseCode === 200) {
    //                 _setDealerList(resp?.data?.data?.data)
    //             }
    //         })
    //         .catch((err) => console.log(err))
    // }

    const dealersList = () => {
        var ManagerId=0;var UserId=0;
        var StateManagerId=0;
        getAllDealers(UserId,ManagerId,StateManagerId)
            .then(response => {
                debugger;
                _setDealerList(response?.data?.data?.data);
            })
            .catch((err) => console.log(err)).finally(() => _setLoading(false));
    }

    useEffect(() => {
        dealersList();
        getDocumentList();
    }, [])

    const sortedData = _tableList.slice().sort((a: any, b: any) => b.id - a.id);

    const exportEXCEL = () => {
        debugger;
        const header = ["IFMS Id", "Dealer Name", "Upload Date", "Document Name", "Document Download"];

        let body: any = _tableList.map((e: any) => { return [e?.dealerCode, e?.firstName, e?.updateDate, e?.documentName, e?.documentUrl] })

        // var curDate = moment()?.format("YYYY-MM-DD")

        var filename = 'Document Access'

        downloadExcel({

            fileName: filename,

            sheet: "ManagerList",

            tablePayload: {

                header,

                body: body,

            },

        });

    }
    return (<>
        <div className="container">
            <div className="my-3">
                <div className="mx-auto">
                    <div className="fw500">Document Access</div>
                    <div className="row my-2">
                        <div className="my-1 col-md-3">
                            <div className="text-muted fs16">Search by Dealer</div>
                            <FormControl size="small" fullWidth className="" sx={{ "& legend": { display: "none" }, "& fieldset": { top: 0 } }} error={_validate.dealerId.error}>
                                <Select
                                    label=""
                                    value={_formData?.dealerId}
                                    onChange={(e: any) => changeFormData('dealerId', e.target.value)}
                                >
                                    <MenuItem value={0}>All</MenuItem>
                                    {_dealerList?.map((dItem: any, dIndex: number) => (
                                        <MenuItem value={dItem?.id} key={dIndex}>
                                            {dItem?.firstName}
                                        </MenuItem>
                                    ))}
                                </Select>

                                <FormHelperText>{_validate.dealerId.message}</FormHelperText>
                            </FormControl>
                        </div>
                        <div className="my-1 col-md-3">
                            <div className="text-muted fs16">Search by Dealer Code</div>
                            <TextField
                                label=""
                                size="small"
                                className=""
                                fullWidth
                                variant='outlined'
                                value={
                                    _formData?.dealerId === 0
                                        ? "All"
                                        : _dealerList?.filter((fItem: any) => fItem?.id === _formData?.dealerId)[0]?.employeeCode
                                }
                                InputProps={{ readOnly: true }}
                            />
                        </div>
                    </div>
                    <div className="row py-1">
                        <div className="col-md-3 text-muted fs16">File Name</div>
                        <div className="col-md-3 text-muted fs16">Upload Document</div>
                    </div>
                    <div className="pb-4">
                        {_documentList?.map((dItem: any, dIndex: any) =>
                            <div className="row py-1" key={dIndex}>
                                <div className="row align-items-center">
                                    <div className="col-md-3">
                                        <TextField label="" size="small" className="" fullWidth variant='outlined' value={dItem?.documentName} onChange={(e: any) => handleDocumentChange(dIndex, 'documentName', e.target.value)} />
                                    </div>
                                    <div className="col-md-3">
                                        <DragDropUpload fileName={dItem?.documentUrl} sendUploadFiles={(file: any) => receiveUploadFiles(file, dIndex)} labelName={' '} />
                                    </div>
                                    <div className="col-md-1">
                                        <div className="col-1 text-center">
                                            {dIndex === (_documentList?.length - 1) ? <AddCircle role="button" className='fs-2' onClick={addMoreDoc} /> :
                                                <Cancel role="button" className='fs-2' onClick={() => deleteDocList(dIndex)} />}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                        <div className="d-flex my-3">
                            <Button className="transformNone text-dark" size="small" variant="contained" disabled={_loading} onClick={insertItem} style={{background:"linear-gradient(to bottom, #9FD8FD 50%,  #8CCEFF 50%)",border:"2px solid  #0098E1"}}>Save</Button>
                        </div>
                    </div>
                    <div className="row align-items-center border-top py-3">
                        <div className="mt-2 col-md-2">
                            <FormControl className="bg-light" size="small" fullWidth variant="outlined" >
                                <InputLabel htmlFor="outlined-adornment-password">Search</InputLabel>
                                <OutlinedInput className="bg-white"
                                    label="Search"
                                    onChange={(e) => setSearchInput(e.target.value)}
                                    endAdornment={
                                        <InputAdornment position="end">
                                            <IconButton aria-label="toggle password visibility" edge="end" type="submit">
                                                <Search />
                                            </IconButton>
                                        </InputAdornment>
                                    }
                                />
                            </FormControl> </div>
                        <div className="mt-2 col-md-3">
                            <FormControl size="small" fullWidth className="bg-white">
                                <InputLabel>Filter by dealer</InputLabel>
                                <Select label="Filter by dealer" value={selectedDealer} onChange={handleDealerchange}>
                                    <MenuItem value={'selectall'}>All</MenuItem>
                                    {_tableList?.map((item: any, index: number) => (
                                        <MenuItem value={item.firstName}>
                                            {item?.dealerCode != null && item?.dealerCode + " - " + item.firstName}
                                            {item?.dealerCode == null && item.firstName}
                                        </MenuItem>))}
                                </Select>
                            </FormControl>
                        </div>
                        {/* <div className="mt-2 col-md-3">
                            <FormControl size="small" fullWidth className="bg-white">
                                <InputLabel>Filter by city</InputLabel>
                                <Select label="Filter by city" value={_formData?.city} onChange={(e: any) => changeFormData('city', e.target.value)}>
                                    <MenuItem value={'5days'}>Last 5 days</MenuItem>
                                    <MenuItem value={'15days'}>Last 15 days</MenuItem>
                                    <MenuItem value={'30days'}>Last 30 days</MenuItem>
                                    <MenuItem value={'3months'}>Last 3 months</MenuItem>
                                </Select>
                            </FormControl>
                        </div> */}
                        {/* <div className="mt-2 col-md-3">
                            <FormControl size="small" fullWidth className="bg-white">
                                <InputLabel>Filter by region</InputLabel>
                                <Select label="Filter by region" value={_formData?.region} onChange={(e: any) => changeFormData('region', e.target.value)}>
                                    <MenuItem value={'5days'}>Last 5 days</MenuItem>
                                    <MenuItem value={'15days'}>Last 15 days</MenuItem>
                                    <MenuItem value={'30days'}>Last 30 days</MenuItem>
                                    <MenuItem value={'3months'}>Last 3 months</MenuItem>
                                </Select>
                            </FormControl>
                        </div> */}

                        <div className="mt-2 col-md-1">
                            {/* <Button className="bg-primary text-white w-75 textTransformNone py-2">Search</Button> */}
                        </div>
                        <div className="mt-2 col-md-5">

                        </div>
                        <div className="mt-2 col-md-1">
                            <img className="p-2" style={{ height: '55px' }} src={ExportExcel}
                                onClick={exportEXCEL}
                            /> </div>
                    </div>
                </div>
                <Box className="mt-4" sx={{ width: '100%' }}>
                    <Paper className='my-0 shadow' sx={{ width: '100%', mb: 2 }} elevation={0}>
                        <TableContainer>
                            <Table size={"small"}>
                                <TableHead className="" style={{ backgroundColor:"#0098E1"}}>
                                    <TableRow>
                                        <TableCell align="center" className=" fs16 py-3 fw-bold text-white border-3 border-white">IFMS Id</TableCell>
                                        <TableCell align="center" className="fs16 py-3 fw-bold text-white border-3 border-white" >Dealer Name</TableCell>
                                        <TableCell align="center" className=" fs16 py-3 fw-bold text-white border-3 border-white">Upload Date</TableCell>
                                        <TableCell align="center" className=" fs16 py-3 fw-bold text-white border-3 border-white">Document Name</TableCell>
                                        {/* <TableCell align="center" className=" py-3 fw500 text-white">Document Download</TableCell> */}
                                        <TableCell align='center' className=" fs16 py-3 fw-bold text-white border-3 border-white">Action</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody sx={{ borderRadius: '10px !important' }}>
                                    {sortedData?.length > 0
                                        ? sortedData.filter((content: any) => {
                                            const lowerSearchInput = searchInput.toLowerCase();
                                            const lowerSelectedDealer = selectedDealer.toLowerCase();
                                            // const lowerSelectedManager = selectedManager.toLowerCase();
                                            // const lowerSelectedstatus = selectedstatus.toLowerCase();
                                            return (

                                                (lowerSearchInput === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchInput))) &&
                                                (lowerSelectedDealer === '' || content.firstName?.toLowerCase() === lowerSelectedDealer || lowerSelectedDealer === 'selectall')
                                                // (lowerSelectedstatus === '' || content.status?.toLowerCase() === lowerSelectedstatus || lowerSelectedstatus === 'selectall' ) &&
                                                // (lowerSelectedManager === '' || (content.firstName + ' ' + content.lastName).toLowerCase() === lowerSelectedManager || lowerSelectedManager ===  'selectall')
                                            );
                                        })
                                            ?.slice(
                                                (_page - 1) * _rowsPerPage,
                                                (_page - 1) * _rowsPerPage + _rowsPerPage,
                                            )
                                            ?.map((item: any, index: any) => (


                                                <TableRow key={index} className={index % 2 === 0 ? 'grey-row' : 'white-row'} onClick={() => handleRowClick(index)}>
                                                    <TableCell className="fs16 py-2  border-3 border-white" align="center"> {!hiddenRows[index] && <span className="bg-success p-1 text-white rounded">new</span>}{item?.dealerCode || '-'}</TableCell>
                                                    <TableCell className="fs16 py-2  border-3 border-white" align="center">{item?.firstName || '-'}</TableCell>
                                                    <TableCell className="fs16 py-2  border-3 border-white" align="center">{item?.updateDate || '-'}</TableCell>
                                                    <TableCell className="fs16 py-2  border-3 border-white" align="center">{item?.documentName || '-'}</TableCell>

                                                
                                                    {/* <TableCell className="py-2 fw500" align="center">
                                                        {item?.documentUrl || '-'}
                                                        </TableCell> */}
                                                    <TableCell align='center' className="py-2 fw500 border-3 border-white">
                                                        <CloudDownloadIcon
                                                        onClick={()=>downloadDocument(item?.documentUrl)} 
                                                        />
                                                    </TableCell>
                                                </TableRow>
                                            )) : (
                                            <>
                                                {!_loading ? (
                                                    <TableRow key={0} >
                                                        <TableCell sx={{ borderBottom: 'none', borderRadius: '10px !important' }} align={"center"} colSpan={5}>
                                                            <h3 className='text-muted'>No Data </h3>
                                                        </TableCell>
                                                    </TableRow>
                                                ) : (<></>)
                                                }
                                            </>
                                        )}
                                    <SkeletonProviderTables columns={5} visible={_loading} />

                                </TableBody>
                            </Table>
                        </TableContainer>


                    </Paper>
                </Box>
                <div className="d-flex justify-content-end align-items-center mt-3 pb-4">
                    <Pagination count={Math.ceil(_totalCount / _rowsPerPage)} page={_page} onChange={handleChangePage}
                        variant="outlined" size={"small"} color={"primary"} shape="rounded"
                        renderItem={(item) => (
                            <PaginationItem sx={{ mx: '4px' }}
                                {...item}
                            />
                        )} />
                </div>
            </div>
        </div>
    </>
    )
}