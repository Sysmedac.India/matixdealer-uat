import React from 'react';
import "./style.scss";

function Index() {
    return (<div className="d-flex justify-content-center align-items-center vh-100">
        <div className="position-relative w-100">
            <div className="notfound">
                <div className="notfound-404">
                    <h3>Oops! Page not found</h3>
                    <h1><span>4</span><span>0</span><span>4</span></h1>
                </div>
            </div>
        </div>
    </div>);
}

export default Index;
