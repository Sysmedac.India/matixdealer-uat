import { FormControl, InputLabel, MenuItem, Select, TextField, Dialog, Paper, Pagination, PaginationItem, OutlinedInput, InputAdornment, IconButton, Button } from "@mui/material";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Search } from "@mui/icons-material";
import '../../assets/scss/page.css';
import { getAllDealers, getDealersDetails } from "../../models/model";
import { useEffect, useState } from "react";
import { SkeletonProviderTables } from "../../providers/SkeletonProvider";
import ExportExcel from '../../assets/images/Export Excel.png';
import { downloadExcel } from "react-export-table-to-excel";
import { useStateValue } from '../../providers/StateProvider'
function Dealers() {

    const [_loading, _setLoading] = useState<any>(true);
    const [_list, _setList] = useState<any>([]);
    const [_userteamslist, _setUserTeamsList] = useState<any>([]);
    const [searchInput, setSearchInput] = useState("");
    const [_assignList, _setAssignList] = useState<any>([])
    const [_editList, _setEditList] = useState<any>([])
    const [_popup, _setPopup] = useState<any>(false);
    const [selectedstatus, setSelectedstatus] = useState('');
    const [_checkvalue, _setCheckvalue] = useState<any>({});
    const [_formCheckvalue, _setFormCheckvalue] = useState<any>({});
    const [selectedCity, setSelectedCity] = useState('');
    const [selectedManager, setSelectedManager] = useState('');
    const [_createManager, _setCreateManager] = useState<any>(false);
    const [_page, _setPage] = useState(1);
    const [_rowsPerPage] = useState(10);
    const [_totalCount, _setTotalCount] = useState(0);
    const [{user}, dispatch]: any = useStateValue();


    const handleChangePage = (event: React.ChangeEvent<unknown>, value: number) => {
        _setPage(value);
    };
    const handleStatusChange = (event: any) => {
        setSelectedstatus(event.target.value);
    };
    const handleCityChange = (event: any) => {
        setSelectedCity(event.target.value); // Update the selectedCity state
    };
    const handleManagerChange = (event: any) => {
        setSelectedManager(event.target.value); // Update the selectedCity state
    };


    const gridBindDetails = () => {
        debugger;
        var ManagerId=0;var UserId=0;
        var StateManagerId=0;
        if(user.accessToken.data.userRoleId==4)
        {UserId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==3)
        {ManagerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==2)
        {StateManagerId=user.accessToken.data.userid;}
        getAllDealers(UserId,ManagerId,StateManagerId)
            .then(response => {
                _setList(response?.data?.data?.data);
                _setTotalCount([...response?.data?.data?.data]?.length);
                console.log(_list);
            })
            .catch((err) => console.log(err)).finally(() => _setLoading(false));
    }

    const dateCell: any = document.getElementById('dateCell');
    if (dateCell) {
        const cellDateString = dateCell.textContent;
        const cellDateParts = cellDateString.split('-');
        const cellDate = new Date(`${cellDateParts[2]}-${cellDateParts[1]}-${cellDateParts[0]}`);
        const currentDate = new Date();
     
        if (cellDate < currentDate) {
           // dateCell.style.backgroundColor  = 'red';
            dateCell.style.color = 'red';
        } else {
           // dateCell.style.backgroundColor  = 'green';
            dateCell.style.color = 'green';
        }
    }


    useEffect(() => {
        gridBindDetails();
    }, []);

    const exportEXCEL = () => {
        debugger;
        const header = ["IFMS Id", "Name", "City", "Address", "Region", "Phone No", "Designation", "Email", "Manager", "Urea Lisc", "Other Fertilizer Lisc", "Pesticide Lisc", "PAN", "Status"];

        let body: any = _list.map((e: any) => {
            const fullName = e?.firstName + ' ' + e?.lastName;
            return [e?.employeeCode, fullName, e?.city, e?.address, e?.countryName, e?.mobileNumber, e?.designation, e?.emailId, e?.managerName !== null && e?.managerName?.firstName || '', e?.ureaLisc, e?.otherFertilizerLisc, e?.pesticideLisc, e?.seedLisc, e?.status]
        })

        // var curDate = moment()?.format("YYYY-MM-DD")

        var filename = 'Dealers'

        downloadExcel({

            fileName: filename,

            sheet: "DealerList",

            tablePayload: {

                header,

                body: body,

            },

        });

    }

    const sortedData = _list.slice().sort((a: any, b: any) => b.id - a.id);
    return (<>
        {_createManager === false && (
            <div>
                <div className="my-3 mt-4">
                    <div className="col-md-12 mx-auto mt-4">
                        <div className="row align-items-center">
                            <div className="col-md-1"></div>
                            <div className="col-md-10"><div className="row">
                                <div className="mt-2 col-md-3">
                                    <FormControl className="bg-light" size="small" fullWidth variant="outlined" >
                                        <InputLabel htmlFor="outlined-adornment-password">Search</InputLabel>
                                        <OutlinedInput className="bg-white"
                                            label="Search"
                                            onChange={(e) => setSearchInput(e.target.value)}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton aria-label="toggle password visibility" edge="end" type="submit">
                                                        <Search />
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by city</InputLabel>
                                        <Select label="Select Range" value={selectedCity}
                                            onChange={handleCityChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={item.city}>{item.city}</MenuItem>
                                            ))}
                                            {/* {_list && _list.length > 0 && _list
                                                .filter((item: any) => item?.city !== null)
                                                .map((item: any, index: any) => (
                                                    item.city ? ( // Check if managerName is not null
                                                        <MenuItem value={item.cty}>
                                                            {item.city}
                                                        </MenuItem>
                                                    ) : null
                                                ))} */}
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by Manager</InputLabel>
                                        <Select label="Select Range" value={selectedManager} onChange={handleManagerChange}>
                                            {_list && _list.length > 0 && _list
                                                .filter((item: any) => item?.managerName?.firstName !== null && item?.managerName?.lastName !== null)
                                                .map((item: any, index: any) => (
                                                    item.managerName ? ( // Check if managerName is not null
                                                        <MenuItem value={`${item.managerName.firstName} ${item.managerName.lastName}`}>
                                                            {item.managerName.firstName} {item.managerName.lastName}
                                                        </MenuItem>
                                                    ) : null
                                                ))}
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl size="small" fullWidth className="bg-white">
                                        <InputLabel>Filter by Status</InputLabel>
                                        <Select label="Select Range" value={selectedstatus} onChange={handleStatusChange} >
                                            <MenuItem value={'Active'}>Active</MenuItem>
                                            <MenuItem value={'InActive'}>InActive</MenuItem>
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                            {/* <MenuItem value={'30days'}>Last 30 days</MenuItem>
                                            <MenuItem value={'3months'}>Last 3 months</MenuItem> */}
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    {/* <Button type="button" style={{ backgroundColor: "#00AAE7" }} className="btn text-white">
                                        Search
                                    </Button> */}
                                </div> <div className="mt-2 col-md-1">
                                    <img className="p-2" style={{ height: '55px' }} src={ExportExcel}
                                        onClick={exportEXCEL}
                                    />
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-md-1">
                        </div>
                        <div className="col-md-10">
                            <div className="tablebx" style={{ overflowX: "auto" }}>

                                <Paper sx={{ width: '100%', mb: 1 }} elevation={0}>
                                    <TableContainer className="rounded">
                                        <Table className="border" size="small">
                                            <TableHead className=" py-4" style={{ borderBottom: '0.1rem  solid gray', height: "4rem",backgroundColor:"#0098E1" }}>
                                                <TableRow>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>S.No.</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Dealer Code</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>IFMS Code</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Name</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>City</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Address</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>State</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Mobile</TableCell>
                                                    {/* <TableCell className="py-2 fw-bold text-white" align={"center"}>Designation</TableCell> */}
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Email</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Manager</TableCell>
                                                    {/* <TableCell className="py-2 fw-bold text-white" align={"center"}>PAN</TableCell> */}
                                                    {/* <TableCell className="py-2 fw-bold text-white" align={"center"}>GST</TableCell> */}
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Urea Lisc </TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Other Fertilizer Lisc </TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Pesticide Lisc </TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>PAN </TableCell>
                                                  
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {/* {_list?.length > 0 ?  _list.filter((content: any) => {
                                                        const lowerSearchInput = searchInput.toLowerCase();
                                                        const lowerSelectedCity = selectedCity.toLowerCase();
                                                        const lowerSelectedManager = selectedManager.toLowerCase();
                                                        const lowerSelectedstatus = selectedstatus.toLowerCase();
                                                        return (
                                                            (lowerSearchInput === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchInput))) &&
                                                            (lowerSelectedCity === '' || content.city?.toLowerCase() === lowerSelectedCity||lowerSelectedCity==='selectall') &&
                                                            (lowerSelectedstatus === '' || content.status?.toLowerCase() === lowerSelectedstatus||lowerSelectedstatus==='selectall') &&
                                                            (lowerSelectedManager === '' || (content.managerName?.firstName + ' ' + content.managerName?.lastName).toLowerCase() === lowerSelectedManager||lowerSelectedManager==='selectall')
                                                        );
                                                    })?.slice(
                                                    (_page - 1) * _rowsPerPage,
                                                    (_page - 1) * _rowsPerPage + _rowsPerPage,
                                                )?.map((item: any, index: number) => (
                                                    <TableRow key={index} className={index % 2 === 0 ? 'white-row' : 'grey-row'} style={{height:"3rem"}}>
                                                        <TableCell className="" align={"center"}>{(index + 1) + ((_page - 1) * 10)}</TableCell>
                                                        <TableCell align="center">{item?.employeeCode}</TableCell>
                                                        <TableCell className="p-0 m-0" align="center">{item?.firstName}{item.lastName}</TableCell>
                                                        <TableCell align="center">{item?.city}</TableCell>
                                                        <TableCell align="center">{item?.address}</TableCell>
                                                        <TableCell align="center">{item?.countryName}</TableCell>
                                                        <TableCell align="center">{item?.mobileNumber}</TableCell>
                                                        <TableCell align="center">{item?.designation}</TableCell>
           
                                                        <TableCell align="center">{item?.emailId}</TableCell>
                                                        <TableCell align="center">{item?.managerName?.firstName}</TableCell>
                                                        <TableCell align="center">{'-'}</TableCell>
                                                        <TableCell align="center">{'-'}</TableCell>
                                                        <TableCell align="center">{'-'}</TableCell>
                                                        <TableCell align="center">{'-'}</TableCell>
                                                        <TableCell align="center">{item?.status}</TableCell>
                                                    </TableRow>
                                                ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">Data Not Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )} */}

                                                {sortedData?.length > 0 ? sortedData.filter((content: any) => {
                                                    const lowerSearchInput = searchInput.toLowerCase();
                                                    const lowerSelectedCity = selectedCity.toLowerCase();
                                                    const lowerSelectedManager = selectedManager.toLowerCase();
                                                    const lowerSelectedstatus = selectedstatus.toLowerCase();
                                                    return (
                                                        (lowerSearchInput === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchInput))) &&
                                                        (lowerSelectedCity === '' || content.city?.toLowerCase() === lowerSelectedCity || lowerSelectedCity === 'selectall') &&
                                                        (lowerSelectedstatus === '' || content.status?.toLowerCase() === lowerSelectedstatus || lowerSelectedstatus === 'selectall') &&
                                                        (lowerSelectedManager === '' || (content.managerName?.firstName + ' ' + content.managerName?.lastName).toLowerCase() === lowerSelectedManager || lowerSelectedManager === 'selectall')
                                                    );
                                                })?.slice(
                                                    (_page - 1) * _rowsPerPage,
                                                    (_page - 1) * _rowsPerPage + _rowsPerPage,
                                                )?.map((item: any, index: number) => (
                                                    <TableRow key={index} className={index % 2 === 0 ? 'grey-row' : 'white-row'} style={{ height: "3rem" }}>
                                                        <TableCell className="fs16" align={"center"}>{(index + 1) + ((_page - 1) * 10)}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item?.employeeCode}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item?.ifmsCode}</TableCell>
                                                        <TableCell className="fs16 p-0 m-0 border-3 border-white" align="center">{item?.firstName}{item.lastName}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item?.city}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item?.address}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item?.stateName}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item?.mobileNumber}</TableCell>
                                                        {/* <TableCell align="center">{item?.designation}</TableCell> */}
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item?.emailId}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item?.managerName?.firstName}</TableCell>
                                                        {/* <TableCell align="center">{item?.pan}</TableCell> */}
                                                        {/* <TableCell align="center">{'-'}</TableCell> */}
                                                        <TableCell align="center" className="fs16 my-auto border-3 border-white" id="dateCell">{item?.cstNo}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item?.lstNo}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item?.serviceTaxRegnNo}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item?.pan}</TableCell>
                                                        {/* <TableCell align="center">{item?.status}</TableCell> */}
                                                    </TableRow>
                                                ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">Data Not Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}

                                                <SkeletonProviderTables columns={9} visible={_loading} />
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    <div className="d-flex justify-content-end align-items-center mt-3 pb-4">
                                        <Pagination count={Math.ceil(_totalCount / _rowsPerPage)} page={_page} onChange={handleChangePage}
                                            variant="outlined" size={"small"} color={"primary"} shape="rounded"
                                            renderItem={(item) => (
                                                <PaginationItem sx={{ mx: '4px' }}
                                                    {...item}
                                                />
                                            )} />
                                    </div>
                                </Paper>

                            </div>
                        </div>
                        <div className="col-md-1">
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-6"></div>
                        <div className="col-md-6"></div>
                    </div>
                </div>

            </div>)
        }
    </>);
}

export default Dealers;


