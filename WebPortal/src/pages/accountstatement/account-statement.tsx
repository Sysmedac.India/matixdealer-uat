import { FormControl, InputLabel, MenuItem, Select, TextField, Dialog, Paper, Pagination, PaginationItem, OutlinedInput, InputAdornment, IconButton, Button } from "@mui/material";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Search } from "@mui/icons-material";
import '../../assets/scss/page.css';
import { getAllAnnouncements, getAllDealers } from "../../models/model";
import { useEffect, useState } from "react";
import { SkeletonProviderTables } from "../../providers/SkeletonProvider";
import ExportExcel from '../../assets/images/Export Excel.png';
import { downloadExcel } from "react-export-table-to-excel";
import moment from "moment";
import {
    Search as IconSearch,
    Replay as IconReplay
} from "@mui/icons-material";
import { useStateValue } from '../../providers/StateProvider';
import { ROUTES } from "../../configs/constants";

function AccountStatement() {

    const [_loading, _setLoading] = useState<any>(true);
    const [_list, _setList] = useState<any>([]);
    const [_userteamslist, _setUserTeamsList] = useState<any>([]);
    const [searchInput, setSearchInput] = useState("");
    const [_assignList, _setAssignList] = useState<any>([])
    const [_editList, _setEditList] = useState<any>([])
    const [_popup, _setPopup] = useState<any>(false);
    const [_checkvalue, _setCheckvalue] = useState<any>({});
    const [_formCheckvalue, _setFormCheckvalue] = useState<any>({});
    const [selectedCity, setSelectedCity] = useState('');
    const [selectedManager, setSelectedManager] = useState('');
    const [_createManager, _setCreateManager] = useState<any>(false);
    const [_page, _setPage] = useState(1);
    const [_rowsPerPage] = useState(10);
    const [_totalCount, _setTotalCount] = useState(0);
    const [_showDate,_setShowDate] = useState<any>(false);
    const [{ user }, dispatch]: any = useStateValue();

    const [_formData, _setFormData] = useState<any>({
        startDate: '',
        endDate: '',
    });

    const validate = {
        startDate: { error: false, message: "" },
        endDate: { error: false, message: "" },
    };

    const [_datevalidate, _setDatevalidate] = useState(validate);

    const searchByDate = () => {
    debugger;
    const _validate: any = Object.assign({}, validate);
    let valid = true;
    if (_formData.startDate === "" || _formData.startDate === 0) {
        _validate.startDate.error = true;
        _validate.startDate.message = "Required Field";
        valid = false;
    }
    if (_formData.endDate === "" || _formData.endDate === 0) {
        _validate.endDate.error = true;
        _validate.endDate.message = "Required Field";
        valid = false;
    }
    _setDatevalidate(_validate);
    if (!valid) {
        return;
    }

    _setShowDate(true)
    }

    const changeFormData = (name: string, value: any) => {
        _setFormData({ ..._formData, [name]: value });
    };

    const handleChangePage = (event: React.ChangeEvent<unknown>, value: number) => {
        _setPage(value);
    };

    const changeDateFormat = (date: any) => {
        return moment(date).format("DD/MM/YYYY");
    }

    const handleCityChange = (event: any) => {
        setSelectedCity(event.target.value); // Update the selectedCity state
    };
    const handleManagerChange = (event: any) => {
        setSelectedManager(event.target.value); // Update the selectedCity state
    };

    const gridBindDetails = () => {
        var ManagerId=0;var UserId=0;
        var StateManagerId=0;
        if(user.accessToken.data.userRoleId==4)
        {UserId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==3)
        {ManagerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==2)
        {StateManagerId=user.accessToken.data.userid;}
        getAllDealers(UserId,ManagerId,StateManagerId)
            .then(response => {
                debugger;
                _setList(response?.data?.data?.data);
                _setTotalCount([...response?.data?.data?.data]?.length);
                console.log(_list);
            })
            .catch((err) => console.log(err)).finally(() => _setLoading(false));
    }

    const downloadDocument = (employeecode:any) => {
        var stDate=moment(_formData?.startDate).format("DD.MM.YYYY");
        var enDate=moment(_formData?.endDate).format("DD.MM.YYYY");
        // window.open('http://matixeccqas.panlocal.mfcl:8000/sap/bc/abap/zrest_api/matrix/' +
        // '?sap-client=220' +
        // '&ID=DPDF'+ 
        // '&SapDealerNo='+employeecode+
        // '&FrmDate=' +stDate +
        // '&ToDate=' +enDate)
//https://localhost:7260/api/Matix/downloadInvoice?SapDealerNo=0000000087&FromDate=10.05.2023&ToDate=10.10.2023
        window.open(ROUTES.api.DownloadInvoice+
        '?SapDealerNo='+employeecode+
        '&FromDate=' +stDate +
        '&ToDate=' +enDate)
    }
 

    // const gridBindDetails = () => {
    //     getAllAnnouncements("","")
    //         .then(response => {
    //             _setList(response?.data?.data?.data);
    //             _setTotalCount([...response?.data?.data?.data]?.length);
    //             console.log(_list);
    //         })
    //         .catch((err) => console.log(err)).finally(() => _setLoading(false));
    // }

  
    const clearSearch = (e: any) => {
        e.preventDefault();

        _setFormData({
            ..._formData,
            startDate: '',
            endDate: '',
        });
        _setShowDate(false)
    }


    useEffect(() => {
        gridBindDetails();
    }, []);

    const exportEXCEL = () => {
        debugger;
        const header = ["IFMS Id", "Dealer", "Start Date", "End Date", "Statement of Account"];

        let body: any = _list.map((e: any) => {

            return [e?.employeeCode, e?.announcement1, e?.fromDate, e?.toDate]
        })

        // var curDate = moment()?.format("YYYY-MM-DD")

        var filename = 'AccountStatement'

        downloadExcel({

            fileName: filename,

            sheet: "DealerList",

            tablePayload: {

                header,

                body: body,

            },

        });

    }

    return (<>
        {_createManager === false && (
            <div>
                <div className="my-3 mt-4">
                    <div className="col-md-12 mx-auto mt-4">
                        <div className="row align-items-center">
                            <div className="col-md-1"></div>
                            <div className="col-md-10"><div className="row">
                                <div className="mt-2 col-md-3">
                                    <FormControl className="bg-light" size="small" fullWidth variant="outlined" >
                                        <InputLabel htmlFor="outlined-adornment-password">Search</InputLabel>
                                        <OutlinedInput className="bg-white"
                                            label="Search"
                                            onChange={(e) => setSearchInput(e.target.value)}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton aria-label="toggle password visibility" edge="end" type="submit">
                                                        <Search />
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2" style={{ color: "#00AAE7" }}>
                                    <TextField
                                        fullWidth
                                        //className='mt-1'
                                        size='small'
                                        type='date'
                                        value={_formData?.startDate}
                                        onChange={(event) =>
                                            changeFormData("startDate", event.target.value)
                                        }
                                        InputLabelProps={{ shrink: true }}
                                        error={_datevalidate?.startDate?.error}
                                        helperText={_datevalidate?.startDate?.message}
                                    />
                                </div>

                                <div className="mt-2 col-md-2" style={{ color: "#00AAE7" }}>
                                    <TextField
                                        fullWidth
                                      //  className='mt-1'
                                        size='small'
                                        type='date'
                                        value={_formData?.endDate}
                                        onChange={(event) =>
                                            changeFormData("endDate", event.target.value)
                                        }
                                        InputLabelProps={{ shrink: true }}
                                        error={_datevalidate?.endDate?.error}
                                        helperText={_datevalidate?.endDate?.message}
                                    />
                                </div>
                                <div className="mt-2 col-md-2">
                            <Button className="bg-primary text-dark w-75 textTransformNone" style={{background:"linear-gradient(to bottom, #9FD8FD 50%,  #8CCEFF 50%)",border:"2px solid  #0098E1"}} onClick={searchByDate}>Search</Button>
                        </div>
                        <div className="mt-2 col-md-1">
                            <IconButton type="button" sx={{ p: '10px' }} aria-label="reset" onClick={clearSearch}>
                                <IconReplay />
                            </IconButton>
                        </div>


                                {/* <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by city</InputLabel>
                                        <Select label="Select Range" value={selectedCity}
                                            onChange={handleCityChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={item.city}>{item.city}</MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                </div> */}

                                {/* <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by region</InputLabel>
                                        <Select label="Select Range" value={selectedCity}
                                            onChange={handleCityChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={item.region}>{item.region}</MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                </div> */}
                                {/* <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by state</InputLabel>
                                        <Select label="Select Range" value={selectedCity}
                                            onChange={handleCityChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={item.state}>{item.state}</MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                </div> */}
                                {/* <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by Manager</InputLabel>
                                        <Select label="Select Range" value={selectedManager} onChange={handleManagerChange}>
                                            {_list.map((item: any, index: number) => (
                                                <MenuItem value={`${item?.managerName?.firstName} ${item?.managerName?.lastName}`}>{item?.managerName?.firstName} {item?.managerName?.lastName}</MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                </div> */}
                              
                                    {/* <Button type="button" style={{ backgroundColor: "#00AAE7" }} className="btn text-white">
                                    Search
                                </Button> */}
                                
                                <div className="mt-2 col-md-1">
                                    {/* <img className="p-2" style={{ height: '55px' }} src={ExportExcel}
                                        onClick={exportEXCEL}
                                    /> */}
                                </div>
                            </div>
                            </div>

                            <div className="mt-2 col-md-1">
                            </div>
                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-md-1">
                        </div>
                        <div className="col-md-10">
                            <div className="tablebx" style={{ overflowX: "auto" }}>

                                <Paper sx={{ width: '100%', mb: 1 }} elevation={0}>
                                    <TableContainer className="rounded">
                                        <Table className="border" size="small">
                                            <TableHead className=" py-4" style={{ borderBottom: '0.1rem  solid gray', backgroundColor:"#0098E1", height: "4rem" }}>
                                                <TableRow>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>S.No.</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Dealer Code</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Dealer Name</TableCell>
                                                    {_showDate && (
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>StartDate</TableCell>)}
                                                   {_showDate && (  
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>EndDate</TableCell>)}
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Statement of Account</TableCell>

                                                </TableRow>
                                            </TableHead>
                                            <TableBody>

                                                {_list?.length > 0 ? _list.filter((content: any) => {
                                                    const lowerSearchInput = searchInput.toLowerCase();
                                                    const lowerSelectedCity = selectedCity.toLowerCase();
                                                    const lowerSelectedManager = selectedManager.toLowerCase();

                                                    return (

                                                        (lowerSearchInput === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchInput))) &&
                                                        (lowerSelectedCity === '' || content.city?.toLowerCase() === lowerSelectedCity) &&
                                                        (lowerSelectedManager === '' || (content.managerName.firstName + ' ' + content.managerName.lastName).toLowerCase() === lowerSelectedManager)
                                                    );
                                                })?.slice(
                                                    (_page - 1) * _rowsPerPage,
                                                    (_page - 1) * _rowsPerPage + _rowsPerPage,
                                                )?.map((item: any, index: number) => (
                                                    <TableRow key={index} className={index % 2 === 0 ? 'grey-row' : 'white-row'}>
                                                        <TableCell align={"center"} className="fs16 border-3 border-white">{(index + 1) + ((_page - 1) * 10)}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item.employeeCode}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item.firstName}</TableCell>
                                                        {_showDate && (
                                                        <TableCell align="center" className="fs16 border-3 border-white">{changeDateFormat(_formData.startDate)}</TableCell>)}
                                                        {_showDate && (
                                                        <TableCell align="center" className="fs16 border-3 border-white">{changeDateFormat(_formData.endDate)}</TableCell>)}
                                                        <TableCell align="center" className="fs16 border-3 border-white"><CloudDownloadIcon onClick={(e)=>downloadDocument(item.employeeCode)} /></TableCell>
                                                    </TableRow>

                                                ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">Data Not Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                                <SkeletonProviderTables columns={9} visible={_loading} />
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    <div className="d-flex justify-content-end align-items-center mt-3 pb-4">
                                        <Pagination count={Math.ceil(_totalCount / _rowsPerPage)} page={_page} onChange={handleChangePage}
                                            variant="outlined" size={"small"} color={"primary"} shape="rounded"
                                            renderItem={(item) => (
                                                <PaginationItem sx={{ mx: '4px' }}
                                                    {...item}
                                                />
                                            )} />
                                    </div>
                                </Paper>

                            </div>
                        </div>
                        <div className="col-md-1">
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-6"></div>
                        <div className="col-md-6"></div>
                    </div>
                </div>

            </div>)
        }
    </>);
}

export default AccountStatement;


