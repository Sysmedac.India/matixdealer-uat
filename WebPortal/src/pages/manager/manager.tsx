
import DatePickerControl from "../../components/helpers/DatePickerControl";
import { FormControl, InputLabel, MenuItem, Select, TextField, Dialog, Paper, FormControlLabel, Checkbox, OutlinedInput, InputAdornment, IconButton, Button, Pagination, PaginationItem, FormHelperText } from "@mui/material";
import SubdirectoryArrowRightIcon from '@mui/icons-material/SubdirectoryArrowRight';
import Table from '@mui/material/Table';
import { SkeletonProviderTables } from "../../providers/SkeletonProvider";
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { ArrowDownwardRounded, Search } from "@mui/icons-material";
import '../../assets/scss/page.css';
import { CheckBox, MoreVertRounded } from "@mui/icons-material";
import CheckBoxTableIcon from '@mui/icons-material/CheckBox';
import { blue, green, red } from "@mui/material/colors";
import DeleteForeverRoundedIcon from '@mui/icons-material/DeleteForeverRounded';
import { getStateManagerDetails, getusersByUserTeams, insertAssingmanager, activeStatusManager, addManager, getDesignationList, getAllAssignedUsers, getAllStatemanager } from "../../models/model";
import { RemoveManager } from "../../models/model";
import { useEffect, useState } from "react";
import EditIcon from '@mui/icons-material/Edit';
import Swal from 'sweetalert2';
import CancelIcon from '@mui/icons-material/Cancel';
import { validateEmail, validateMobile } from "../../services/ValidationService";
import '../../assets/scss/app.scss';
import ExportExcel from '../../assets/images/Export Excel.png';
import { downloadExcel } from "react-export-table-to-excel";
import moment from "moment";
import { useStateValue } from '../../providers/StateProvider'
function Manager() {
    // const [_formData, _setFormData] = useState<any>({
    //     startDate: dayjs(moment().format('YYYY/MM/DD')) || null,
    //     endDate: dayjs(moment().format('YYYY/MM/DD')) || null,
    //     day: '',
    // });

    const [openremoveCheckedUserIds, setOpenremoveCheckedUserIds] = useState<number[]>([]);
    const [checkedUserIds, setCheckedUserIds] = useState<number[]>([]);
    const [assignCheckedUserIds, setAssignCheckedUserIds] = useState<number[]>([]);
    const [_list, _setList] = useState<any>([]);
    const [_userteamslist, _setUserTeamsList] = useState<any>([]);
    const [searchInput, setSearchInput] = useState("");
    const [searchremove, setSearchremove] = useState("");
    const [assigncity, setAssigncity] = useState("");
    const [dealer, setDealer] = useState('');
    const [_assignList, _setAssignList] = useState<any>([])
    const [_editList, _setEditList] = useState<any>([])
    const [dialog, setDialog] = useState(false)
    const [assign, setAssign] = useState(false)
    const [_popup, _setPopup] = useState<any>(false);
    const [_checkvalue, _setCheckvalue] = useState<any>({});
    const [_formCheckvalue, _setFormCheckvalue] = useState<any>({});
    const [selectedCity, setSelectedCity] = useState('');
    const [selectedManager, setSelectedManager] = useState('');
    const [selectedstatus, setSelectedstatus] = useState('');
    const [selectedregion, setSelectedregion] = useState('');
    const [selectedname, setSelectedname] = useState('');
    const [assingname, setAsigname] = useState('');
    const [isChecked, setisChecked] = useState<string[]>([]);
    const [delmsg, setDelmsg] = useState('');
    const [_createManager, _setCreateManager] = useState<any>(false);
    const [_page, _setPage] = useState(1);
    const [_rowsPerPage] = useState(10);
    const [_totalCount, _setTotalCount] = useState(0);
    const [_loading, _setLoading] = useState<any>(true);
    const [asignChecked, setAsignChecked] = useState<string[]>([]);
    const [_buttonLoading, _setbuttonLoading] = useState(false);
    const [_managerId, _setManagerId] = useState<any>('');
    const [_type, _setType] = useState<any>("create");
    const [_userId, _setUserId] = useState("");
    const [_titleresponse, _settitleresponse] = useState<any>([]);
    const [{user}, dispatch]: any = useStateValue();

    const gettitle = () => {
        getDesignationList()
            .then((response) => {
                debugger;
                _settitleresponse(response.data.data);
                console.log(response.data);
            })
            .catch((error) => {
                console.log(error.response);
            });
    };

    const [_formData, _setFormData] = useState<any>({
        userId: 0,
        employeeCode: "",
        firstName: "",
        mobileNumber: "",
        emailId: "",
        stateName: "",
        city: "",
        status: "",
    });

    const changeFormData = (name: string, value: any) => {
        _setFormData({ ..._formData, [name]: value });
    };

    const label = { inputProps: { 'aria-label': 'Checkbox demo' } };


    const handleCityChange = (event: any) => {
        setSelectedCity(event.target.value); // Update the selectedCity state
    };
    const handleManagerChange = (event: any) => {
        setSelectedManager(event.target.value); // Update the selectedCity state
    };
    const handleStatusChange = (event: any) => {
        setSelectedstatus(event.target.value);
    };
    const handleregionChange = (event: any) => {
        setSelectedregion(event.target.value);
    }
    const handleassignName = (event: any) => {
        setAsigname(event.target.value)
    }
    const handleassingcity = (event: any) => {
        setAssigncity(event.target.value)
    }
    const handlenameChange = (event: any) => {
        setSelectedname(event.target.value);
    }
    const handleActionClick = () => {
        _setPopup(true)
    }

    const handleChangePage = (event: React.ChangeEvent<unknown>, value: number) => {
        _setPage(value);
    };


    const cancelManager = () => {
        _setCreateManager(false)
        _setFormData({
            ..._formData,
            userId: 0,
            employeeCode: "",
            firstName: "",
            mobileNumber: "",
            emailId: "",
            stateName: "",
            city: "",
            designation: "",
            status: "",
        });
        _setType('')
    }

    const activeChecked = (val: any, status: any) => {
        debugger;
        activeStatusManager(val, status).then(response => {
        })
        gridBindDetails();
    }

    const openremove = (val: any) => {
        getusersByUserTeams(val)
            .then(res => {
                debugger;
                if (res?.data?.data?.isSuccess === true) {
                    _setUserTeamsList(res?.data?.data?.data);
                    _setManagerId(val)
                }
                else {
                    _setUserTeamsList([]);
                }
            })
            .catch(err => {

            });
        setDialog(true);
    }

    const openassign = (val: any) => {
        setAssign(true)
        assignDetails(val)
    }


    const actionClick = (type: any, item?: any) => {
        _setCreateManager(true)
        _setType(type);
        _setUserId(item?.id)
        _setFormData({
            userId: item.id,
            employeeCode: item.employeeCode,
            firstName: item.firstName,
            mobileNumber: item.mobileNumber,
            emailId: item.emailId,
            stateName: item.countryName,
            city: item.city,
            designation: item.designation,
            status: item.status,
        });
    }


    const createManager = () => {
        _setCreateManager(true)
    }

    const validate = {
        employeeCode: { error: false, message: "" },
        firstName: { error: false, message: "" },
        mobileNumber: { error: false, message: "" },
        city: { error: false, message: "" },
        stateName: { error: false, message: "" },
        emailId: { error: false, message: "" },
        designation: { error: false, message: "" }
    };
    const [_managervalidate, _setManagervalidate] = useState(validate);

    const InserManager = () => {
        debugger;
        //  var userid = user.id;
        const _validate: any = Object.assign({}, validate);
        let valid = true;
        if (_formData.employeeCode === "" || _formData.employeeCode === 0) {
            _validate.employeeCode.error = true;
            _validate.employeeCode.message = "Required Field";
            valid = false;
        }
        if (_formData.firstName === "" || _formData.firstName === 0) {
            _validate.firstName.error = true;
            _validate.firstName.message = "Required Field";
            valid = false;
        }
        if (_formData.mobileNumber === "" || _formData.mobileNumber === 0) {
            _validate.mobileNumber.error = true;
            _validate.mobileNumber.message = "Required Field";
            valid = false;
        }
        if (!validateMobile(_formData.mobileNumber)) {
            _validate.mobileNumber.error = true;
            _validate.mobileNumber.message = "Please Enter Valid Mobile Number";
            valid = false;
        }

        if (_formData.city === "" || _formData.city === 0) {
            _validate.city.error = true;
            _validate.city.message = "Required Field";
            valid = false;
        }
        if (_formData.stateName === "" || _formData.stateName === 0) {
            _validate.stateName.error = true;
            _validate.stateName.message = "Required Field";
            valid = false;
        }
        if (_formData.designation === "" || _formData.designation === 0) {
            _validate.designation.error = true;
            _validate.designation.message = "Required Field";
            valid = false;
        }
        // if (_formData.emailId === "" || _formData.emailId === 0) {
        //     _validate.emailId.error = true;
        //     _validate.emailId.message = "Required Field";
        //     valid = false;
        // }

        if (!validateEmail(_formData.emailId)) {
            _validate.emailId.error = true;
            _validate.emailId.message = "Please Enter Valid Email";
            valid = false;
        }

        _setManagervalidate(_validate);
        if (!valid) {
            _setbuttonLoading(false);
            return;
        }
        _setbuttonLoading(true);

        add()

        _setFormData({
            ..._formData,
            id: 0,
            employeeCode: "",
            firstName: "",
            mobileNumber: "",
            emailId: "",
            stateName: "",
            city: "",
            designation: "",
            status: "",
        });
    };

    const add = () => {
        const data = {
            id: 0,
            employeeCode: _formData.employeeCode,
            firstName: _formData.firstName,
            mobileNumber: _formData.mobileNumber,
            emailId: _formData.emailId,
            countryName: _formData.stateName,
            city: _formData.city,
            designation: _formData.designation,
            status: _formData.status,
            loginPassword: _formData.employeeCode,
            configUserRoleId: "3",
        };
        console.log(data);
        addManager(data).then((response: any) => {
            debugger;
            if (response.data.status === true) {
                _setCreateManager(false)
                Swal.fire({
                    title: 'Success!',
                    text: 'Manager Created Successfully',
                    icon: 'success',
                    confirmButtonText: 'OK',
                });
                gridBindDetails();
            } else {
                //  swal("Something went wrong!", { icon: "warning" });
            }
        });
        _setbuttonLoading(false);
    };


    const updateManager = () => {
        debugger;
        //  var userid = user.id;
        const _validate: any = Object.assign({}, validate);
        let valid = true;
        if (_formData.employeeCode === "" || _formData.employeeCode === 0) {
            _validate.employeeCode.error = true;
            _validate.employeeCode.message = "Required Field";
            valid = false;
        }
        if (_formData.firstName === "" || _formData.firstName === 0) {
            _validate.firstName.error = true;
            _validate.firstName.message = "Required Field";
            valid = false;
        }
        if (_formData.mobileNumber === "" || _formData.mobileNumber === 0) {
            _validate.mobileNumber.error = true;
            _validate.mobileNumber.message = "Required Field";
            valid = false;
        }
        if (!validateMobile(_formData.mobileNumber)) {
            _validate.mobileNumber.error = true;
            _validate.mobileNumber.message = "Please Enter Valid Mobile Number";
            valid = false;
        }

        if (_formData.city === "" || _formData.city === 0) {
            _validate.city.error = true;
            _validate.city.message = "Required Field";
            valid = false;
        }
        if (_formData.stateName === "" || _formData.stateName === 0) {
            _validate.stateName.error = true;
            _validate.stateName.message = "Required Field";
            valid = false;
        }
        if (_formData.designation === "" || _formData.designation === 0) {
            _validate.designation.error = true;
            _validate.designation.message = "Required Field";
            valid = false;
        }
        // if (_formData.emailId === "" || _formData.emailId === 0) {
        //     _validate.emailId.error = true;
        //     _validate.emailId.message = "Required Field";
        //     valid = false;
        // }

        if (!validateEmail(_formData.emailId)) {
            _validate.emailId.error = true;
            _validate.emailId.message = "Please Enter Valid Email";
            valid = false;
        }

        _setManagervalidate(_validate);
        if (!valid) {
            _setbuttonLoading(false);
            return;
        }
        _setbuttonLoading(true);

        update()

        _setFormData({
            ..._formData,
            id: 0,
            employeeCode: "",
            firstName: "",
            mobileNumber: "",
            emailId: "",
            stateName: "",
            city: "",
            designation: "",
            status: "",
        });
    };

    const update = () => {
        const data = {
            id: _userId,
            employeeCode: _formData.employeeCode,
            firstName: _formData.firstName,
            mobileNumber: _formData.mobileNumber,
            emailId: _formData.emailId,
            countryName: _formData.stateName,
            city: _formData.city,
            designation: _formData.designation,
            status: _formData.status,
            loginPassword: _formData.employeeCode,
            configUserRoleId: "3",
        };
        console.log(data);
        addManager(data).then((response: any) => {
            debugger;
            if (response.data.status === true) {
                _setCreateManager(false)
                Swal.fire({
                    title: 'Success!',
                    text: 'State Manager Updated Successfully',
                    icon: 'success', // Specify the icon type here
                    confirmButtonText: 'OK',
                });
                gridBindDetails();
            } else {
                //  swal("Something went wrong!", { icon: "warning" });
            }
        });
        _setbuttonLoading(false);
    };


    const handleCheckbox = (e: any) => {
        const { value, checked } = e.target;
        console.log("data", value);
        debugger;
        if (checked) {
            setisChecked([...isChecked, value]);
        }
        else {
            setisChecked(isChecked.filter((e) => e !== value));
        }
    }
    const alldelete = () => {
        debugger
        const data = {
            managerId: _managerId,
            assighId: isChecked.map(value => ({ id: value }))
        };
        RemoveManager(data).then(response => {
            if (response.data.data.isSuccess === true) {
                Swal.fire({
                    title: 'Success!',
                    text: 'Dealer Removed Successfully',
                    icon: 'success',
                    confirmButtonText: 'OK',
                });
                console.log("success");

            }
            else {
                console.log("bad request");
            }
        })

        setDialog(false);

    };

    const handleCheckbox2 = (e: any, index: any) => {
        const { value, checked } = e.target;
        console.log("data", value);
        const tempData: any = _assignList;
        tempData[index].asignChecked = checked;
        _setAssignList(tempData);
        debugger;
        if (checked) {
            setAsignChecked([...asignChecked, value]);
        }
        else {
            setAsignChecked(asignChecked.filter((e) => e !== value));
        }
    }

    const Insertassign = () => {
        debugger;
        const data = {
            managerId: _managerId,
            assighId: asignChecked.map(value => ({ id: value }))
        };
        insertAssingmanager(data).then(response => {
            debugger;
            if (response.data.status === true) {
                Swal.fire({
                    title: 'Success!',
                    text: 'Dealer Assigned Successfully',
                    icon: 'success',
                    confirmButtonText: 'OK',
                });
                console.log("success");
            }
            else {
                console.log("bad request");

            }
        })
        setAssign(false)
    }


    const gridBindDetails = () => {
        var ManagerId=0;var UserId=0;
        var StateManagerId=0;
        if(user.accessToken.data.userRoleId==3)
        {UserId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==2)
        {StateManagerId=user.accessToken.data.userid;}

        getAllStatemanager(UserId,StateManagerId)
            .then(response => {
                debugger;
                _setList(response?.data?.data?.data);
                
                _setTotalCount([...response?.data?.data?.data]?.length);
                console.log(_list);
            })
            .catch((err) => console.log(err)).finally(() => _setLoading(false));
    }

    const assignDetails = (val: any) => {
        debugger;
        //getStateManagerDetails('', '', '', '', 4)
        getAllAssignedUsers(4, val)
            .then(response => {
                debugger;
                var tempData = response?.data?.data?.data;
                var result = [];
                getusersByUserTeams(val)
                    .then(res1 => {
                        debugger;
                        if (res1?.data?.data?.isSuccess === true) {
                            const userIds: any = res1?.data?.data?.data.map((item: any) => item.id) || [];
                            var ManagersSelList = tempData?.length > 0 ? [
                                ...tempData?.map((pItem: any) => {
                                    if (userIds?.includes(pItem?.id)) {
                                        return {
                                            mobileNumber: pItem?.mobileNumber,
                                            status: pItem?.status,
                                            stateName: pItem?.stateName,
                                            countryName: pItem?.countryName,
                                            createDate: pItem?.createDate,
                                            updateDate: pItem?.updateDate,
                                            address: pItem?.address,
                                            city: pItem?.city,
                                            countryCode: pItem?.countryCode,
                                            loginUserName: pItem?.loginUserName,
                                            emailId: pItem?.emailId,
                                            firstName: pItem?.firstName,
                                            lastName: pItem?.lastName,
                                            postalCode: pItem?.postalCode,
                                            stateCode: pItem?.stateCode,
                                            id: pItem?.id,
                                            dealerCode: pItem?.dealerCode,
                                            employeeCode: pItem?.employeeCode,
                                            userRole: pItem?.userRole,
                                            designation: pItem?.designation,
                                            asignChecked: true
                                        }
                                    }
                                    else {
                                        return {
                                            mobileNumber: pItem?.mobileNumber,
                                            status: pItem?.status,
                                            stateName: pItem?.stateName,
                                            countryName: pItem?.countryName,
                                            createDate: pItem?.createDate,
                                            updateDate: pItem?.updateDate,
                                            address: pItem?.address,
                                            city: pItem?.city,
                                            countryCode: pItem?.countryCode,
                                            loginUserName: pItem?.loginUserName,
                                            emailId: pItem?.emailId,
                                            firstName: pItem?.firstName,
                                            lastName: pItem?.lastName,
                                            postalCode: pItem?.postalCode,
                                            stateCode: pItem?.stateCode,
                                            id: pItem?.id,
                                            dealerCode: pItem?.dealerCode,
                                            employeeCode: pItem?.employeeCode,
                                            userRole: pItem?.userRole,
                                            designation: pItem?.designation,
                                            asignChecked: false
                                        }
                                    }

                                })
                            ] : []
                            console.log(ManagersSelList)
                            _setAssignList(ManagersSelList);
                        }
                        else {
                            //_setUserTeamsList([]);
                            _setAssignList(tempData);
                        }
                    })
                    .catch(err => {
                        _setAssignList(tempData);
                    });
                _setManagerId(val)
                // console.log("assignList")
                // console.log(_assignList);
            })
            .catch((err) => console.log(err));
        getusersByUserTeams(val)
            .then(res => {
                debugger;
                if (res?.data?.data?.isSuccess === true) {
                    const userIds = res?.data?.data?.data.map((item: any) => item.id) || [];
                    setAssignCheckedUserIds(userIds);
                    setCheckedUserIds(userIds.filter((id: any) => openremoveCheckedUserIds.includes(id)));
                }
                else {
                    _setUserTeamsList([]);
                }
            })
            .catch(err => {
            });
    }

    const searchDetails = (input: any) => {
        debugger;
        getStateManagerDetails(input, '', '', '', 3)
            .then(response => {
                //_setAssignList(response?.data?.data);
                console.log(_assignList);
            })
            .catch((err) => console.log(err));
    }

    useEffect(() => {
        gridBindDetails();
        gettitle();
    }, []);

    const exportEXCEL = () => {
        debugger;
        const header = ["Employee Id", "Name", "Region", "City", "Designation", "PhoneNo", "Email", "State Manager"];

        let body: any = _list.map((e: any) => { return [e?.employeeCode, e?.firstName, e?.countryName, e?.city, e?.designation, e?.mobileNumber, e?.emailId, e?.stateManagerName !== null && e?.stateManagerName?.firstName || ''] })

        // var curDate = moment()?.format("YYYY-MM-DD")

        var filename = 'Managers'

        downloadExcel({

            fileName: filename,

            sheet: "ManagerList",

            tablePayload: {

                header,

                body: body,

            },

        });

    }
    const storedTokenString = localStorage.getItem('_token');
    const storedToken = storedTokenString ? JSON.parse(storedTokenString) : null;
    const userId = storedToken?.token?.data?.userid || null;
    const userRoleid=storedToken?.token?.data?.userRoleId || null;
    const sortedData = _list.slice().sort((a:any, b:any) => b.id - a.id);
    useEffect(()=>{
        debugger;
        
    },[])
    const userFirstName = sortedData?.length > 0 ? sortedData
  .filter((content: any) => userId && content?.stateManagerName?.id === userId)
  .map((user: any) => ({id:user.id}))
  : [];

const userFirstNameString = JSON.stringify(userFirstName);
localStorage.setItem("userFirstName", userFirstNameString);
    
    
    // const storedUserFirstNameString = localStorage.getItem("userFirstName");
    // if (storedUserFirstNameString) {
    //   const storedUserFirstName = JSON.parse(storedUserFirstNameString);
    //   console.log(storedUserFirstName); 
    // }
    return (<>
        {_createManager === false && (
            <div>
                <div className="my-3 mt-4">
                    <div className="col-md-12 mx-auto mt-4">
                        <div className="row align-items-center">
                            <div className="col-md-1"></div>
                            <div className="col-md-10"><div className="row">
                                <div className="mt-2 col-md-3">
                                    <FormControl className="bg-light" size="small" fullWidth variant="outlined" >
                                        <InputLabel htmlFor="outlined-adornment-password">Search</InputLabel>
                                        <OutlinedInput className="bg-white"
                                            label="Search"
                                            onChange={(e) => setSearchInput(e.target.value)}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton aria-label="toggle password visibility" edge="end" type="submit">
                                                        <Search />
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by city</InputLabel>
                                        <Select label="Select Range" value={selectedCity}
                                            onChange={handleCityChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={item.city}>{item.city}</MenuItem>
                                            ))}
                                            {/* {_list && _list.length > 0 && _list
                                                .filter((item: any) => item?.city !== null)
                                                .map((item: any, index: any) => (
                                                    item.city ? (
                                                        <MenuItem value={item.cty}>
                                                            {item.city}
                                                        </MenuItem>
                                                    ) : null
                                                ))} */}
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by Manager</InputLabel>
                                        <Select label="Select Range" value={selectedManager} onChange={handleManagerChange}>
                                            {_list.map((item: any, index: number) => (
                                                <MenuItem value={`${item.firstName} ${item.lastName}`}>{item.firstName} {item.lastName}</MenuItem>
                                            ))}
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl size="small" fullWidth className="bg-white">
                                        <InputLabel>Filter by Status</InputLabel>
                                        <Select label="Select Range" value={selectedstatus} onChange={handleStatusChange} >
                                            <MenuItem value={'Active'} >
                                                Active</MenuItem>
                                            <MenuItem value={'InActive'} >
                                                InActive</MenuItem>

                                            <MenuItem value={'selectall'}>All</MenuItem>

                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-1">
                                </div>
                                <div className="mt-2 col-md-1 p-0" style={{padding:"0px"}}>
                                    <Button type="button" style={{ padding:"5px",  background:"linear-gradient(to bottom, #9FD8FD 50%,  #8CCEFF 50%)",border:"2px solid  #0098E1" }} className="btn text-dark text-capitalize"
                                        onClick={createManager}>
                                        + create
                                    </Button>
                                </div>
                                <div className="mt-1 col-md-1">
                                    <img className="p-2" style={{ height: '55px' }} src={ExportExcel}
                                        onClick={exportEXCEL}
                                    />
                                </div>
                            </div>
                            </div>


                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-md-1">
                        </div>
                        <div className="col-md-10">
                            <div className="tablebx" style={{ overflowX: "auto" }}>

                                <Paper sx={{ width: '100%', mb: 1 }} elevation={0}>
                                    <TableContainer className="rounded">
                                        <Table className="border" size="small">
                                            <TableHead className=" py-4" style={{ borderBottom: '0.1rem  solid gray', height: "4rem",backgroundColor:"#0098E1" }}>
                                                <TableRow>
                                                    <TableCell className="fs16 py-2 border-3 border-white fw-bold text-white" align={"center"}>S.No.</TableCell>
                                                    <TableCell className="fs16 py-2 border-3 border-white fw-bold text-white" align={"center"}>Employee Id</TableCell>
                                                    <TableCell className="fs16 py-2 border-3 border-white fw-bold text-white" align={"center"}>Name</TableCell>
                                                    <TableCell className="fs16 py-2 border-3 border-white fw-bold text-white" align={"center"}>City</TableCell>
                                                    <TableCell className="fs16 py-2 border-3 border-white fw-bold text-white" align={"center"}>State</TableCell>
                                                    <TableCell className="fs16 py-2 border-3 border-white fw-bold text-white" align={"center"}>Designation</TableCell>
                                                    <TableCell className="fs16 py-2 border-3 border-white fw-bold text-white" align={"center"}>Mobile</TableCell>
                                                    <TableCell className="fs16 py-2 border-3 border-white fw-bold text-white" align={"center"}>Email</TableCell>
                                                    <TableCell className="fs16 py-2 border-3 border-white fw-bold text-white" align={"center"}>State Manager</TableCell>
                                                    <TableCell className="fs16 py-2 border-3 border-white fw-bold text-white" align={"center"}>Action</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                      
                                                {sortedData?.length > 0 ? sortedData
                                                    .filter((content: any) => {
                                                        const lowerSearchInput = searchInput.toLowerCase();
                                                        const lowerSelectedCity = selectedCity.toLowerCase();
                                                        const lowerSelectedManager = selectedManager.toLowerCase();
                                                        const lowerSelectedstatus = selectedstatus.toLowerCase();
                                                        return (
                                                            // userId && 
                                                            // content?.stateManagerName?.id === userId &&
                                                            (lowerSearchInput === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchInput))) &&
                                                            (lowerSelectedCity === '' || content.city?.toLowerCase() === lowerSelectedCity || lowerSelectedCity === 'selectall') &&
                                                            (lowerSelectedstatus === '' || content.status?.toLowerCase() === lowerSelectedstatus || lowerSelectedstatus === 'selectall') &&
                                                            (lowerSelectedManager === '' || (content.firstName + ' ' + content.lastName).toLowerCase() === lowerSelectedManager || lowerSelectedManager === 'selectall')
                                                        );
                                                    })?.slice(
                                                        (_page - 1) * _rowsPerPage,
                                                        (_page - 1) * _rowsPerPage + _rowsPerPage,
                                                    )
                                                    ?.map((item: any, index: number) => (
                                                        <TableRow key={index} className={index % 2 === 0 ? 'white-row' : 'grey-row'}>
                                                            <TableCell align={"center"} >{(index + 1) + ((_page - 1) * 10)}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.employeeCode}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.firstName} {item.lastName}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.city}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.stateName}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.designation}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.mobileNumber}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.emailId}</TableCell>
                                                            <TableCell align="center" className="fs16 border-3 border-white">{item.stateManagerName?.firstName}</TableCell>
                                                            <TableCell  className="fs16 border-3 border-white">
                                                                <div className="p-1" role="button" data-bs-toggle="dropdown">
                                                                    <MoreVertRounded className="text-muted" />
                                                                </div>
                                                                <div className="dropdown-menu dropdown-menu-bottom">
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => actionClick("update", item)}>
                                                                        <span className="mt-1" >Edit</span>
                                                                        <span className="mx-2">
                                                                            <EditIcon style={{ color: "black" }} />
                                                                        </span>
                                                                    </div>
                                                                    <hr className="" />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => openassign(item.id)}>Assign&nbsp;&nbsp;<SubdirectoryArrowRightIcon sx={{ color: green[800] }}></SubdirectoryArrowRightIcon></div>
                                                                    <hr />

                                                                    {/* <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button">Edit &nbsp;&nbsp;<EditOutlinedIcon style={{ fontSize: "medium" }}></EditOutlinedIcon><hr /></div> */}
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button">Active &nbsp;&nbsp;

                                                                        <Checkbox
                                                                            className="p-0"
                                                                            checked={item?.status === "Active"}
                                                                            onChange={(event) => {
                                                                                activeChecked(item?.id, event.target.checked ? "Active" : "InActive");
                                                                            }}
                                                                        />
                                                                    </div>
                                                                    <hr />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => openremove(item?.id)}>Remove&nbsp;&nbsp;<DeleteForeverRoundedIcon sx={{ color: red[800] }}></DeleteForeverRoundedIcon></div>
                                                                </div>
                                                            </TableCell>
                                                        </TableRow>
                                                    ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">No Data Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                     
                                            {/* {userRoleid===1 && (<>  {sortedData?.length > 0 ? sortedData
                                                    .filter((content: any) => {
                                                        const lowerSearchInput = searchInput.toLowerCase();
                                                        const lowerSelectedCity = selectedCity.toLowerCase();
                                                        const lowerSelectedManager = selectedManager.toLowerCase();
                                                        const lowerSelectedstatus = selectedstatus.toLowerCase();
                                                        return (
                                                           
                                                            (lowerSearchInput === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchInput))) &&
                                                            (lowerSelectedCity === '' || content.city?.toLowerCase() === lowerSelectedCity || lowerSelectedCity === 'selectall') &&
                                                            (lowerSelectedstatus === '' || content.status?.toLowerCase() === lowerSelectedstatus || lowerSelectedstatus === 'selectall') &&
                                                            (lowerSelectedManager === '' || (content.firstName + ' ' + content.lastName).toLowerCase() === lowerSelectedManager || lowerSelectedManager === 'selectall')
                                                        );
                                                    })?.slice(
                                                        (_page - 1) * _rowsPerPage,
                                                        (_page - 1) * _rowsPerPage + _rowsPerPage,
                                                    )
                                                    ?.map((item: any, index: number) => (
                                                        <TableRow key={index} className={index % 2 === 0 ? 'white-row' : 'grey-row'}>
                                                            <TableCell align={"center"}>{(index + 1) + ((_page - 1) * 10)}</TableCell>
                                                            <TableCell align="center">{item.employeeCode}</TableCell>
                                                            <TableCell align="center">{item.firstName} {item.lastName}</TableCell>
                                                            <TableCell align="center">{item.countryName}</TableCell>
                                                            <TableCell align="center">{item.city}</TableCell>
                                                            <TableCell align="center">{item.designation}</TableCell>
                                                            <TableCell align="center">{item.mobileNumber}</TableCell>
                                                            <TableCell align="center">{item.emailId}</TableCell>
                                                            <TableCell align="center">{item.stateManagerName?.firstName}</TableCell>
                                                            <TableCell>
                                                                <div className="p-1" role="button" data-bs-toggle="dropdown">
                                                                    <MoreVertRounded className="text-muted" />
                                                                </div>
                                                                <div className="dropdown-menu dropdown-menu-bottom">
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => actionClick("update", item)}>
                                                                        <span className="mt-1" >Edit</span>
                                                                        <span className="mx-2">
                                                                            <EditIcon style={{ color: "black" }} />
                                                                        </span>
                                                                    </div>
                                                                    <hr className="" />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => openassign(item.id)}>Assign&nbsp;&nbsp;<SubdirectoryArrowRightIcon sx={{ color: green[800] }}></SubdirectoryArrowRightIcon></div>
                                                                    <hr />

                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button">Active &nbsp;&nbsp;

                                                                        <Checkbox
                                                                            className="p-0"
                                                                            checked={item?.status === "Active"}
                                                                            onChange={(event) => {
                                                                                activeChecked(item?.id, event.target.checked ? "Active" : "InActive");
                                                                            }}
                                                                        />
                                                                    </div>
                                                                    <hr />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => openremove(item?.id)}>Remove&nbsp;&nbsp;<DeleteForeverRoundedIcon sx={{ color: red[800] }}></DeleteForeverRoundedIcon></div>
                                                                </div>
                                                            </TableCell>
                                                        </TableRow>
                                                    ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">No Data Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )} </>)} */}
                                                     {/* {userRoleid===3 && (<>
                                                {sortedData?.length > 0 ? sortedData
                                                    .filter((content: any) => {
                                                        const lowerSearchInput = searchInput.toLowerCase();
                                                        const lowerSelectedCity = selectedCity.toLowerCase();
                                                        const lowerSelectedManager = selectedManager.toLowerCase();
                                                        const lowerSelectedstatus = selectedstatus.toLowerCase();
                                                        return (
                                                         
                                                            (lowerSearchInput === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchInput))) &&
                                                            (lowerSelectedCity === '' || content.city?.toLowerCase() === lowerSelectedCity || lowerSelectedCity === 'selectall') &&
                                                            (lowerSelectedstatus === '' || content.status?.toLowerCase() === lowerSelectedstatus || lowerSelectedstatus === 'selectall') &&
                                                            (lowerSelectedManager === '' || (content.firstName + ' ' + content.lastName).toLowerCase() === lowerSelectedManager || lowerSelectedManager === 'selectall')
                                                        );
                                                    })?.slice(
                                                        (_page - 1) * _rowsPerPage,
                                                        (_page - 1) * _rowsPerPage + _rowsPerPage,
                                                    )
                                                    ?.map((item: any, index: number) => (
                                                        <TableRow key={index} className={index % 2 === 0 ? 'white-row' : 'grey-row'}>
                                                            <TableCell align={"center"}>{(index + 1) + ((_page - 1) * 10)}</TableCell>
                                                            <TableCell align="center">{item.employeeCode}</TableCell>
                                                            <TableCell align="center">{item.firstName} {item.lastName}</TableCell>
                                                            <TableCell align="center">{item.countryName}</TableCell>
                                                            <TableCell align="center">{item.city}</TableCell>
                                                            <TableCell align="center">{item.designation}</TableCell>
                                                            <TableCell align="center">{item.mobileNumber}</TableCell>
                                                            <TableCell align="center">{item.emailId}</TableCell>
                                                            <TableCell align="center">{item.stateManagerName?.firstName}</TableCell>
                                                            <TableCell>
                                                                <div className="p-1" role="button" data-bs-toggle="dropdown">
                                                                    <MoreVertRounded className="text-muted" />
                                                                </div>
                                                                <div className="dropdown-menu dropdown-menu-bottom">
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => actionClick("update", item)}>
                                                                        <span className="mt-1" >Edit</span>
                                                                        <span className="mx-2">
                                                                            <EditIcon style={{ color: "black" }} />
                                                                        </span>
                                                                    </div>
                                                                    <hr className="" />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => openassign(item.id)}>Assign&nbsp;&nbsp;<SubdirectoryArrowRightIcon sx={{ color: green[800] }}></SubdirectoryArrowRightIcon></div>
                                                                    <hr />

                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button">Active &nbsp;&nbsp;

                                                                        <Checkbox
                                                                            className="p-0"
                                                                            checked={item?.status === "Active"}
                                                                            onChange={(event) => {
                                                                                activeChecked(item?.id, event.target.checked ? "Active" : "InActive");
                                                                            }}
                                                                        />
                                                                    </div>
                                                                    <hr />
                                                                    <div className="text-black rounded--50 bgBlue" style={{ textAlign: "center" }} role="button" onClick={() => openremove(item?.id)}>Remove&nbsp;&nbsp;<DeleteForeverRoundedIcon sx={{ color: red[800] }}></DeleteForeverRoundedIcon></div>
                                                                </div>
                                                            </TableCell>
                                                        </TableRow>
                                                    ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">No Data Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                            </>)} */}
                                                <SkeletonProviderTables columns={9} visible={_loading} />
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    <div className="d-flex justify-content-end align-items-center mt-3 pb-4">
                                        <Pagination count={Math.ceil(_totalCount / _rowsPerPage)} page={_page} onChange={handleChangePage}
                                            variant="outlined" size={"small"} color={"primary"} shape="rounded"
                                            renderItem={(item) => (
                                                <PaginationItem sx={{ mx: '4px' }}
                                                    {...item}
                                                />
                                            )} />
                                    </div>
                                </Paper>

                            </div>
                        </div>
                        <div className="col-md-1">
                        </div>
                    </div>


                    <div className="row">
                        <div className="col-md-6"></div>
                        <div className="col-md-6"></div>
                    </div>

                    {/* Remove Manager */}
                    <Dialog open={dialog} onClose={() => setDialog(false)} >
                        <div className='p-2' style={{ width: "36rem" }}>
                            <div className="container full-condent">
                                <div className="row py-3">
                                    <div className=" col mt-2 fw-bold">Remove Manager</div>
                                    <div className=" col mt-2 text-end" onClick={() => setDialog(false)}><CancelIcon /></div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <TextField id="outlined-basic" fullWidth size="small" label="Search Dealer" variant="outlined"
                                            onChange={(e) => {
                                                setSearchremove(e.target.value);
                                            }}
                                        />
                                    </div>
                                    <div className="col-md-4">
                                        <FormControl fullWidth size="small" className="bg-white">
                                            <InputLabel>Filter by Region</InputLabel>
                                            <Select label="Select Range" value={selectedregion} onChange={handleregionChange}>
                                                {_userteamslist.map((item: any) => (
                                                    <MenuItem value={item.countryName}>{item.countryName}</MenuItem>

                                                ))}
                                                <MenuItem value={'selectall'}>All</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                    <div className="col-md-4">
                                        <FormControl fullWidth size="small" className="bg-white">
                                            <InputLabel>Filter by Dealer</InputLabel>
                                            <Select label="Select Range" value={selectedname} onChange={handlenameChange}>
                                                {_userteamslist.map((item: any) => (
                                                    <MenuItem value={`${item.firstName} ${item.lastName}`}>{item.firstName}{item.lastName}</MenuItem>
                                                ))}
                                                <MenuItem value={'selectall'}>All</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </div><br />

                                <Paper sx={{ width: '100%', mb: 1 }} elevation={0}>
                                    <TableContainer>
                                        <Table className="border">
                                            <TableHead className="" style={{ borderBottom: '0.1rem  solid gray', backgroundColor:"#0098E1" }}>
                                                <TableRow>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}><input type="checkbox" style={{height:"18px",width:"18px"}}/></TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>IFMS Id</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Manager Name</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Region</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>City</TableCell>

                                                </TableRow>
                                            </TableHead>

                                            <TableBody>
                                                {_userteamslist?.length > 0 ? _userteamslist
                                                    .filter((content: any) => {
                                                        const lowerSearchremove = searchremove.toLowerCase();
                                                        const lowerSelectedregion = selectedregion.toLowerCase();
                                                        const lowerSelectedname = selectedname.toLowerCase();

                                                        return (

                                                            (lowerSearchremove === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchremove))) &&
                                                            (lowerSelectedregion === '' || content.countryName?.toLowerCase() === lowerSelectedregion || lowerSelectedregion === 'selectall') &&
                                                            (lowerSelectedname === '' || (content.firstName + ' ' + content.lastName).toLowerCase() === lowerSelectedname || lowerSelectedname === 'selectall')
                                                        );
                                                    })
                                                    ?.map((item: any, index: number) => (
                                                        <TableRow key={item.id}>
                                                            <TableCell align="center" className="p-0 border-3 border-white">
                                                                <Checkbox
                                                                    value={item.id} checked={item.isChecked} onChange={(e) => handleCheckbox(e)} />
                                                            </TableCell>
                                                            <TableCell align="center" className="p-0 border-3 border-white">{item.dealerCode}</TableCell>
                                                            <TableCell align="center" className="p-0 border-3 border-white">{item.firstName} {item.lastName}</TableCell>
                                                            <TableCell align="center" className="p-0 border-3 border-white">{item.countryName}</TableCell>
                                                            <TableCell align="center" className="p-0 border-3 border-white">{item.city}</TableCell>
                                                        </TableRow>
                                                    ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">Data not Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    <div className=" text-end mt-2">
                                        <button type="button" className="btn btn-danger text-end text-white" onClick={alldelete}>Remove</button>
                                    </div>
                                </Paper>
                            </div>
                        </div>
                    </Dialog>

                    {/* Assign Manager */}
                    <Dialog open={assign} onClose={() => setAssign(false)} >

                        <div className='p-2' style={{ width: "36rem" }}>
                            <div className="container full-condent">
                                <div className="row py-3">
                                    <div className=" col mt-2 fw-bold">Assign Dealer</div>
                                    <div className=" col mt-2 text-end" onClick={() => setAssign(false)}><CancelIcon /></div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <TextField id="outlined-basic" size="small"
                                            onChange={(e) => {
                                                setDealer(e.target.value);
                                            }}
                                            fullWidth label="Search Dealer" variant="outlined" />
                                    </div>
                                    <div className="col-md-4">
                                        <FormControl fullWidth size="small" className="bg-white">
                                            <InputLabel>Filter by Dealer</InputLabel>
                                            <Select label="Select Range" value={assingname} onChange={handleassignName}>
                                                {_assignList.map((item: any) => (
                                                    <MenuItem value={item.firstName}>{item.firstName}</MenuItem>
                                                ))}
                                                <MenuItem value={'selectall'}>All</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                    <div className="col-md-4">
                                        <FormControl fullWidth size="small" className="bg-white">
                                            <InputLabel>Filter by City</InputLabel>
                                            <Select label="Select Range" value={assigncity} onChange={handleassingcity}>
                                                {_assignList.map((item: any) => (
                                                    <MenuItem value={item.city}>{item.city}</MenuItem>
                                                ))}
                                                <MenuItem value={'selectall'}>All</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </div>
                                </div><br />
                                <Paper sx={{ width: '100%', mb: 1 }} elevation={0}>
                                    <TableContainer>
                                        <Table className="border">
                                            <TableHead className="b" style={{ borderBottom: '0.1rem  solid gray', backgroundColor:"#0098E1" }}>
                                                <TableRow>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}><input type="checkbox" style={{height:"18px",width:"18px"}}/></TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white " align={"center"}>Dealer Code</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Dealer Name</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Region</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>City</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {_assignList?.length > 0 ? _assignList
                                                    .filter((content: any) => {
                                                        const lowerSearchassigncity = assigncity.toLowerCase();
                                                        const lowersearchassignname = assingname.toLowerCase();
                                                        const lowersearchdealer = dealer.toLowerCase();

                                                        return (

                                                            (lowersearchdealer === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowersearchdealer))) &&
                                                            (lowersearchassignname === '' || content.firstName?.toLowerCase() === lowersearchassignname || lowersearchassignname === 'selectall') &&
                                                            (lowerSearchassigncity === '' || content.city.toLowerCase() === lowerSearchassigncity || lowerSearchassigncity === 'selectall')
                                                        );
                                                    })
                                                    ?.map((item: any, index: number) => (

                                                        <TableRow key={item.id} className={index % 2 === 0 ? 'grey-row' : 'white-row'}>
                                                            <TableCell align="center" className="p-0 border-3 border-white">
                                                                <Checkbox
                                                                    value={item.id} checked={item.asignChecked} onChange={(e) => handleCheckbox2(e, index)} />


                                                            </TableCell>
                                                            <TableCell align="center" className="p-0 border-3 border-white"> {item.dealerCode}</TableCell>
                                                            <TableCell align="center" className="p-0 border-3 border-white">{item.firstName} {item.lastName}</TableCell>
                                                            <TableCell align="center" className="p-0 border-3 border-white">{item.countryName}</TableCell>
                                                            <TableCell align="center" className="p-0 border-3 border-white">{item.city}</TableCell>


                                                        </TableRow>
                                                    ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">No Data Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </Paper>
                            </div>

                            <div className="text-end mt-2">
                                <button type="button" className="btn btn-success text-end assignButton" onClick={Insertassign}>Assign</button>
                            </div>
                        </div>
                    </Dialog>

                </div>

            </div>)
        }

        {_createManager === true && (
            <div>
                <div className="row mt-4">
                    <div className="col-md-1"></div>
                    <div className=" col-md-3 fw-bold">Create Manager</div>
                </div>

                <div className="row mt-3">
                    <div className="col-md-1"></div>
                    <div className="col-md-2">
                        <div>Employee ID</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.employeeCode}
                            onChange={(event) => {
                                changeFormData("employeeCode", event.target.value);
                            }}
                            error={_managervalidate?.employeeCode?.error}
                            helperText={_managervalidate?.employeeCode?.message}
                        />
                    </div>
                    <div className="col-md-2">
                        <div>Name</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.firstName}
                            onChange={(event) => {
                                changeFormData("firstName", event.target.value);
                            }}
                            error={_managervalidate?.firstName?.error}
                            helperText={_managervalidate?.firstName?.message}
                        />
                    </div>
                    <div className="col-md-2">
                        <div>Region</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.stateName}
                            onChange={(event) => {
                                changeFormData("stateName", event.target.value);
                            }}
                            error={_managervalidate?.stateName?.error}
                            helperText={_managervalidate?.stateName?.message}
                        />
                    </div>
                    <div className="col-md-2">
                        <div>City</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.city}
                            onChange={(event) => {
                                changeFormData("city", event.target.value);
                            }}
                            error={_managervalidate?.city?.error}
                            helperText={_managervalidate?.city?.message}
                        />
                    </div>
                </div>

                <div className="row mt-4">
                    <div className="col-md-1"></div>
                    <div className="col-md-2">
                        <div>Designation</div>
                        {/* <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.designation}
                            onChange={(event) => {
                                changeFormData("designation", event.target.value);
                            }}
                            error={_managervalidate?.designation?.error}
                            helperText={_managervalidate?.designation?.message}
                        /> */}

                        <FormControl
                            className='mt-2 w-100'
                            error={_managervalidate?.designation?.error}>
                            <Select
                                size='small'
                                value={_formData.designation}
                                onChange={(event) => changeFormData("designation", event.target.value)}>
                                {_titleresponse?.map((item: any, index: any) => (
                                    <MenuItem value={item.designation}>{item.designation}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                        <FormHelperText>{_managervalidate?.designation?.message}</FormHelperText>
                    </div>
                    <div className="col-md-2">
                        <div>Phone Number</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.mobileNumber}
                            onChange={(event: any) => {
                                !isNaN(event.target.value)
                                    ? changeFormData("mobileNumber", event.target.value)
                                    : changeFormData("mobileNumber", "")
                            }}
                            error={_managervalidate?.mobileNumber?.error}
                            helperText={_managervalidate?.mobileNumber?.message}
                        />
                    </div>
                    <div className="col-md-2">
                        <div>Email</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.emailId}
                            onChange={(event) => {
                                changeFormData("emailId", event.target.value);
                            }}
                            error={_managervalidate?.emailId?.error}
                            helperText={_managervalidate?.emailId?.message}
                        />
                    </div>

                    <div className="col-md-2"> </div>

                </div>
                <div className="row mt-4 px-5">
                    <div className="col-md-1"></div>
                    <div className="col-md-8 d-flex justify-content-end">
                        <div className="row d-flex">
                            <div className="col-md-5 text-end my-auto">
                                <span>
                                    {/* <Checkbox checked={_formData.status}
                                        onChange={(event) => {
                                            changeFormData("status", event.target.value);
                                        }}
                                    /> */}

                                    <Checkbox
                                        checked={_formData.status === "Active"}
                                        onChange={(event) => {
                                            changeFormData("status", event.target.checked ? "Active" : "InActive");
                                        }}
                                    />
                                </span>
                                <span className="mx-1">Active</span></div>
                            <div className="col-md-4">
                                <Button className="border rounded text-muted" onClick={cancelManager}>Cancel</Button>
                            </div>
                            <div className="col-md-3">
                                <Button className="border rounded text-white px-2" style={{ backgroundColor: "#0CBB0C", width: "5rem" }}
                                    onClick={_type === "create" ? InserManager : updateManager}
                                >
                                    {_type === "create" ? "Submit" : "Update"}
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-1"></div>
                </div>
            </div>)

        }
    </>);
}

export default Manager;


