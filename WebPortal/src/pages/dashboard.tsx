import dayjs from "dayjs";
import moment from "moment";
import { useEffect, useState } from "react";
import DatePickerControl from "../components/helpers/DatePickerControl";
import { Button, FormControl, InputLabel, MenuItem, Select, TextField, IconButton } from "@mui/material";
import { IMAGES_ICON } from "../assets/images/exportImages";
import { dashboardDetails, getScrollAnnouncement } from "../models/model";
import { useStateValue } from '../providers/StateProvider';
import "./dashboard.scss";
import {
    Search as IconSearch,
    Replay as IconReplay
} from "@mui/icons-material";

export default function Dashboard() {
    const [{ user }, dispatch]: any = useStateValue();
    const [_documentsCount, _setDocumentsCount] = useState<any>()
    const [_list, _setList] = useState<any>([]);
    const [_statemanagerCount, _setStateManagerCount] = useState<any>()
    const [_managerCount, _setManagerCount] = useState<any>()
    const [_dealerCount, _setDealerCount] = useState<any>()
    const [_announcementCount, _setAnnouncementCount] = useState<any>()
    const [_accountStatementCount, _setAccountStatementCount] = useState<any>()
    const [_salesCount, _setSalesCount] = useState<any>()
    const [_updatedFormData, _setUpdatedFormData] = useState<any>()
    const [_scrollmessage, _setScrollMessage] = useState<any>("");
    const [_scrollMessageList, _setScrollMessageList] = useState<any>([]);

    const clearSearch = (e: any) => {
        e.preventDefault();

        _setFormData({
            ..._formData,
            startDate: '',
            endDate: '',
            day: '',
        });
        var ManagerId=0;var DealerId=0;
        var StateManagerId=0;
        if(user.accessToken.data.userRoleId==4)
        {DealerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==3)
        {ManagerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==2)
        {StateManagerId=user.accessToken.data.userid;}
        dashboardDetails('', '', '',DealerId,ManagerId,StateManagerId)
            .then(response => {
                debugger;
                _setList(response?.data?.data?.data);
                _setStateManagerCount(response?.data?.data?.data?.stateManagersCount)
                _setManagerCount(response?.data?.data?.data?.managersCount)
                _setDealerCount(response?.data?.data?.data?.dealersCount)
                _setAnnouncementCount(response?.data?.data?.data?.announcement)
                _setAccountStatementCount(response?.data?.data?.data?.accountStatement)
                _setSalesCount(response?.data?.data?.data?.salesCount)
                _setDocumentsCount(response?.data?.data?.data?.documentsCount)
                //documentsCount
                console.log(response);
            })
            .catch((err: any) => console.log(err));
    }

    const handleSelectChange = (value: any) => {
        debugger;
        const selectedValue = value;
        let newDate = dayjs();

        switch (selectedValue) {
            case '5days':
                newDate = newDate.subtract(5, 'day');
                break;
            case '15days':
                newDate = newDate.subtract(15, 'day');
                break;
            case '30days':
                newDate = newDate.subtract(30, 'day');
                break;
            case '3months':
                newDate = newDate.subtract(3, 'month');
                break;
            default:
                break;
        }
        _setUpdatedFormData(newDate.format('YYYY/MM/DD'));
        _setFormData({ day: value });
        dashboardValuesRangeSelect(newDate.format('YYYY/MM/DD'))
    };

    const dashboardValuesRangeSelect = (val: any) => {
        debugger;
        var ManagerId=0;var DealerId=0;
        var StateManagerId=0;
        if(user.accessToken.data.userRoleId==4)
        {DealerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==3)
        {ManagerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==2)
        {StateManagerId=user.accessToken.data.userid;}
        dashboardDetails(val || '', _formData.startDate || '', _formData.endDate || '',DealerId,ManagerId,StateManagerId)
            .then(response => {
                debugger;
                _setList(response?.data?.data?.data);
                _setStateManagerCount(response?.data?.data?.data?.stateManagersCount)
                _setManagerCount(response?.data?.data?.data?.managersCount)
                _setDealerCount(response?.data?.data?.data?.dealersCount)
                _setAnnouncementCount(response?.data?.data?.data?.announcement)
                _setAccountStatementCount(response?.data?.data?.data?.accountStatement)
                _setSalesCount(response?.data?.data?.data?.salesCount)
                _setDocumentsCount(response?.data?.data?.data?.documentsCount)
                console.log(response);
            })
            .catch((err: any) => console.log(err));
    }



    const [_formData, _setFormData] = useState<any>({
        startDate: '',
        endDate: '',
        day: '',
    });

    const changeFormData = (name: string, value: any) => {
        _setFormData({ ..._formData, [name]: value });
    };


    const changeDateFormat = (date: any) => {
        return moment(date).format("YYYY/MM/DD");
    };

    


    const dashboardValues = () => {
        debugger;
        var ManagerId=0;var DealerId=0;
        var StateManagerId=0;
        if(user.accessToken.data.userRoleId==4)
        {DealerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==3)
        {ManagerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==2)
        {StateManagerId=user.accessToken.data.userid;}

        dashboardDetails(_updatedFormData || '', _formData.startDate || '', _formData.endDate || '',DealerId,ManagerId,StateManagerId)
            .then(response => {
                debugger;
                _setList(response?.data?.data?.data);
                _setStateManagerCount(response?.data?.data?.data?.stateManagersCount)
                _setManagerCount(response?.data?.data?.data?.managersCount)
                _setDealerCount(response?.data?.data?.data?.dealersCount)
                _setAnnouncementCount(response?.data?.data?.data?.announcement)
                _setAccountStatementCount(response?.data?.data?.data?.accountStatement)
                _setSalesCount(response?.data?.data?.data?.salesCount)
                _setDocumentsCount(response?.data?.data?.data?.documentsCount)
                console.log(response);
            })
            .catch((err: any) => console.log(err));
    }

    const storedTokenString = localStorage.getItem('_token');

    const storedToken = storedTokenString ? JSON.parse(storedTokenString) : null;

    const userId = storedToken?.token?.data?.userid || null;
    const userRoleId = storedToken?.token?.data?.userRoleId || null;


    const scrollerMessage = () => {
        var scrollMessage="";
        getScrollAnnouncement(userRoleId, userId).then(response => {
            //_setScrollMessage(response?.data?.data?.data?.announcement1)
            _setScrollMessageList(response?.data?.data?.data)
        })
    }

    useEffect(() => {
        dashboardValues();
        scrollerMessage();
    }, []);

    return (<>
        <div className="bgLightMain vh-100">
            <div className="">
                <div className="row"></div>

                <div className="d-flex justify-content-center mt-4">
                    <div className="scroller-container">
                        <div className="scrolling-message">
                        {_scrollMessageList?.length > 0 ? (_scrollMessageList.map((item: any, index: number) => (
                            <>{item?.announcement1} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</>
                        )))
                        :(<></>)}
                            <span></span>
                            {/* {_scrollmessage} */}
                        </div>
                    </div>
                </div>
                <div className="col-md-6 mx-auto" style={{ marginTop: "50px" }}>
                    <div className="row align-items-center">
                        <div className="mt-2 col-md-3">
                            <FormControl fullWidth className="bg-white rounded" size="small">
                                <InputLabel>Select Range</InputLabel>
                                <Select label="Select Range" value={_formData?.day}
                                    onChange={(e) => { handleSelectChange(e.target.value); }}>
                                    <MenuItem value={'5days'}>Last 5 days</MenuItem>
                                    <MenuItem value={'15days'}>Last 15 days</MenuItem>
                                    <MenuItem value={'30days'}>Last 30 days</MenuItem>
                                    <MenuItem value={'3months'}>Last 3 months</MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                        <div className="mt-2 col-md-3" style={{ color: "#00AAE7" }}>
                            {/* <DatePickerControl label={""} onchange={ (date: Date | null) => changeFormData('startDate', date)} value={_formData?.startDate} /> */}

                            <TextField
                                fullWidth
                                className='mt-1 bg-primary'
                                size='small'
                                type='date'
                                value={_formData?.startDate}
                                onChange={(event) =>
                                    changeFormData("startDate", event.target.value)
                                }
                                InputLabelProps={{ shrink: true }}
                                
                            />
                        </div>
                        <div className="mt-2 col-md-3">
                            {/* <DatePickerControl label={""} onchange={(date: Date | null) => changeFormData('endDate', date)} value={_formData?.endDate} minDate={_formData?.startDate} /> */}

                            <TextField
                                fullWidth
                                className='mt-1 rounded'
                                size='small'
                                type='date'
                                value={_formData?.endDate}
                                onChange={(event) =>
                                    changeFormData("endDate", event.target.value)
                                }
                                InputLabelProps={{ shrink: true }}
                            />
                        </div>
                        <div className="mt-2 col-md-2">
                            <Button className="bg-primary text-dark w-75 textTransformNone " style={{background:"linear-gradient(to bottom, #9FD8FD 50%,  #8CCEFF 50%)",border:"2px solid  #0098E1"}} onClick={dashboardValues}>Search</Button>

                        </div>
                        <div className="mt-2 col-md-1">
                            <IconButton type="button" sx={{ p: '10px' }} aria-label="reset" onClick={clearSearch}>
                                <IconReplay />
                            </IconButton>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="col-md-9 mx-auto row my-4">
                        {user?.accessToken?.data?.userRoleId == 1 && (
                            <div className="col-md-4 p-3">
                                <div className="bg-white text-center p-3 rounded">
                                    <div className="my-2">
                                        <img src={IMAGES_ICON.StateManagers} alt="Icon" height={32} />
                                    </div>
                                    <div className="fs-2 fw-bold mt-3">{_statemanagerCount ? _statemanagerCount : '0'}</div>
                                    <div className="fs14 text-muted">State Managers</div>
                                </div>
                            </div>)}
                        {(user?.accessToken?.data?.userRoleId == 1 || (user?.accessToken?.data?.userRoleId === 2)) && (
                            <div className="col-md-4 p-3">
                                <div className="bg-white text-center p-3 rounded">
                                    <div className="my-2">
                                        <img src={IMAGES_ICON.StateManagers} alt="Icon" height={32} />
                                    </div>
                                    <div className="fs-2 fw-bold mt-3">{_managerCount ? _managerCount : '0'}</div>
                                    <div className="fs14 text-muted">Managers</div>
                                </div>
                            </div>
                        )}
                        {user?.accessToken?.data?.userRoleId == 1 || user?.accessToken?.data?.userRoleId == 2 || user?.accessToken?.data?.userRoleId == 3 && (
                            <div className="col-md-4 p-3">
                                <div className="bg-white text-center p-3 rounded">
                                    <div className="my-2">
                                        <img src={IMAGES_ICON.Dealers} alt="Icon" height={32} />
                                    </div>
                                    <div className="fs-2 fw-bold mt-3">{_dealerCount ? _dealerCount : '0'}</div>
                                    <div className="fs14 text-muted">Dealers</div>
                                </div>
                            </div>)}
                        {user?.accessToken?.data?.userRoleId == 1 && (
                            <div className="col-md-4 p-3">
                                <div className="bg-white text-center p-3 rounded">
                                    <div className="my-2">
                                        <img src={IMAGES_ICON.Announcement} alt="Icon" height={32} />
                                    </div>
                                    <div className="fs-2 fw-bold mt-3">{_announcementCount ? _announcementCount : '0'}</div>
                                    <div className="fs14 text-muted">Announcement</div>
                                </div>
                            </div>)}
                        {((user?.accessToken?.data?.userRoleId === 1) || (user?.accessToken?.data?.userRoleId === 4) || (user?.accessToken?.data?.userRoleId === 3) || (user?.accessToken?.data?.userRoleId === 2)) && (
                            <div className="col-md-4 p-3">
                                <div className="bg-white text-center p-3 rounded">
                                    <div className="my-2">
                                        <img src={IMAGES_ICON.AccountStatement} alt="Icon" height={32} />
                                    </div>
                                    <div className="fs-2 fw-bold mt-3">{_accountStatementCount ? _accountStatementCount : '0'}</div>
                                    <div className="fs14 text-muted">Account Statement</div>
                                </div>
                            </div>
                        )}
                        {((user?.accessToken?.data?.userRoleId == 1) || (user?.accessToken?.data?.userRoleId == 3) || (user?.accessToken?.data?.userRoleId == 2) || (user?.accessToken?.data?.userRoleId == 4)) && (
                            <div className="col-md-4 p-3">
                                <div className="bg-white text-center p-3 rounded">
                                    <div className="my-2">
                                        <img src={IMAGES_ICON.DocumentAccess} alt="Icon" height={32} />
                                    </div>
                                    <div className="fs-2 fw-bold mt-3">{_documentsCount?_documentsCount:'0'}</div>
                                    <div className="fs14 text-muted">Document Access</div>
                                </div>
                            </div>)}
                        {((user?.accessToken?.data?.userRoleId == 1) || (user?.accessToken?.data?.userRoleId == 4) || (user?.accessToken?.data?.userRoleId == 3) || (user?.accessToken?.data?.userRoleId == 2)) && (
                            <div className="col-md-4 p-3">
                                <div className="bg-white text-center p-3 rounded">
                                    <div className="my-2">
                                        <img src={IMAGES_ICON.Sales} alt="Icon" height={32} />
                                    </div>
                                    <div className="fs-2 fw-bold mt-3">{_salesCount ? _salesCount : '0'}</div>

                                    <div className="fs14 text-muted">Sales</div>
                                </div>
                            </div>)}
                    </div>
                </div>
            </div>
        </div>
    </>
    )
}