import { FormControl, InputLabel, MenuItem, Select, TextField, Dialog, Paper, Pagination, PaginationItem, OutlinedInput, InputAdornment, 
    IconButton, Button,Checkbox } from "@mui/material";
import {MenuRounded,Replay as IconReplay} from '@mui/icons-material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Search } from "@mui/icons-material";
import '../../assets/scss/page.css';
import { getAllsales,GetAllSalesDetailsByMaterialGroup } from "../../models/model";
import { useEffect, useState } from "react";
import { SkeletonProviderTables } from "../../providers/SkeletonProvider";
import moment from "moment";
import ExportExcel from'../../assets/images/Export Excel.png';
import { downloadExcel } from "react-export-table-to-excel";
import { useStateValue } from '../../providers/StateProvider';
import dayjs from "dayjs";

function Sales() {

    const [_loading, _setLoading] = useState<any>(true);
    const [_list, _setList] = useState<any>([]);
    const [_materialList, _setMaterialList] = useState<any>([]);
    const [_userteamslist, _setUserTeamsList] = useState<any>([]);
    const [searchInput, setSearchInput] = useState("");
    const [_assignList, _setAssignList] = useState<any>([])
    const [_editList, _setEditList] = useState<any>([])
    const [_popup, _setPopup] = useState<any>(false);
    const [_checkvalue, _setCheckvalue] = useState<any>({});
    const [_formCheckvalue, _setFormCheckvalue] = useState<any>({});
    const [selectedCity, setSelectedCity] = useState('');
    const [selectedregion, setSelectedregion] = useState('');
    const [selectedManager, setSelectedManager] = useState('');
    const [selectedinvoice, setSelectedinvoice] = useState('');
    const [_createManager, _setCreateManager] = useState<any>(false);
    const [_page, _setPage] = useState(1);
    const [_rowsPerPage] = useState(10);
    const [_totalCount, _setTotalCount] = useState(0);
    const [{ user }, dispatch]: any = useStateValue();
    const [_formFilterCheckvalue, _setFormFilterCheckvalue] = useState<any>(
        {DealerCode:true,DealerName:true,ProductCode:true,ProductDesc:true,BatchNo:true,OBDNo:true
        ,ActualGiQuantity:true,ActualGiDate:true,ZGENo:true,SupplyingPlantCode:true,SupplyingPlantName:true,
        SupplyingPlantDistrict:true,SupplyingPlantState:true,TransporterName:true,LrNo:true,LrDate:true,
        TruckNo:true,TruckCapacity:true,InvoiceNumber:true,InvoiceDate:true,SoldToCode:true,SoldToName:true,
        SoldToBlock:true,SoldToDistrict:true,ShipToName:true,ShipToDistrict:true,InvoiceValue:true,
        InvoiceValueWithoutGST:true,InvoiceVlueWithGST:true,GST:true,MaterialGroup:true,SoldToLocation:true,
        ShipToCode:true,ShipToLocation:true,ShipToBlock:true});
    const [_formData, _setFormData] = useState<any>({
            startDate: '',
            endDate: '',
            day: '',
        });
        const [_updatedFormData, _setUpdatedFormData] = useState<any>()

    const handleChangePage = (event: React.ChangeEvent<unknown>, value: number) => {
        _setPage(value);
    };

    const changeDateFormat = (date: any) => {
        return moment(date).format("DD/MM/YYYY");
    }

    const handleCityChange = (event: any) => {
        setSelectedCity(event.target.value); 
    };
    const handleManagerChange = (event: any) => {
        setSelectedManager(event.target.value);
    };

    const handleRegionChange = (event: any) => {
        setSelectedregion(event.target.value); 
    };

    const handleinvoicechange=(event:any)=>{
        setSelectedinvoice(event.target.value);
    };
    const gridBindDetails = (DayRange:any,StartDate:any,EndDate:any,MaterialGroup:any) => {
        debugger;
        var ManagerId=0;var DealerId=0;
        var StateManagerId=0;
        if(user.accessToken.data.userRoleId==4)
        {DealerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==3)
        {ManagerId=user.accessToken.data.userid;}
        else if(user.accessToken.data.userRoleId==2)
        {StateManagerId=user.accessToken.data.userid;}
        getAllsales(DealerId,ManagerId,StateManagerId,DayRange,StartDate,EndDate,MaterialGroup)
            .then(response => {
                debugger;
                _setList(response?.data?.data?.data);
                 _setTotalCount([...response?.data?.data?.data]?.length);
                console.log("Allcount",_list);
            })
            .catch((err) => console.log(err)).finally(() => _setLoading(false));
    }

 
    useEffect(() => {
        gridBindDetails('', '', '','');
        GetAllSalesDetailsByMaterialGroup().then(response=>{
            _setMaterialList(response?.data?.data?.data)
        })
        .catch((err) => console.log(err));

    }, []);

    const exportEXCEL = () => {
        debugger;
               const header = ["IFMS Id", "Dealer Name", "Product Code", "Product Desc","Batch No","OBD No","Actual GI Quantity","Actual GI Date","ZGE No","Supplying Plant Code","Supplying Plant Name","Supplying Plant District","Supplying Plant State","Transporter Name",
            "Lr No","Lr Date","Truck No","Truck Capacity","Invoice Number","Invoice Date","Sold-to Code","Sold-to Name","Sold-to Location","Sold-to Block","Sold-to District","Ship-to Code","Ship-to Name","Ship-to Location","Ship-to Block","Ship-to District","Invoice Value"
            // ,"GST"
            
            ];
            
               let body: any = _list.map((e: any) => { return [e?.code, e?.name, e?.productCode, e?.productDesc,e?.batchNo,e?.obdNo,e?.actualGiQuantity,
            
                ,e?.actualGiDate,e?.ztrsNo,e?.supplyingPlantCode,e?.supplyingPlantName,e?.supplyingPlantDistrict,e?.supplyingPlantState,e?.tptName
                ,e?.lrNo,e?.lrDate,e?.truckNo,e?.truckCapacity,e?.invoiceNumber,e?.invoiceDate,e?.soldToCode,e?.soldToName,e?.soldToLocation,e?.soldToBlock,e?.soldToDistrict,e?.shipToCode,e?.shipToName,
                e?.shipToLocation,e?.shipToBlock,e?.shipToDistrict,e?.invoiceValue
                // ,e?.gst
            
            ] })
            
               // var curDate = moment()?.format("YYYY-MM-DD")
            
               var filename = 'Sales'
            
               downloadExcel({
            
                 fileName: filename,
            
                 sheet: "Sales",
            
                 tablePayload: {
            
                   header,
            
                   body: body,
            
                 },
            
               });
            
             }
       
             const handleFilterChange=(filterBy:any,value:any)=>{
                _setFormFilterCheckvalue({ ..._formFilterCheckvalue, [filterBy]:!value  });
             }
             const handleSelectChange=(value:any)=>{
                const selectedValue = value;
                let newDate = dayjs();
        
                switch (selectedValue) {
                    case '5days':
                        newDate = newDate.subtract(5, 'day');
                        break;
                    case '15days':
                        newDate = newDate.subtract(15, 'day');
                        break;
                    case '30days':
                        newDate = newDate.subtract(30, 'day');
                        break;
                    case '3months':
                        newDate = newDate.subtract(3, 'month');
                        break;
                    default:
                        break;
                }
                _setFormData({ day: value });
                _setUpdatedFormData(newDate.format('YYYY/MM/DD'));
                gridBindDetails(newDate.format('YYYY/MM/DD'),'','',_formData.MaterialGroup || '')
             }


             const changeFormData = (name: string, value: any) => {
                _setFormData({ ..._formData, [name]: value });
                if(name=="MaterialGroup"){
                    gridBindDetails('','','',value)
                }
            };
            const clearSearch = (e: any) => {
                e.preventDefault();
        
                _setFormData({
                    ..._formData,
                    startDate: '',
                    endDate: '',
                    day: '',
                });
                _setUpdatedFormData('')
               
            }

            const searchButtonClick=()=>{
                gridBindDetails('',_formData.startDate || '',_formData.endDate || '',_formData.MaterialGroup || '')
            }
            //  const sortedData = _list.slice().sort((a: any, b: any) => b.id - a.id);
    return (<>
        {_createManager === false && (
            <div>
                <div className="my-3 mt-4">
                    <div className="col-md-12 mx-auto mt-4">
                        <div className="row align-items-center">
                            <div className="col-md-1"></div>
                            <div className="col-md-10">
                                <div className="row">
                                {/* <div className="mt-2 col-md-3">
                                    <FormControl className="bg-light" size="small" fullWidth variant="outlined" >
                                        <InputLabel htmlFor="outlined-adornment-password">Search</InputLabel>
                                        <OutlinedInput className="bg-white"
                                            label="Search"
                                            onChange={(e) => setSearchInput(e.target.value)}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton aria-label="toggle password visibility" edge="end" type="submit">
                                                        <Search />
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by city</InputLabel>
                                        <Select label="Select Range" value={selectedCity}
                                            onChange={handleCityChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={item.city}>{item.city}</MenuItem>
                                            ))}
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div> */}
                                {/* <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by Manager</InputLabel>
                                        <Select label="Select Range" value={selectedManager}
                                            onChange={handleManagerChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={item.manager}>{item.manager}</MenuItem>
                                            ))}
                                             <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by InvoiceNo</InputLabel>
                                        <Select label="Select Range" value={selectedinvoice} onChange={handleinvoicechange}>
                                            {_list.map((item: any, index: number) => (
                                                <MenuItem value={item?.invoiceNumber}>{item?.invoiceNumber}</MenuItem>
                                            ))}
                                             <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-1">
                                <Button type="button" style={{  background:"linear-gradient(to bottom, #9FD8FD 50%,  #8CCEFF 50%)",border:"2px solid  #0098E1" }} className="btn text-dark text-capitalize">
                                    Search
                                </Button>
                            </div>  */}
                            <div className="mt-2 col-md-2">
                                <FormControl fullWidth className="bg-white rounded" size="small">
                                    <InputLabel>Select Range</InputLabel>
                                    <Select label="Select Range" value={_formData?.day}
                                        onChange={(e) => { handleSelectChange(e.target.value); }}>
                                        <MenuItem value={'5days'}>Last 5 days</MenuItem>
                                        <MenuItem value={'15days'}>Last 15 days</MenuItem>
                                        <MenuItem value={'30days'}>Last 30 days</MenuItem>
                                        <MenuItem value={'3months'}>Last 3 months</MenuItem>
                                    </Select>
                                </FormControl>
                            </div>
                            <div className="mt-2 col-md-2">
                                <FormControl fullWidth className="bg-white rounded" size="small">
                                    <InputLabel>Select MaterialGroup</InputLabel>
                                    <Select label="Select Range" value={_formData?.MaterialGroup}
                                        onChange={(e) => { changeFormData("MaterialGroup",e.target.value); }}>
                                        {_materialList.map((item: any, index: number) => (
                                        <MenuItem value={item?.materialGroup}>{item?.materialGroup}</MenuItem>    ))}                                    
                                    </Select>
                                </FormControl>
                            </div>
                            <div className="mt-2 col-md-2">
                                <TextField
                                    fullWidth
                                    className='mt-1 bg-primary'
                                    size='small'
                                    type='date'
                                    value={_formData?.startDate}
                                    onChange={(event) =>
                                        changeFormData("startDate", event.target.value)
                                    }
                                    InputLabelProps={{ shrink: true }}
                                />
                            </div>
                            <div className="mt-2 col-md-2">
                                <TextField
                                    fullWidth
                                    className='mt-1 rounded'
                                    size='small'
                                    type='date'
                                    value={_formData?.endDate}
                                    onChange={(event) =>
                                        changeFormData("endDate", event.target.value)
                                    }
                                    InputLabelProps={{ shrink: true }}
                                />
                            </div>
                            <div className="mt-2 col-md-1">
                                <Button className="bg-primary text-dark w-75 textTransformNone " style={{background:"linear-gradient(to bottom, #9FD8FD 50%,  #8CCEFF 50%)",border:"2px solid  #0098E1"}} onClick={searchButtonClick}>Search</Button>
                            </div>
                            <div className="mt-2 col-md-1">
                                <IconButton type="button" sx={{ p: '10px' }} aria-label="reset" onClick={clearSearch}>
                                    <IconReplay />
                                </IconButton>
                            </div>
                            <div className="mt-2 col-md-1">
                            <MenuRounded className='bg-field-light' role="button" data-bs-toggle="dropdown" />
                            <div className="dropdown-menu dropdown-menu-right"  style={{maxHeight:"65vh",overflow:"scroll"}}>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.DealerCode} onClick={() => handleFilterChange('DealerCode',_formFilterCheckvalue?.DealerCode)} /> Dealer Code</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.DealerName} onClick={() => handleFilterChange('DealerName',_formFilterCheckvalue?.DealerName)} /> Dealer Name</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.ProductCode} onClick={() => handleFilterChange('ProductCode',_formFilterCheckvalue?.ProductCode)} /> Product Code</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.ProductDesc} onClick={() => handleFilterChange('ProductDesc',_formFilterCheckvalue?.ProductDesc)} /> Product Desc</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.BatchNo} onClick={() => handleFilterChange('BatchNo',_formFilterCheckvalue?.BatchNo)} /> Batch No</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.OBDNo} onClick={() => handleFilterChange('OBDNo',_formFilterCheckvalue?.OBDNo)} /> OBD No</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.ActualGiQuantity} onClick={() => handleFilterChange('ActualGiQuantity',_formFilterCheckvalue?.ActualGiQuantity)} /> Actual GI Quantity</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.ActualGiDate} onClick={() => handleFilterChange('ActualGiDate',_formFilterCheckvalue?.ActualGiDate)} /> Actual GI Date</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.ZGENo} onClick={() => handleFilterChange('ZGENo',_formFilterCheckvalue?.ZGENo)} /> ZGE No</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.SupplyingPlantCode} onClick={() => handleFilterChange('SupplyingPlantCode',_formFilterCheckvalue?.SupplyingPlantCode)} /> Supplying Plant Code</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.SupplyingPlantName} onClick={() => handleFilterChange('SupplyingPlantName',_formFilterCheckvalue?.SupplyingPlantName)} /> Supplying Plant Name</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.SupplyingPlantDistrict} onClick={() => handleFilterChange('SupplyingPlantDistrict',_formFilterCheckvalue?.SupplyingPlantDistrict)} /> Supplying Plant District</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.SupplyingPlantState} onClick={() => handleFilterChange('SupplyingPlantState',_formFilterCheckvalue?.SupplyingPlantState)} /> Supplying Plant State</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.TransporterName} onClick={() => handleFilterChange('TransporterName',_formFilterCheckvalue?.TransporterName)} /> Transporter Name</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.LrNo} onClick={() => handleFilterChange('LrNo',_formFilterCheckvalue?.LrNo)} /> Lr No</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.LrDate} onClick={() => handleFilterChange('LrDate',_formFilterCheckvalue?.LrDate)} /> Lr Date</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.TruckNo} onClick={() => handleFilterChange('TruckNo',_formFilterCheckvalue?.TruckNo)} /> Truck No</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.TruckCapacity} onClick={() => handleFilterChange('TruckCapacity',_formFilterCheckvalue?.TruckCapacity)} /> Truck Capacity</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.InvoiceNumber} onClick={() => handleFilterChange('InvoiceNumber',_formFilterCheckvalue?.InvoiceNumber)} /> Invoice Number</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.InvoiceDate} onClick={() => handleFilterChange('InvoiceDate',_formFilterCheckvalue?.InvoiceDate)} /> Invoice Date</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.SoldToCode} onClick={() => handleFilterChange('SoldToCode',_formFilterCheckvalue?.SoldToCode)} /> Sold To Code</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.SoldToName} onClick={() => handleFilterChange('SoldToName',_formFilterCheckvalue?.SoldToName)} /> Sold To Name</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.SoldToBlock} onClick={() => handleFilterChange('SoldToBlock',_formFilterCheckvalue?.SoldToBlock)} /> Sold To Block</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.SoldToDistrict} onClick={() => handleFilterChange('SoldToDistrict',_formFilterCheckvalue?.SoldToDistrict)} /> Sold To District</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.SoldToDistrict} onClick={() => handleFilterChange('SoldToLocation',_formFilterCheckvalue?.SoldToLocation)} /> Sold To Location</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.ShipToName} onClick={() => handleFilterChange('ShipToCode',_formFilterCheckvalue?.ShipToCode)} /> Ship To Code</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.ShipToName} onClick={() => handleFilterChange('ShipToName',_formFilterCheckvalue?.ShipToName)} /> Ship To Name</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.ShipToName} onClick={() => handleFilterChange('ShipToBlock',_formFilterCheckvalue?.ShipToBlock)} /> Ship To Block</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.ShipToDistrict} onClick={() => handleFilterChange('ShipToDistrict',_formFilterCheckvalue?.ShipToDistrict)} /> Ship To District</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.ShipToDistrict} onClick={() => handleFilterChange('ShipToLocation',_formFilterCheckvalue?.ShipToLocation)} /> Ship To Location</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.InvoiceValue} onClick={() => handleFilterChange('InvoiceValue',_formFilterCheckvalue?.InvoiceValue)} /> Invoice Value</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.InvoiceValueWithoutGST} onClick={() => handleFilterChange('InvoiceValueWithoutGST',_formFilterCheckvalue?.InvoiceValueWithoutGST)} /> Invoice Value Without GST</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.InvoiceVlueWithGST} onClick={() => handleFilterChange('InvoiceVlueWithGST',_formFilterCheckvalue?.InvoiceVlueWithGST)} /> Invoice Vlue With GST</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.GST} onClick={() => handleFilterChange('GST',_formFilterCheckvalue?.GST)} /> GST</div>
                                <div className="dropdown-item" role="button"><Checkbox className='p-0' color="secondary" checked={_formFilterCheckvalue?.MaterialGroup} onClick={() => handleFilterChange('MaterialGroup',_formFilterCheckvalue?.MaterialGroup)} /> Material Group</div>

                            </div>
                            </div>
                            <div className="mt-2 col-md-1">
                            <img className="p-2" style={{ height: '55px' }} src={ExportExcel}
                             onClick={exportEXCEL} 
                             />
                            </div>
                            </div>
                            </div>
                           
                           
                        </div>
                        <div className="row align-items-center">
                        <div className="mt-2 col-md-3">
                            
                        </div>
                        <div className="mt-2 col-md-3" style={{ color: "#00AAE7" }}>
                            
                        </div>
                        <div className="mt-2 col-md-3">
                            
                        </div>
                        <div className="mt-2 col-md-2">
                            

                        </div>
                        <div className="mt-2 col-md-1">
                            
                        </div>
                    </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-md-1">
                        </div>
                        <div className="col-md-10">
                            <div className="tablebx" style={{ overflowX: "auto" }}>

                                <Paper sx={{ width: '100%', mb: 1 }} elevation={0}>
                                    <TableContainer className="rounded">
                                        <Table className="border" size="small">
                                            <TableHead className=" py-4" style={{ borderBottom: '0.1rem  solid gray', height: "4rem" ,backgroundColor:"#0098E1" }}>
                                                <TableRow>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>S.No</TableCell>
                                                    {_formFilterCheckvalue?.DealerCode===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Dealer Code</TableCell>}
                                                    {_formFilterCheckvalue?.DealerName===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Dealer Name</TableCell>}
                                                    {_formFilterCheckvalue?.ProductCode===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Product Code</TableCell>}
                                                    {_formFilterCheckvalue?.ProductDesc===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Product Desc</TableCell>}
                                                    {_formFilterCheckvalue?.BatchNo===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Batch No</TableCell>}
                                                    {_formFilterCheckvalue?.OBDNo===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>OBD No</TableCell>}
                                                    {_formFilterCheckvalue?.ActualGiQuantity===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Actual GI Quantity</TableCell>}
                                                    {_formFilterCheckvalue?.ActualGiDate===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Actual GI Date</TableCell>}
                                                    {_formFilterCheckvalue?.ZGENo===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>ZGE No</TableCell>}
                                                    {_formFilterCheckvalue?.SupplyingPlantCode===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Supplying Plant Code</TableCell>}
                                                    {_formFilterCheckvalue?.SupplyingPlantName===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Supplying Plant Name</TableCell>}
                                                    {_formFilterCheckvalue?.SupplyingPlantDistrict===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Supplying Plant District</TableCell>}
                                                    {_formFilterCheckvalue?.SupplyingPlantState===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Supplying Plant State</TableCell>}
                                                    {_formFilterCheckvalue?.TransporterName===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Transporter Name</TableCell>}
                                                    {_formFilterCheckvalue?.LrNo===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Lr No</TableCell>}
                                                    {_formFilterCheckvalue?.LrDate===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Lr Date</TableCell>}
                                                    {_formFilterCheckvalue?.TruckNo===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Truck No</TableCell>}
                                                    {_formFilterCheckvalue?.TruckCapacity===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Truck Capacity</TableCell>}
                                                    {_formFilterCheckvalue?.InvoiceNumber===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Invoice Number</TableCell>}
                                                    {_formFilterCheckvalue?.InvoiceDate===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Invoice Date</TableCell>}
                                                    {_formFilterCheckvalue?.SoldToCode===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Sold-to Code</TableCell>}
                                                    {_formFilterCheckvalue?.SoldToName===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Sold-to Name</TableCell>}
                                                    {_formFilterCheckvalue?.SoldToLocation===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Sold-to Location</TableCell>}
                                                    {_formFilterCheckvalue?.SoldToBlock===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Sold-to Block</TableCell>}
                                                    {_formFilterCheckvalue?.SoldToDistrict===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Sold-to District</TableCell>}
                                                    {_formFilterCheckvalue?.ShipToCode===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Ship-to Code</TableCell>}
                                                    {_formFilterCheckvalue?.ShipToName===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Ship-to Name</TableCell>}
                                                    {_formFilterCheckvalue?.ShipToLocation===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Ship-to Location</TableCell>}
                                                    {_formFilterCheckvalue?.ShipToBlock===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Ship-to Block</TableCell>}
                                                    {_formFilterCheckvalue?.ShipToDistrict===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Ship-to District</TableCell>}
                                                    {_formFilterCheckvalue?.InvoiceValue===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Invoice Value</TableCell>}
                                                    {_formFilterCheckvalue?.InvoiceValueWithoutGST===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Invoice Value Without GST</TableCell>}
                                                    {_formFilterCheckvalue?.InvoiceVlueWithGST===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Invoice Vlue With GST</TableCell>}
                                                    {_formFilterCheckvalue?.GST===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>GST</TableCell>}
                                                    {_formFilterCheckvalue?.MaterialGroup===true && <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Material Group</TableCell>}
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>

                                                {_list?.length > 0 ?  _list.filter((content: any) => {
                                                        const lowerSearchInput = searchInput.toLowerCase();
                                                        const lowerSelectedCity = selectedCity.toLowerCase();
                                                        const lowerSelectedManager = selectedManager.toLowerCase();
                                                        const lowerSelectedinvoice = selectedinvoice.toLowerCase();
                                                        const lowerSelectedregion = selectedregion.toLowerCase();
                                                        return (

                                                            (lowerSearchInput === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchInput))) &&
                                                            (lowerSelectedCity === '' || content.city?.toLowerCase() === lowerSelectedCity||lowerSelectedCity==='selectall') &&
                                                            (lowerSelectedManager === '' || content.manager.toLowerCase() === lowerSelectedManager||lowerSelectedManager==='selectall')&&
                                                            (lowerSelectedinvoice === '' || content.invoiceNumber.toLowerCase() === lowerSelectedinvoice||lowerSelectedinvoice==='selectall')&&
                                                            (lowerSelectedregion === '' || content.region.toLowerCase() === lowerSelectedregion||lowerSelectedregion==='selectall')
                                                            );
                                                    })?.slice(
                                                    (_page - 1) * _rowsPerPage,
                                                    (_page - 1) * _rowsPerPage + _rowsPerPage,
                                                )?.map((item: any, index: number) => (
                                                    <TableRow key={index} className={index % 2 === 0 ? 'grey-row' : 'white-row'}>
                                                        <TableCell align={"center"} className="fs16 border-3 border-white">{(index + 1) + ((_page - 1) * 10)}</TableCell>
                                                        {_formFilterCheckvalue?.DealerCode===true &&<TableCell align="center"className="fs16 border-3 border-white">{item.code}</TableCell>}
                                                        {_formFilterCheckvalue?.DealerName===true && <TableCell align="center"className="fs16 border-3 border-white">{item.name}</TableCell>}
                                                        {_formFilterCheckvalue?.ProductCode===true && <TableCell align="center"className="fs16 border-3 border-white">{item.productCode}</TableCell>}
                                                        {_formFilterCheckvalue?.ProductDesc===true && <TableCell align="center"className="fs16 border-3 border-white">{item.productDesc}</TableCell>}
                                                        {_formFilterCheckvalue?.BatchNo===true && <TableCell align="center"className="fs16 border-3 border-white">{item.batchNo}</TableCell>}
                                                        {_formFilterCheckvalue?.OBDNo===true && <TableCell align="center"className="fs16 border-3 border-white">{item.obdNo}</TableCell>}
                                                        {_formFilterCheckvalue?.ActualGiQuantity===true && <TableCell align="center"className="fs16 border-3 border-white">{item.actualGiQuantity}</TableCell>}
                                                        {_formFilterCheckvalue?.ActualGiDate===true && <TableCell align="center"className="fs16 border-3 border-white">{item.actualGiDate}</TableCell>}
                                                        {_formFilterCheckvalue?.ZGENo===true && <TableCell align="center"className="fs16 border-3 border-white">{item.ztrsNo}</TableCell>}
                                                        {_formFilterCheckvalue?.SupplyingPlantCode===true && <TableCell align="center"className="fs16 border-3 border-white">{item.supplyingPlantCode}</TableCell>}
                                                        {_formFilterCheckvalue?.SupplyingPlantName===true && <TableCell align="center"className="fs16 border-3 border-white">{item.supplyingPlantName}</TableCell>}
                                                        {_formFilterCheckvalue?.SupplyingPlantDistrict===true && <TableCell align="center"className="fs16 border-3 border-white">{item.supplyingPlantDistrict}</TableCell>}
                                                        {_formFilterCheckvalue?.SupplyingPlantState===true && <TableCell align="center"className="fs16 border-3 border-white">{item.supplyingPlantState}</TableCell>}
                                                        {_formFilterCheckvalue?.TransporterName===true && <TableCell align="center"className="fs16 border-3 border-white">{item.tptName}</TableCell>}
                                                        {_formFilterCheckvalue?.LrNo===true && <TableCell align="center"className="fs16 border-3 border-white">{item.lrNo}</TableCell>}
                                                        {_formFilterCheckvalue?.LrDate===true && <TableCell align="center"className="fs16 border-3 border-white">{item.lrDate}</TableCell>}
                                                        {_formFilterCheckvalue?.TruckNo===true && <TableCell align="center"className="fs16 border-3 border-white">{item.truckNo}</TableCell>}
                                                        {_formFilterCheckvalue?.TruckCapacity===true && <TableCell align="center"className="fs16 border-3 border-white">{item.truckCapacity}</TableCell>}
                                                        {_formFilterCheckvalue?.InvoiceNumber===true && <TableCell align="center"className="fs16 border-3 border-white">{item.invoiceNumber}</TableCell>}
                                                        {_formFilterCheckvalue?.InvoiceDate===true && <TableCell align="center"className="fs16 border-3 border-white">{item.invoiceDate}</TableCell>}
                                                        {_formFilterCheckvalue?.SoldToCode===true && <TableCell align="center"className="fs16 border-3 border-white">{item.soldToCode}</TableCell>}
                                                        {_formFilterCheckvalue?.SoldToName===true && <TableCell align="center"className="fs16 border-3 border-white">{item.soldToName}</TableCell>}
                                                        {_formFilterCheckvalue?.SoldToLocation===true && <TableCell align="center"className="fs16 border-3 border-white">{item.soldToLocation}</TableCell>}
                                                        {_formFilterCheckvalue?.SoldToBlock===true && <TableCell align="center"className="fs16 border-3 border-white">{item.soldToBlock}</TableCell>}
                                                        {_formFilterCheckvalue?.SoldToDistrict===true && <TableCell align="center"className="fs16 border-3 border-white">{item.soldToDistrict}</TableCell>}
                                                        {_formFilterCheckvalue?.ShipToCode===true && <TableCell align="center"className="fs16 border-3 border-white">{item.shipToCode}</TableCell>}
                                                        {_formFilterCheckvalue?.ShipToName===true && <TableCell align="center"className="fs16 border-3 border-white">{item.shipToName}</TableCell>}
                                                        {_formFilterCheckvalue?.ShipToLocation===true && <TableCell align="center"className="fs16 border-3 border-white">{item.shipToLocation}</TableCell>}
                                                        {_formFilterCheckvalue?.ShipToBlock===true && <TableCell align="center"className="fs16 border-3 border-white">{item.shipToBlock}</TableCell>}
                                                        {_formFilterCheckvalue?.ShipToDistrict===true && <TableCell align="center"className="fs16 border-3 border-white">{item.shipToDistrict}</TableCell>}
                                                        {_formFilterCheckvalue?.InvoiceValue===true && <TableCell align="center"className="fs16 border-3 border-white">{item.invoiceValue}</TableCell>}
                                                        {_formFilterCheckvalue?.InvoiceValueWithoutGST===true && <TableCell align="center"className="fs16 border-3 border-white">{item.invoiceValueWithoutGst}</TableCell>}
                                                        {_formFilterCheckvalue?.InvoiceVlueWithGST===true && <TableCell align="center"className="fs16 border-3 border-white">{item.invoiceValueWithGst}</TableCell>}
                                                        {_formFilterCheckvalue?.GST===true && <TableCell align="center"className="fs16 border-3 border-white">{item.gst}</TableCell>}
                                                        {_formFilterCheckvalue?.MaterialGroup===true && <TableCell align="center"className="fs16 border-3 border-white">{item.materialGroup}</TableCell>}
                                                        
                                                       
                                                    </TableRow>

                                                ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">Data Not Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                                <SkeletonProviderTables columns={9} visible={_loading} />
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    <div className="d-flex justify-content-end align-items-center mt-3 pb-4">
                                        <Pagination count={Math.ceil(_totalCount / _rowsPerPage)} page={_page} onChange={handleChangePage}
                                            variant="outlined" size={"small"} color={"primary"} shape="rounded"
                                            renderItem={(item) => (
                                                <PaginationItem sx={{ mx: '4px' }}
                                                    {...item}
                                                />
                                            )} />
                                    </div>
                                </Paper>

                            </div>
                        </div>
                        <div className="col-md-1">
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-6"></div>
                        <div className="col-md-6"></div>
                    </div>
                </div>

            </div>)
        }
    </>);
}

export default Sales;


