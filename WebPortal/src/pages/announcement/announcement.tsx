import { FormControl, FormHelperText, InputLabel, MenuItem, Select, TextField, Dialog, Paper, Pagination, PaginationItem, OutlinedInput, InputAdornment, IconButton, Button, Checkbox } from "@mui/material";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Search } from "@mui/icons-material";
import '../../assets/scss/page.css';
import { addAnnouncement, getAllAnnouncements, getAllDealers, getStateManagerDetails } from "../../models/model";
import { useEffect, useState } from "react";
import { SkeletonProviderTables } from "../../providers/SkeletonProvider";
import DatePickerControl from "../../components/helpers/DatePickerControl";
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import ExportExcel from '../../assets/images/Export Excel.png';
import { downloadExcel } from "react-export-table-to-excel";
import EditIcon from '@mui/icons-material/Edit';
import Swal from 'sweetalert2';
import moment from "moment";
import './announcement.scss'
function Announcements() {
    const [_type, _setType] = useState<any>("create");
    const [_userId, _setUserId] = useState("");
    const [_loading, _setLoading] = useState<any>(true);
    const [_list, _setList] = useState<any>([]);
    const [_userteamslist, _setUserTeamsList] = useState<any>([]);
    const [searchInput, setSearchInput] = useState("");
    const [_assignList, _setAssignList] = useState<any>([])
    const [_editList, _setEditList] = useState<any>([])
    const [_popup, _setPopup] = useState<any>(false);
    const [_checkvalue, _setCheckvalue] = useState<any>({});
    const [_formCheckvalue, _setFormCheckvalue] = useState<any>({});
    const [selectedCity, setSelectedCity] = useState('');
    const [_titleresponse, _settitleresponse] = useState<any>([]);
    const [_managerName, _setManagerName] = useState<any>([]);
    const [_allmanagerName, _setAllmanagerName] = useState<any>([]);
    const [selectedManager, setSelectedManager] = useState('');
    const [_createManager, _setCreateManager] = useState<any>(false);
    const [_page, _setPage] = useState(1);
    const [_rowsPerPage] = useState(10);
    const [_totalCount, _setTotalCount] = useState(0);
    const [_buttonLoading, _setbuttonLoading] = useState(false);


    const dealersList = () => {
        var ManagerId=0;var UserId=0;
        var StateManagerId=0;
        getAllDealers(UserId,ManagerId,StateManagerId)
            .then(response => {
                debugger;
                _settitleresponse(response?.data?.data?.data);
                console.log(_list);
            })
            .catch((err) => console.log(err)).finally(() => _setLoading(false));
    }

    const managerList = () => {
        getStateManagerDetails('', '', '', '', 3)
            .then(response => {
                _setManagerName(response?.data?.data?.data);
                console.log(_list);
            })
            .catch((err) => console.log(err)).finally(() => _setLoading(false));
    }

    const [_formData, _setFormData] = useState<any>({
        Id: 0,
        FromDate: "",
        ToDate: "",
        AnnouncementByManager: "",
        AnnouncementByManagerId: 0,
        AnnouncementByCity: "",
        AnnouncementByDealer: "",
        AnnouncementByDealerId: 0,
        Announcementfilter: '',
        Announcement1: "",
        AnnouncementName: "",
        Status: "",
    });

    const changeFormData = (name: string, value: any) => {
        // _setFormData({ ..._formData, [name]: value });
        debugger;
        if (name === "AnnouncementByManager" && value === "all") {

            const allFirstNames = _managerName.map((item: any) => item.firstName);
            _setFormData({ ..._formData, AnnouncementByManager: allFirstNames });
        } else {

            _setFormData({ ..._formData, [name]: value });
        }
    };
    const handleall = () => {
        debugger
        const allFirstNames = _managerName.map((item: any) => item.firstName);
        _setFormData({ ..._formData, AnnouncementByManager: allFirstNames });
    }

    const handleChangePage = (event: React.ChangeEvent<unknown>, value: number) => {
        _setPage(value);
    };

    const handleCityChange = (event: any) => {
        setSelectedCity(event.target.value);
    };
    const handleManagerChange = (event: any) => {
        setSelectedManager(event.target.value);
    };

    const gridBindDetails = () => {
        getAllAnnouncements("", "")
            .then(response => {
                _setList(response?.data?.data?.data);
                _setTotalCount([...response?.data?.data?.data]?.length);
                console.log(_list);
            })
            .catch((err) => console.log(err)).finally(() => _setLoading(false));
    }

    const changeDateFormat = (date: any) => {
        return moment(date).format("DD/MM/YYYY");
    }

    const createManager = () => {
        _setCreateManager(true)
    }

    const cancelManager = () => {
        _setCreateManager(false)
        _setFormData({
            ..._formData,
            Id: 0,
            FromDate: "",
            ToDate: "",
            AnnouncementByManager: "",
            AnnouncementByDealer: "",
            AnnouncementByCity: "",
            Announcementfilter: "",
            AnnouncementName: "",
            Announcement1: "",
            status: "",
        });
        _setType('')
    }

    const validate = {
        FromDate: { error: false, message: "" },
        ToDate: { error: false, message: "" },
        AnnouncementByManager: { error: false, message: "" },
        AnnouncementByCity: { error: false, message: "" },
        Announcementfilter: { error: false, message: "" },
        AnnouncementName: { error: false, message: "" },
        AnnouncementByDealer: { error: false, message: "" },
        Announcement1: { error: false, message: "" },
    };
    const [_managervalidate, _setManagervalidate] = useState(validate);

    const InsertAnnouncement = () => {
        debugger;
        //  var userid = user.id;
        const _validate: any = Object.assign({}, validate);
        let valid = true;
        if (_formData.FromDate === "" || _formData.FromDate === 0) {
            _validate.FromDate.error = true;
            _validate.FromDate.message = "Required Field";
            valid = false;
        }
        if (_formData.ToDate === "" || _formData.ToDate === 0) {
            _validate.ToDate.error = true;
            _validate.ToDate.message = "Required Field";
            valid = false;
        }
        if (!_formData.AnnouncementByManager && !_formData.Announcementfilter && !_formData.AnnouncementName && _formData.AnnouncementByCity) {
            _validate.AnnouncementByManager.error = true;
            _validate.AnnouncementByManager.message = "At least one field is required";
            _validate.Announcementfilter.error = true;
            _validate.Announcementfilter.message = "At least one field is required";
            _validate.AnnouncementName.error = true;
            _validate.AnnouncementName.message = "At least one field is required";
            _validate.AnnouncementByCity.error = true;
            _validate.AnnouncementByCity.message = "At least one field is required";
            valid = false;
        }
        else {
            _validate.AnnouncementByManager.error = false;
            _validate.AnnouncementByManager.message = "";
            _validate.Announcementfilter.error = false;
            _validate.Announcementfilter.message = "";
            _validate.AnnouncementName.error = false;
            _validate.AnnouncementName.message = "";
            _validate.AnnouncementByCity.error = false;
            _validate.AnnouncementByCity.message = "";
        }

        if (_formData.Announcement1 === "" || _formData.Announcement1 === 0) {
            _validate.Announcement1.error = true;
            _validate.Announcement1.message = "Required Field";
            valid = false;
        }

        _setManagervalidate(_validate);
        if (!valid) {
            _setbuttonLoading(false);
            return;
        }
        _setbuttonLoading(true);

        addAnnouncementDetail()

        _setFormData({
            ..._formData,
            Id: 0,
            FromDate: "",
            ToDate: "",
            AnnouncementByManager: "",
            AnnouncementByDealer: "",
            AnnouncementByCity: "",
            Announcementfilter: '',
            AnnouncementName: '',
            Announcement1: "",
            status: "",
        });
    };

    const addAnnouncementDetail = () => {
        var dealerName = "";
        var managerName = "";

        if (_formData.Announcementfilter == "All") {
            if (_formData.AnnouncementByManager != 0) {
                var res = _managerName?.filter((filter: any) => {
                    return (
                        _formData.AnnouncementByManager === filter?.id
                    )
                }).map((item: any) => (
                    managerName = item?.firstName
                ));
            }
            else {
                managerName = "All";
            }
            if (_formData.AnnouncementByDealer != 0) {
                var res = _titleresponse?.filter((filter: any) => {
                    return (
                        _formData.AnnouncementByDealer === filter?.id
                    )
                }).map((item: any) => (
                    dealerName = item?.firstName
                ));
            }
            else {
                dealerName = "All";
            }
        }

        if(_formData.Announcementfilter == "Manager")
        {
            if (_formData.AnnouncementByManager != 0) {
                var res = _managerName?.filter((filter: any) => {
                    return (
                        _formData.AnnouncementByManager === filter?.id
                    )
                }).map((item: any) => (
                    managerName = item?.firstName
                ));
            }
            else {
                managerName = "All";
            }
        }

        if(_formData.Announcementfilter == "Dealers")
        {
            if (_formData.AnnouncementByDealer != 0) {
                var res = _titleresponse?.filter((filter: any) => {
                    return (
                        _formData.AnnouncementByDealer === filter?.id
                    )
                }).map((item: any) => (
                    dealerName = item?.firstName
                ));
            }
            else {
                dealerName = "All";
            }
        }
       

        const data = {
            id: _formData.Id,
            fromDate: _formData.FromDate,
            toDate: _formData.ToDate,
            announcementFor: _formData.Announcementfilter,
            announcementName: _formData.AnnouncementName,
            announcementByManager: managerName,
            AnnouncementByManagerId: _formData.AnnouncementByManager || 0,
            announcementByCity: _formData.AnnouncementByCity,
            announcementByDealer: dealerName,
            AnnouncementByDealerId: _formData.AnnouncementByDealer || 0,
            announcement1: _formData.Announcement1,
            // announcementById: 0,
            status: _formData.status,
        };
        console.log(data);
        addAnnouncement(data).then((response: any) => {
            debugger;
            if (response.data.status === true) {
                Swal.fire({
                    title: 'Success!',
                    text: 'Announcement Created Successfully',
                    icon: 'success',
                    confirmButtonText: 'OK',
                });
                _setCreateManager(false)
                gridBindDetails();
                //  swal("Manager Added successfully!", { icon: "success" });
            } else {
                //  swal("Something went wrong!", { icon: "warning" });
            }
        });
        _setbuttonLoading(false);
    };

    useEffect(() => {
        gridBindDetails();
        dealersList();
        managerList();
    }, []);

    const actionClick = (type: any, item?: any) => {
        debugger;
        _setCreateManager(true)
        _setType(type);
        _setUserId(item?.id)

        _setFormData({
            Id: item.id,
            Announcement1: item.announcement1,
            AnnouncementByDealer:item.announcementByDealerId,
            Announcementfilter:item.announcementFor,
          //  AnnouncementByDealerId:item.announcementByDealerId,
            AnnouncementName: item.announcementName,
            AnnouncementByCity: item.announcementByCity,
            AnnouncementByManager:item.announcementByManagerId,
          //  AnnouncementByManagerId:item.announcementByManagerId,
            FromDate: item.fromDate?.split("T")[0],
            ToDate: item.toDate?.split("T")[0],
             status: item.status,
        });
    }


    const updateAnnouncement = () => {
        debugger;
        //  var userid = user.id;
        const _validate: any = Object.assign({}, validate);
        let valid = true;
        if (_formData.FromDate === "" || _formData.FromDate === 0) {
            _validate.FromDate.error = true;
            _validate.FromDate.message = "Required Field";
            valid = false;
        }
        if (_formData.ToDate === "" || _formData.ToDate === 0) {
            _validate.ToDate.error = true;
            _validate.ToDate.message = "Required Field";
            valid = false;
        }
        if (!_formData.AnnouncementByManager && !_formData.Announcementfilter && !_formData.AnnouncementName && _formData.AnnouncementByCity) {
            _validate.AnnouncementByManager.error = true;
            _validate.AnnouncementByManager.message = "At least one field is required";
            _validate.Announcementfilter.error = true;
            _validate.Announcementfilter.message = "At least one field is required";
            _validate.AnnouncementName.error = true;
            _validate.AnnouncementName.message = "At least one field is required";
            _validate.AnnouncementByCity.error = true;
            _validate.AnnouncementByCity.message = "At least one field is required";
            valid = false;
        }
        else {
            _validate.AnnouncementByManager.error = false;
            _validate.AnnouncementByManager.message = "";
            _validate.Announcementfilter.error = false;
            _validate.Announcementfilter.message = "";
            _validate.AnnouncementName.error = false;
            _validate.AnnouncementName.message = "";
            _validate.AnnouncementByCity.error = false;
            _validate.AnnouncementByCity.message = "";
        }

        if (_formData.Announcement1 === "" || _formData.Announcement1 === 0) {
            _validate.Announcement1.error = true;
            _validate.Announcement1.message = "Required Field";
            valid = false;
        }


        _setManagervalidate(_validate);
        if (!valid) {
            _setbuttonLoading(false);
            return;
        }
        _setbuttonLoading(true);

        updateAnnouncementDetail()

        _setFormData({
            ..._formData,
            Id: 0,
            FromDate: "",
            ToDate: "",
            AnnouncementByManager: "",
            AnnouncementByDealer: "",
            AnnouncementByCity: "",
            AnnouncementName: "",
            status: "",
        });
    }

    const updateAnnouncementDetail = () => {
        var dealerName = "";
        var managerName = "";

        if (_formData.Announcementfilter == "All") {
            if (_formData.AnnouncementByManager != 0) {
                var res = _managerName?.filter((filter: any) => {
                    return (
                        _formData.AnnouncementByManager === filter?.id
                    )
                }).map((item: any) => (
                    managerName = item?.firstName
                ));
            }
            else {
                managerName = "All";
            }
            if (_formData.AnnouncementByDealer != 0) {
                var res = _titleresponse?.filter((filter: any) => {
                    return (
                        _formData.AnnouncementByDealer === filter?.id
                    )
                }).map((item: any) => (
                    dealerName = item?.firstName
                ));
            }
            else {
                dealerName = "All";
            }
        }

        if(_formData.Announcementfilter == "Manager")
        {
            if (_formData.AnnouncementByManager != 0) {
                var res = _managerName?.filter((filter: any) => {
                    return (
                        _formData.AnnouncementByManager === filter?.id
                    )
                }).map((item: any) => (
                    managerName = item?.firstName
                ));
            }
            else {
                managerName = "All";
            }
        }

        if(_formData.Announcementfilter == "Dealers")
        {
            if (_formData.AnnouncementByDealer != 0) {
                var res = _titleresponse?.filter((filter: any) => {
                    return (
                        _formData.AnnouncementByDealer === filter?.id
                    )
                }).map((item: any) => (
                    dealerName = item?.firstName
                ));
            }
            else {
                dealerName = "All";
            }
        }
        const data = {
            id: _userId,
            fromDate: _formData.FromDate,
            toDate: _formData.ToDate,
            announcementFor: _formData.Announcementfilter,
            announcementName: _formData.AnnouncementName,
            announcementByManager: managerName,
            AnnouncementByManagerId: _formData.AnnouncementByManager,
            announcementByCity: _formData.AnnouncementByCity,
            announcementByDealer: dealerName,
            AnnouncementByDealerId: _formData.AnnouncementByDealer,
            announcement1: _formData.Announcement1,
            announcementById: 0,
            status: _formData.status,
            // status: _formData.status,
        };
        console.log(data);
        addAnnouncement(data).then((response: any) => {
            debugger;
            if (response.data.status === true) {
                Swal.fire({
                    title: 'Success!',
                    text: 'Announcement Updated Successfully',
                    icon: 'success',
                    confirmButtonText: 'OK',
                });
                _setCreateManager(false)
                gridBindDetails();
                //  swal("Manager Added successfully!", { icon: "success" });
            } else {
                //  swal("Something went wrong!", { icon: "warning" });
            }
        });
        _setbuttonLoading(false);
    }
    const sortedData = _list.slice().sort((a: any, b: any) => b.id - a.id);

    const exportEXCEL = () => {
        debugger;
        const header = ["Announcement Name", "Date and Time", "City", "Manager", "Dealer"];

        let body: any = _list.map((e: any) => {
            const fullName = e?.firstName + ' ' + e?.lastName;
            return [e?.announcementName, e?.fromDate, e?.announcementByCity, e?.announcementByManager, e?.announcementByDealer]
        })

        // var curDate = moment()?.format("YYYY-MM-DD")

        var filename = 'Announcement'

        downloadExcel({

            fileName: filename,

            sheet: "AnnouncementList",

            tablePayload: {

                header,

                body: body,

            },

        });
    }


    return (<>

        <div>
            {_createManager === false && (
                <div className="my-3 mt-4">
                    <div className="col-md-12 mx-auto mt-4">
                        <div className="row align-items-center">
                            <div className="col-md-1"></div>
                            <div className="col-md-11"><div className="row">
                                <div className="mt-2 col-md-3">
                                    <FormControl className="bg-light" size="small" fullWidth variant="outlined" >
                                        <InputLabel htmlFor="outlined-adornment-password">Search</InputLabel>
                                        <OutlinedInput className="bg-white"
                                            label="Search"
                                            onChange={(e) => setSearchInput(e.target.value)}
                                            endAdornment={
                                                <InputAdornment position="end">
                                                    <IconButton aria-label="toggle password visibility" edge="end" type="submit">
                                                        <Search />
                                                    </IconButton>
                                                </InputAdornment>
                                            }
                                        />
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by city</InputLabel>
                                        <Select label="Select Range" value={selectedCity}
                                            onChange={handleCityChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={item.announcementByCity}>{item.announcementByCity}</MenuItem>
                                            ))}
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>
                                {/* <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by region</InputLabel>
                                        <Select label="Select Range" value={selectedCity}
                                            onChange={handleCityChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={item.region}>{item.region}</MenuItem>
                                            ))}
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div> */}
                                {/* <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by state</InputLabel>
                                        <Select label="Select Range" value={selectedCity}
                                            onChange={handleCityChange}>
                                            {_list.map((item: any, index: number) => (<MenuItem value={item.state}>{item.state}</MenuItem>
                                            ))}
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div> */}
                                <div className="mt-2 col-md-2">
                                    <FormControl fullWidth size="small" className="bg-white">
                                        <InputLabel>Filter by Manager</InputLabel>
                                        <Select label="Select Range" value={selectedManager} onChange={handleManagerChange}>
                                            {_list.map((item: any, index: number) => (
                                                <MenuItem value={item?.announcementByManager}>{item?.announcementByManager}</MenuItem>
                                            ))}
                                            <MenuItem value={'selectall'}>All</MenuItem>
                                        </Select>
                                    </FormControl>
                                </div>
                                <div className="mt-2 col-md-3">
                                    <Button type="button" style={{ background:"linear-gradient(to bottom, #9FD8FD 50%,  #8CCEFF 50%)",border:"2px solid  #0098E1"}} className="btn text-dark text-capitalize" onClick={createManager}>
                                        + create
                                    </Button>
                                </div> <div className="mt-2 col-md-1">
                                    <img className="p-2" style={{ height: '55px' }} src={ExportExcel}
                                        onClick={exportEXCEL}
                                    />
                                </div>
                            </div>
                            </div>

                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-md-1">
                        </div>
                        <div className="col-md-10">
                            <div className="tablebx" style={{ overflowX: "auto" }}>
                                <Paper sx={{ width: '100%', mb: 1 }} elevation={0}>
                                    <TableContainer className="rounded">
                                        <Table className="border" size="small">
                                            <TableHead className=" py-4" style={{ borderBottom: '0.1rem  solid gray', backgroundColor:"#0098E1", height: "4rem" }}>
                                                <TableRow>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>S.No.</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Announcement Name</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Start Date</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>End Date</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>City</TableCell>
                                                    {/* <TableCell className="py-2 fw-bold text-white" align={"center"}>State</TableCell> */}
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Manager</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Dealer</TableCell>
                                                    <TableCell className="fs16 py-2 fw-bold text-white border-3 border-white" align={"center"}>Action</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>

                                                {sortedData?.length > 0 ? sortedData.filter((content: any) => {
                                                    const lowerSearchInput = searchInput.toLowerCase();
                                                    const lowerSelectedCity = selectedCity.toLowerCase();
                                                    const lowerSelectedManager = selectedManager.toLowerCase();

                                                    return (

                                                        (lowerSearchInput === '' || Object.values(content).some((value) => value?.toString().toLowerCase().includes(lowerSearchInput))) &&
                                                        (lowerSelectedCity === '' || content.announcementByCity?.toLowerCase() === lowerSelectedCity || lowerSelectedCity === 'selectall') &&
                                                        (lowerSelectedManager === '' || content.announcementByManager?.toLowerCase() === lowerSelectedManager || lowerSelectedManager === 'selectall')
                                                    );
                                                })?.slice(
                                                    (_page - 1) * _rowsPerPage,
                                                    (_page - 1) * _rowsPerPage + _rowsPerPage,
                                                )?.map((item: any, index: number) => (
                                                    <TableRow key={index} style={{ height: "3rem" }} className={index % 2 === 0 ? 'grey-row' : 'white-row'}>
                                                        <TableCell align={"center"} className="fs16 border-3 border-white">{(index + 1) + ((_page - 1) * 10)}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item.announcementName}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{changeDateFormat(item.fromDate)}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{changeDateFormat(item.toDate)}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item.announcementByCity}</TableCell>
                                                        {/* <TableCell align="center">...</TableCell> */}
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item.announcementByManager}</TableCell>
                                                        <TableCell align="center" className="fs16 border-3 border-white">{item.announcementByDealer}</TableCell>
                                                        <TableCell  align="center" className="fs16 border-3 border-white"><div className="text-black rounded--50 bgBlue my-3  " style={{ textAlign: "center" }} role="button" onClick={() => actionClick("update", item)}>
                                                            {/* <span className="mt-1" >Edit</span> */}
                                                            <span className="mx-2">
                                                                <EditIcon style={{ color: "black" }} />
                                                            </span>
                                                        </div></TableCell>
                                                    </TableRow>
                                                ))
                                                    : (
                                                        <TableRow key={0}>
                                                            <TableCell align="center" colSpan={7}>
                                                                <h3 className="text-muted">Data Not Found</h3>
                                                            </TableCell>
                                                        </TableRow>
                                                    )}
                                                <SkeletonProviderTables columns={9} visible={_loading} />
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                    <div className="d-flex justify-content-end align-items-center mt-3 pb-4">
                                        <Pagination count={Math.ceil(_totalCount / _rowsPerPage)} page={_page} onChange={handleChangePage}
                                            variant="outlined" size={"small"} color={"primary"} shape="rounded"
                                            renderItem={(item) => (
                                                <PaginationItem sx={{ mx: '4px' }}
                                                    {...item}
                                                />
                                            )} />
                                    </div>
                                </Paper>
                            </div>
                        </div>
                        <div className="col-md-1">
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-6"></div>
                        <div className="col-md-6"></div>
                    </div>
                </div>
            )}
        </div>

        {_createManager && (
            <div>
                <div className="row mt-4">
                    <div className="col-md-1"></div>
                    <div className=" col-md-3 fw-bold ">Create Announcement</div>
                </div>

                <div className="row mt-3">
                    <div className="col-md-1"></div>
                    <div className="col-md-3">
                        <div className="fs16">Announcement Name</div>
                        <TextField
                            className='mt-2 w-100'
                            value={_formData.AnnouncementName}
                            onChange={(event) => {
                                changeFormData("AnnouncementName", event.target.value);
                            }}
                            error={_managervalidate?.AnnouncementName?.error}
                            helperText={_managervalidate?.AnnouncementName?.message}
                        />
                        {/* <div>Announcement by Managers</div>
                       
                     <FormControl
  className='mt-2 w-100'
  error={_managervalidate?.AnnouncementByManager?.error}>
  <Select
    value={_formData.AnnouncementByManager}
    onChange={(event) => changeFormData("AnnouncementByManager", event.target.value)}>
        <MenuItem  value={'all'}>All</MenuItem>
    {_managerName?.map((item: any, index: any) => (
      <MenuItem value={item.firstName}>{item.firstName}</MenuItem>
    ))}
    
  </Select>
</FormControl>
                        <FormHelperText>{_managervalidate?.AnnouncementByManager?.message}</FormHelperText> */}
                    </div>
                </div>
                <div className="row mt-3">
                    <div className="col-md-1"></div>
                    <div className="col-md-3">
                        <div className="fs16">Filters</div>

                        <FormControl
                            className='mt-2 w-100'
                            error={_managervalidate?.Announcementfilter?.error}
                        >
                            <InputLabel>by Managers/Dealers/All</InputLabel>
                            <Select
                                label="Select Range"
                                value={_formData.Announcementfilter}
                                onChange={(event) => changeFormData("Announcementfilter", event.target.value)}
                            >
                                <MenuItem value={'All'}>All</MenuItem>
                                <MenuItem value={'Manager'}>Managers</MenuItem>
                                <MenuItem value={'Dealers'}>Dealers</MenuItem>
                                {/* <MenuItem value={'City'}>City</MenuItem> */}
                            </Select>
                        </FormControl>
                        <FormHelperText>{_managervalidate?.Announcementfilter?.message}</FormHelperText>

                    </div>
                    <div className="col-md-3">

                        {(_formData.Announcementfilter === 'Manager' && (
                            <div className="managers">
                                <div className="fs16">Announcement by Managers</div>
                                <FormControl
                                    className='mt-2 w-100'
                                    error={_managervalidate?.AnnouncementByManager?.error}
                                >
                                    <Select
                                        value={_formData.AnnouncementByManager}
                                        onChange={(event) => changeFormData("AnnouncementByManager", event.target.value)}
                                    >
                                        <MenuItem value={"0"}>All</MenuItem>
                                        {_managerName?.map((item: any, index: any) => (
                                            <MenuItem value={item.id}>{item.firstName}</MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                                <FormHelperText>{_managervalidate?.AnnouncementByManager?.message}</FormHelperText>
                            </div>
                        ))}

                        {(_formData.Announcementfilter === 'Dealers' && (
                            <div className="dealers">
                                <div className="fs16">Announcement by Dealers</div>
                                <FormControl
                                    className='mt-2 w-100'
                                    error={_managervalidate?.AnnouncementByDealer?.error}
                                >
                                    <Select
                                        value={_formData.AnnouncementByDealer}
                                        onChange={(event) => changeFormData("AnnouncementByDealer", event.target.value)}
                                    >
                                        <MenuItem value={"0"}>All</MenuItem>
                                        {_titleresponse?.map((item: any, index: any) => (
                                            <MenuItem value={item?.id}>{item?.employeeCode + " - " + item.firstName}</MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                                <FormHelperText>{_managervalidate?.AnnouncementByDealer?.message}</FormHelperText>
                            </div>
                        ))}

                        {(_formData.Announcementfilter === 'City' && (
                            <div className="city">
                                <div>Announcement by City</div>
                                <TextField
                                    className='mt-2 w-100'
                                    value={_formData.AnnouncementByCity}
                                    onChange={(event) => {
                                        changeFormData("AnnouncementByCity", event.target.value);
                                    }}
                                    error={_managervalidate?.AnnouncementByCity?.error}
                                    helperText={_managervalidate?.AnnouncementByCity?.message}
                                />
                            </div>
                        ))}

                        {(_formData.Announcementfilter === 'All' && (
                            <div>
                                <div className="managers">
                                    <div className="fs14">Announcement by Managers</div>
                                    <FormControl
                                        className='mt-2 w-100'
                                        error={_managervalidate?.AnnouncementByManager?.error}
                                    >
                                        <Select
                                            value={_formData.AnnouncementByManager}
                                            onChange={(event) => changeFormData("AnnouncementByManager", event.target.value)}
                                        >
                                            <MenuItem value={"0"}>All</MenuItem>
                                            {_managerName?.map((item: any, index: any) => (
                                                <MenuItem value={item.id}>{item.firstName}</MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                    <FormHelperText>{_managervalidate?.AnnouncementByManager?.message}</FormHelperText>
                                </div>

                                <div className="dealers">
                                    <div className="fs16">Announcement by Dealers</div>
                                    <FormControl
                                        className='mt-2 w-100'
                                        error={_managervalidate?.AnnouncementByDealer?.error}
                                    >
                                        <Select
                                            value={_formData.AnnouncementByDealer}
                                            onChange={(event) => changeFormData("AnnouncementByDealer", event.target.value)}
                                        >
                                            <MenuItem value={"0"}>All</MenuItem>
                                            {_titleresponse?.map((item: any, index: any) => (
                                                <MenuItem value={item?.id}>{item?.employeeCode + " - " + item.firstName}</MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                    <FormHelperText>{_managervalidate?.AnnouncementByDealer?.message}</FormHelperText>
                                </div>
                                {/* <div className="city">
                                    <div>Announcement by City</div>
                                    <TextField
                                        className='mt-2 w-100'
                                        value={_formData.AnnouncementByCity}
                                        onChange={(event) => {
                                            changeFormData("AnnouncementByCity", event.target.value);
                                        }}
                                        error={_managervalidate?.AnnouncementByCity?.error}
                                        helperText={_managervalidate?.AnnouncementByCity?.message}
                                    />
                                </div> */}

                            </div>
                        ))}
                    </div>

                </div>

                <div className="row mt-4">
                    <div className="col-md-1"></div>
                    <div className="col-md-3">
                        <div className="fs16">From Date</div>
                        <TextField
                            fullWidth
                            className='mt-1'
                            size='small'
                            type='date'
                            value={_formData?.FromDate}
                            onChange={(event) =>
                                changeFormData("FromDate", event.target.value)
                            }
                            InputLabelProps={{ shrink: true }}
                            error={_managervalidate?.FromDate?.error}
                            helperText={_managervalidate?.FromDate?.message}
                        />
                        {/* <DatePickerControl label={""} onchange={(date: Date | null) => changeFormData('FromDate', date)} value={_formData?.FromDate}
                            error={_managervalidate?.FromDate?.error}
                        /> */}
                    </div>
                    <div className="col-md-3">
                        <div className="fs16">To Date</div>
                        {/* <DateP ickerControl label={""} onchange={(date: Date | null) => changeFormData('ToDate', date)} value={_formData?.ToDate} minDate={_formData?.FromDate}
                            error={_managervalidate?.ToDate?.error}
                        /> */}
                        <TextField
                            fullWidth
                            className='mt-1'
                            size='small'
                            type='date'
                            value={_formData?.ToDate}
                            onChange={(event) =>
                                changeFormData("ToDate", event.target.value)
                            }
                            InputLabelProps={{ shrink: true }}
                            error={_managervalidate?.ToDate?.error}
                            helperText={_managervalidate?.ToDate?.message}
                        />

                    </div>
                    {/* <div className="col-md-2">
                        <div>Email</div>
                        <TextField
                            className='mt-2 w-100'
                            size='small'
                            value={_formData.emailId}
                            onChange={(event) => {
                                changeFormData("emailId", event.target.value);
                            }}
                        />
                    </div> */}

                    <div className="col-md-2"> </div>

                </div>

                <div className="row mt-4">
                    <div className="col-md-1"></div>
                    <div className="col-md-6">
                        <div className="fs16">Announcement</div>
                        <textarea style={{ width: "48rem" }} rows={4}
                            value={_formData.Announcement1}
                            onChange={(event) => {
                                changeFormData("Announcement1", event.target.value);
                            }}
                        ></textarea>
                    </div>
                </div>
                <div className="row mt-4 px-5">
                    <div className="col-md-1"></div>
                    <div className="col-md-9 d-flex justify-content-end">
                        <div className="row d-flex">
                            <div className="col-md-4 text-end my-auto">
                                <span>
                                    <Checkbox checked={_formData.status}
                                        onChange={(event) => {
                                            changeFormData("status", event.target.value);
                                        }}
                                    />
                                    {/* <Checkbox
                                        className="p-0"
                                        checked={item?.status === "Active"}
                                        onChange={(event) => {
                                            activeChecked(item?.id, event.target.checked ? "Active" : "InActive");
                                        }}
                                    /> */}
                                </span>
                                <span className="fs16">Active</span></div>
                            <div className="col-md-4">
                                <Button className="border rounded text-muted" onClick={cancelManager}>Cancel</Button>
                            </div>
                            <div className="col-md-4">
                                <Button className=" rounded text-dark px-2" style={{  background:"linear-gradient(to bottom, #9FD8FD 50%,  #8CCEFF 50%)",border:"2px solid  #0098E1", width: "7rem" }} onClick={_type === 'create' ? InsertAnnouncement : updateAnnouncement}>
                                    {_type === 'create' ? 'Submit' : 'Update'}</Button>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-1"></div>
                </div>

            </div>)

        }

    </>);
}

export default Announcements;


