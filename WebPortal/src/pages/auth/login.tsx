import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { ROUTES } from '../../configs/constants';
import { IconButton, TextField } from '@mui/material';
import {  base64, textFieldStyle, userSession } from '../../services/HelperService';
import { LoadingButton } from '@mui/lab';
import { getLocalStorage, insertUpdateLocalStorage } from '../../services/AuthService';
import { VisibilityOff, Visibility } from '@mui/icons-material';
import { useStateValue } from '../../providers/StateProvider';
import { insertLoginUser } from '../../models/model';


export default function Login() {
    const navigate = useNavigate();
    const [{ }, dispatch]: any = useStateValue();

    const [_formData, _setFormData] = useState<any>({
        username: '',
        password: '',
    });

    const validate: any = {
        username: { error: false, message: '' },
        password: { error: false, message: '' }
    }
    const [_validate, _setValidate] = useState<any>(validate);
    const [_snackMessage, _setSnackMessage] = useState('');
    const [_loading, _setLoading] = useState<any>(false);
    const [_showPassword, _setShowPassword] = useState<any>(false);
    const [_loginKey, _setLoginKey] = useState<any>('');

    // maintain form data on change
    const changeFormData = (key: string, value: any) => {
        _setFormData({ ..._formData, [key]: value })
    }


    // click on next this function call and check validation
    const checkValidation = () => {
        debugger;
        let valid = false;
        let validation = Object.assign({}, validate);
        if (!_formData?.username) {
            validation.username.error = true;
            validation.username.message = 'Required field';
            valid = true;
        }

        if (!_formData?.password) {
            validation.password.error = true;
            validation.password.message = 'Required field';
            valid = true;
        }

        _setValidate(validation);
        return valid;
    }


    const onLoginHandle = (e: any) => {
        e.preventDefault();
        _setLoading(true);
        _setSnackMessage('');
        let validation = Object.assign({}, validate);
        if (checkValidation()) {
            _setLoading(false);
            return;
        }
        const data = {
            LoginUserName: _formData.username,
            LoginPassword: _formData.password,
        };
        console.log(data)
        insertLoginUser(_formData.username, _formData.password)
            .then((response) => {
                debugger;
                if (response.data.status === true) {
                    const LoginKey = response.data.data;
                    _setLoginKey(LoginKey);
                    // insertUpdateLocalStorage('login',
                    // {
                    //         username: _formData.username,
                    //         _token: base64.encode(JSON.stringify(LoginKey))
                    // },
                    // )
                    //insertUpdateLocalStorage('_token' ,response.data.data)
                  insertUpdateLocalStorage('_token', { 'token': response.data.data });

                    const _tempData = userSession(true);

                    dispatch({
                        type: "SET_USER",
                        user: _tempData
                    });

                    navigate(ROUTES.DASHBOARD.HOME);
                }
                else if (response.data.statusCode === 400) {
                    let validation = Object.assign({}, validate);
                     validation.username.error = true;
                    // validation.username.message = 'Invalid Credentials';
                    validation.password.error = true;
                    validation.password.message = 'Invalid Username or Password';
                }
                else {

                }
            })
            .catch((error) => {
                console.log(error);
            })
            .finally(() => {
                _setLoading(false);
            });

        //_setLoading(false);
        // insertUpdateLocalStorage('login', { username: 'guest', password: '123456' })
        // navigate(ROUTES.DASHBOARD.HOME);
    }

    useEffect(() => {
        const tempObj = getLocalStorage('login');
        if (tempObj?.username) {
            navigate(ROUTES.DASHBOARD.HOME);
        }
    }, [])

    return <React.Fragment>
        <form className='p-4 rounded--1' onSubmit={onLoginHandle}>
            <div className="px-3">
                <div className="row mt-4">
                    <div className='text-primary my-3 fw-bold'>Login</div>

                    <div className="mt-2">
                        <TextField variant="standard" label="Username" fullWidth value={_formData.username} onChange={(e: any) => changeFormData('username', e.target.value)}
                            sx={{ ...textFieldStyle }} error={_validate.username.error} helperText={_validate.username.message} />
                    </div>
                </div>
            </div>
            <div className="px-3">
                <div className="row mt-4">
                    <div className="mt-2">
                        <TextField variant="standard" onChange={(e: any) => changeFormData('password', e.target.value)}
                            value={_formData?.password} label="Password" type={_showPassword ? "text" : "password"}
                            fullWidth sx={{ ...textFieldStyle }} error={_validate.password.error} helperText={_validate.password.message}
                            InputProps={{
                                endAdornment: (
                                    <IconButton onClick={() => _setShowPassword(!_showPassword)}>
                                        {_showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                ),
                            }}
                        />
                    </div>
                </div>
            </div>
            <div className='mt-4 d-flex justify-content-end px-3'>
                <LoadingButton type="submit" className="btn btn-block px-4 text-white textTransformNone" endIcon={<></>} variant="contained" loadingPosition="end" loading={_loading}>Login</LoadingButton>
            </div>
        </form>
    </React.Fragment>
}