import React                        from 'react';
import {createTheme, ThemeProvider} from "@mui/material";

interface props {
    children?: React.ReactNode;
}

let theme = createTheme({
    palette   : {
        primary  : {
            main        : '#00AAE7',
            contrastText: '#000000'
        },
        secondary: {
            main        : '#eba832',
            contrastText: '#ffffff'
        },
        action   : {
            disabled: '#ffffff',
        }
    },
    components: {
        MuiButtonBase: {
            defaultProps: {
                disableRipple: true,
            }
        }
    }
});

function AppThemeProvider({children}: props) {
    return (<ThemeProvider theme={theme}>{children}</ThemeProvider>);
}

export default AppThemeProvider;