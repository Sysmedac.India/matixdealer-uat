import React from 'react';
import { Outlet } from 'react-router-dom';

export default function OnBoardingLayout() {

    return <React.Fragment>
        <main>
            <div className="container">
                <div className="onBoardingBase rounded--1 mx-auto marginOnBoard col-md-10">
                    <div className="gridBox">
                        <Outlet />
                    </div>
                </div>
            </div>
        </main>
    </React.Fragment >

}
