import React, { useState } from 'react';
import { NavLink, useLocation, useNavigate } from 'react-router-dom';
import {
    Logout,
    Person2Rounded,
    PersonOutlineOutlined,
} from '@mui/icons-material';
import { useStateValue } from '../../../providers/StateProvider';
import { ROUTES } from '../../../configs/constants';

import 'bootstrap/js/src/collapse';
import 'bootstrap/js/dist/dropdown';
import { IMAGES_ICON } from '../../../assets/images/exportImages';
import { removeLocalStorage } from '../../../services/AuthService';

function DashboardHeader({ pageType = 'dashboard' }: any) {
    const navigate = useNavigate();
    const location = useLocation();
    debugger
    const [{ user }, dispatch]: any = useStateValue();
    const [_drawer, _setDrawer] = useState(false);

    console.log(user)

    const logOutClick = () => {

        dispatch({
            type: "SET_USER",
            user: null
        });
        removeLocalStorage('login');
        navigate(ROUTES.LOGIN);
    }

    const storedTokenString = localStorage.getItem('_token');

    const storedToken = storedTokenString ? JSON.parse(storedTokenString) : null;

    const userId = storedToken?.token?.data?.userid || null;
    const userRoleId = storedToken?.token?.data?.userRoleId || null;

    const userName = storedToken?.token?.data?.userName || null;


    return (<>
        <div className="">
            <div className='container'>
                <div className="d-flex justify-content-between align-items-center">
                    <div className="d-flex flex-wrap justify-content-between align-items-center w-100 py-3 mobJustify">
                        <div className="mx-3">
                            <img src={IMAGES_ICON.Logo} alt="logo" style={{ height: '54px' }} draggable={false} />
                        </div>
                        <div className="">
                            <div className="btn-group align-items-center gap-1">
                                <div className="d-flex flex-column">

                                    <div className="fw-bold text-end">{userName}</div>


                                    {userRoleId === 1 && (
                                        <div className="fs14 text-muted text-end">Admin</div>
                                    )}
                                    {userRoleId === 2 && (
                                        <div className="fs14 text-muted text-end">State Manager</div>
                                    )}
                                    {userRoleId === 3 && (
                                        <div className="fs14 text-muted text-end">Manager</div>
                                    )}
                                    {userRoleId === 4 && (
                                        <div className="fs14 text-muted text-end">Dealer</div>
                                    )}
                                 
                                </div>

                                {user?.profilePictureUrl ? <div className="borderCirclePrimary" role="button" data-bs-toggle="dropdown">
                                    {/* <img draggable={false} src={ROUTES.API.DOWNLOAD_FILE + `?url=${user?.profilePictureUrl}`} alt="icons" className='roundedCircle' height={44} width={44} style={{ objectFit: 'cover' }} /> */}
                                </div> :
                                    <div className="p-2 logdiv" role="button" data-bs-toggle="dropdown">
                                        

                                        {/* <PersonOutlineOutlined className="text-muted" /> */}
                                        <Logout className="muted me-3"  onClick={() => logOutClick()} /><br/>
                                        <div className='logcontent'> <label className='logbutton'>Logout</label></div>
                                        {/* <img draggable={false} src={IMAGES_ICON.whitebackground} alt="icons"  className='roundedCircle rounded' height={44} width={44} style={{ objectFit: 'cover',border:"1px solid" }} /> */}
                                    </div>}
                                <div className="dropdown-menu dropdown-menu-right">
                                    <button className="dropdown-item" type="button"><Person2Rounded className="muted me-3" />Profile</button>
                                    <button className="dropdown-item" type="button" onClick={() => logOutClick()}><Logout className="muted me-3" />Logout</button>
                                </div>
                            </div>
                        </div>
                    </div >
                </div>
            </div >
            <div className="bg-primary w-100 py-2">
                <div className="container">
                    <div className="d-flex flex-wrap justify-content-between align-items-center mobJustify">
                        <NavLink to={ROUTES.DASHBOARD.HOME} className="mx-2 nav-link text-decoration-none textLightBlue" >
                            <div className="px-1 my-2">Dashboard</div>
                        </NavLink>
                        {(user?.accessToken?.data?.userRoleId == 1) && (
                            <NavLink to={ROUTES.DASHBOARD.STATE_MANAGER} className="mx-2 nav-link text-decoration-none textLightBlue" >
                                <div className="px-1 my-2">State Manager</div>
                            </NavLink>)}
                        {((user?.accessToken?.data?.userRoleId == 1) || (user?.accessToken?.data?.userRoleId == 2)) &&
                            <NavLink to={ROUTES.DASHBOARD.MANAGER} className="mx-2 nav-link text-decoration-none textLightBlue" >
                                <div className="px-1 my-2">Manager</div>
                            </NavLink>
                        }
                        {((user?.accessToken?.data?.userRoleId == 1) || (user?.accessToken?.data?.userRoleId == 2) || (user?.accessToken?.data?.userRoleId == 4) || (user?.accessToken?.data?.userRoleId==3)) &&
                            <NavLink to={ROUTES.DASHBOARD.DEALER} className="mx-2 nav-link text-decoration-none textLightBlue" >
                                <div className="px-1 my-2">Dealer</div>
                            </NavLink>}
                        {(user?.accessToken?.data?.userRoleId == 1) &&
                            <NavLink to={ROUTES.DASHBOARD.ANNOUNCEMENT} className="mx-2 nav-link text-decoration-none textLightBlue" >
                                <div className="px-1 my-2">Announcement</div>
                            </NavLink>
                        }
                        {((user?.accessToken?.data?.userRoleId == 1) || (user?.accessToken?.data?.userRoleId == 2) || (user?.accessToken?.data?.userRoleId == 4) || (user?.accessToken?.data?.userRoleId == 3)) &&
                            <NavLink to={ROUTES.DASHBOARD.ACCOUNT_STATEMENT} className="mx-2 nav-link text-decoration-none textLightBlue" >
                                <div className="px-1 my-2">Account Statement</div>
                            </NavLink>}
                        {((user?.accessToken?.data?.userRoleId == 1) || (user?.accessToken?.data?.userRoleId == 2) || (user?.accessToken?.data?.userRoleId == 4) || (user?.accessToken?.data?.userRoleId == 3)) &&
                            <NavLink to={ROUTES.DASHBOARD.DOCUMENT_ACCESS} className="mx-2 nav-link text-decoration-none textLightBlue" >
                                <div className="px-1 my-2">Document Access</div>
                            </NavLink>}
                        {((user?.accessToken?.data?.userRoleId == 1) || (user?.accessToken?.data?.userRoleId == 2) || (user?.accessToken?.data?.userRoleId == 4) || (user?.accessToken?.data?.userRoleId == 3)) &&
                            <NavLink to={ROUTES.DASHBOARD.SALES} className="mx-2 nav-link text-decoration-none textLightBlue" >
                                <div className="px-1 my-2">Sales</div>
                            </NavLink>}

                    </div>
                </div>
            </div>
        </div>
    </>);
}

export default DashboardHeader;
