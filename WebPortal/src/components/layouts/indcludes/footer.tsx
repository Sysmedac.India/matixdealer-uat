
import { Link, useNavigate } from 'react-router-dom';
import logo from "../../../assets/images/familyLogo.png";
import { ROUTES } from '../../../configs/constants'
import '../style.module.scss';

export default function Footer() {
    const navigate = useNavigate();
    return (<>
        <div className='bgFooter'>
            <div className='container py-4'>
                <div className='row justify-content-between footerFont'>
                    <div className='col-md-4 text-start'>
                        <div className=' my-2' role="button" onClick={() => navigate(ROUTES.HOME)}>
                            <img src={logo} alt="Logo" height="150" style={{ marginLeft: "-20px" }} />
                        </div>
                        <div className='text-start text-muted w-75'><small></small></div>
                        <div className="my-2 footerBold fs16">Follow us</div>
                        <div className='mt-3'>
                            <i className='smIcons fa-brands fa-facebook' style={{ cursor: 'pointer' }}></i>
                            <i className='ms-3 smIcons fa-brands fa-twitter' style={{ cursor: 'pointer' }}></i>
                            <i className='ms-3 smIcons fa-brands fa-instagram' style={{ cursor: 'pointer' }}></i>
                            <i className='ms-3 smIcons fa-brands fa-youtube' style={{ cursor: 'pointer' }}></i>
                            <i className='ms-3 smIcons fa-brands fa-linkedin-in' style={{ cursor: 'pointer' }}></i>
                        </div>
                    </div>
                    <div className="row col-md-8">
                        <div className='col-md-4 text-start'>
                            <ul className='list-unstyled mt-5'>
                                <li className='mt-1 footerBold fs16'>Work with us</li>
                                <li className='mt-3'>Sign Up</li>
                                <li className='mt-2'>Affiliate Program</li>
                            </ul>
                            <ul className='list-unstyled mt-5'>
                                <li className='mt-1 footerBold fs16'>Hosting</li>
                                <li className='mt-3'>Family Hivez your home</li>
                                <li className='mt-2'>Explore hosting resources</li>
                                <li className='mt-2'>Visit our community forum</li>
                                <li className='mt-2'>How to host responsibly</li>
                            </ul>
                        </div>
                        <div className='col-md-4 text-start'>
                            <ul className='list-unstyled mt-5'>
                                <li className='mt-1 footerBold fs16'>About us</li>
                                <li className='mt-3'>Home</li>
                                <li className='mt-2'>Booking Guarantee</li>
                                <li className='mt-2'>Press</li>
                                <li className='mt-2'>Careers</li>
                            </ul>
                            <ul className='list-unstyled mt-5'>
                                <li className='mt-1 footerBold fs16'>Accommodation</li>
                                <li className='mt-3'>Hostels</li>
                                <li className='mt-2'>Bed and Breakfast</li>
                            </ul>
                        </div>
                        <div className='col-md-4 text-start'>
                            <ul className='list-unstyled mt-5'>
                                <li className='mt-1 footerBold fs16'>Quick links</li>
                                <li className='mt-3'>Blog</li>
                                <li className='mt-2'>Booking Guarantee</li>
                                <li className='mt-2'>Hostel Awards</li>
                                <li className='mt-2'>Team</li>
                            </ul>
                            <ul className='list-unstyled mt-5'>
                                <li className='mt-1 footerBold fs16'>Customer support</li>
                                <li className='mt-3'>Talk to Us</li>
                                <li className='mt-2'>Help</li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <div className="footerBorder py-3">
                <div className="container">
                    <div className="row justify-content-between footerFont">
                        <div className="col-md-4">
                            <div className='mt-2'>Copyright &#169;2023  Matix | All rights reserved</div>
                        </div>
                        <div className="col-md-4">
                            <div className='mt-2'><b role="button">Privacy Policy</b> | <b role="button">Terms and Conditions</b></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>)
}
