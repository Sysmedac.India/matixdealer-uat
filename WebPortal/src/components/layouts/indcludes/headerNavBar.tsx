import React, { useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import { Drawer, IconButton } from '@mui/material';
import {
    Close,
    Home,
    ListAlt,
    Logout,
    MenuOutlined,
    HelpOutline,
    Diversity3,
    LocalOffer,
    SettingsAccessibility,
    Person2Rounded,
    Input,
    HowToReg,
} from '@mui/icons-material';
import { useStateValue } from '../../../providers/StateProvider';
import logo from "../../../assets/images/familyLogo.png";
import india from "../../../assets/images/india.png";
import { ROUTES } from '../../../configs/constants';

import 'bootstrap/js/src/collapse';
import 'bootstrap/js/dist/dropdown';

function HeaderNavBar() {
    const navigate = useNavigate();
    const [{ user }]: any = useStateValue();
    const [_drawer, _setDrawer] = useState(false);

    return (<>
        <div className=" headerBody">
            <div className='container'>
                <div className="d-flex justify-content-between align-items-center">
                    <div className="d-flex justify-content-between align-items-center w-100">
                        <div className="mx-3"><img src={logo} alt="logo" style={{ height: '100px' }} draggable={false} /></div>
                        <div className="d-none d-md-block">
                            <div className="d-flex flex-wrap ">
                                <NavLink to="/join-our-family" className="mx-2 nav-link" >
                                    <div className="px-3">Join our family</div>
                                </NavLink>
                                <NavLink to="/list-your-property" className="mx-2 nav-link" >
                                    <div className="px-3">List your property</div>
                                </NavLink>
                                <NavLink to="/hot-deals" className="mx-2 nav-link" >
                                    <div className="px-3">Hot Deals</div>
                                </NavLink>
                                <NavLink to="/about-us" className="mx-2 nav-link" >
                                    <div className="px-3">About Us</div>
                                </NavLink >
                                <NavLink to="/help" className="mx-2 nav-link" >
                                    <div className="px-3">Help</div>
                                </ NavLink>
                            </div>
                        </div>
                        <div className="d-none d-md-block">
                            <div className="d-flex flex-end justify-content-between align-items-center">
                                <div className="d-flex align-items-center text-nowrap">
                                    <img src={india} alt="Flag" height="18" draggable={false} />
                                    <div className='ms-2'>INR | ENG</div>
                                </div>
                                <div className="mx-3"></div>

                                <div className="btn-group">
                                    <div className="d-flex border rounded px-2 py-1" role="button" data-bs-toggle="dropdown">
                                        <Person2Rounded />
                                        <span className='text-nowrap'>Login <i className="ms-2 fa-solid fa-chevron-down"></i></span>
                                    </div>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <button className="dropdown-item" type="button">Login</button>
                                        <button className="dropdown-item" type="button" onClick={() => navigate(ROUTES.HOME)}>Register</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div >
                    <div onClick={() => _setDrawer(true)} className={"d-flex justify-content-center align-items-center d-block d-md-none"}>
                        <div><MenuOutlined className='me-2' style={{ fontSize: "40px" }} /></div>
                        <div style={{ fontSize: "28px" }}>Menu</div>
                    </div>
                </div>
            </div >
        </div>
        <Drawer anchor={'top'} open={_drawer} onClose={() => _setDrawer(false)} PaperProps={{ sx: { width: "100%" } }} disableScrollLock={true} >
            <div className="p-1">
                <IconButton className="p-0 m-0" sx={{ mx: '4px', color: "red" }} onClick={() => _setDrawer(false)}>
                    <Close />
                </IconButton>
                <div className="mt-3 row justify-content-center mx-auto">
                    {/* <div className='text-dark '><Person /> {user?.petOwnerName || '-'}</div> */}
                    <NavLink className="text-dark my-2 text-decoration-none" to="/home" onClick={() => _setDrawer(false)}><div><Home /> <span className="ms-3">Home</span> </div></NavLink>
                    <NavLink className="text-dark my-2 text-decoration-none" to="/join-out-family" onClick={() => _setDrawer(false)}><Diversity3 /><span className="ms-3">Join our family</span></NavLink>
                    <NavLink className="text-dark my-2 text-decoration-none" to="/list-your-property" onClick={() => _setDrawer(false)}><ListAlt /><span className="ms-3">List your property</span></NavLink>
                    <NavLink className="text-dark my-2 text-decoration-none" to="/hot-deals" onClick={() => _setDrawer(false)}><LocalOffer /><span className="ms-3">Hot Deals</span></NavLink>
                    <NavLink className="text-dark my-2 text-decoration-none" to="/about-us" onClick={() => _setDrawer(false)}><SettingsAccessibility /><span className="ms-3">About Us</span></NavLink>
                    <NavLink className="text-dark my-2 text-decoration-none" to="/help" onClick={() => _setDrawer(false)}><HelpOutline /><span className="ms-3">Help</span></NavLink>
                    <NavLink className="text-dark my-2 text-decoration-none" to="/login" onClick={() => _setDrawer(false)}><Input /><span className="ms-3">Login</span></NavLink>
                    <NavLink className="text-dark my-2 text-decoration-none" to={ROUTES.HOME} onClick={() => _setDrawer(false)}><HowToReg /><span className="ms-3">Registration</span></NavLink>
                </div>
            </div>
        </Drawer>
    </>);
}

export default HeaderNavBar;
