import { Suspense, useEffect } from 'react';
import RouteApp from "../../routes/RouteApp";
import LoaderControl from "../helpers/loaderControl";
import HeaderNavBar from './indcludes/headerNavBar';
import Footer from './indcludes/footer';
import { ROUTES } from '../../configs/constants';
import { Outlet, useNavigate } from 'react-router-dom';
import { useStateValue } from '../../providers/StateProvider';

export default function WebLayout() {
    const navigate = useNavigate();
    const [{ user }]: any = useStateValue();

    useEffect(() => {
        if (!user) {
            navigate(ROUTES.HOME);
        }
    }, []);

    return (<>
        <HeaderNavBar />
        <main>
            <div className='container'>
                <Outlet />
            </div>
        </main>
        <Footer />

    </>);
}