import React from 'react'
import { Outlet } from 'react-router-dom'
import { CssBaseline } from '@mui/material'
import DashboardHeader from './indcludes/dashboardHeader'

export default function DashboardLayout() {
    return <React.Fragment>
        <CssBaseline />
        <DashboardHeader />
        {/* <div className="" > */}
            <main className='vh-100'>
                <Outlet />
            </main>
        {/* </div> */}
    </React.Fragment >
}
