import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import LinearProgress, { linearProgressClasses } from '@mui/material/LinearProgress';

const BorderLinearProgress = styled(LinearProgress)(() => ({
    height: 4,
    [`&.${linearProgressClasses.colorPrimary}`]: {
        backgroundColor: '#dcdcdc',
    },
    [`& .${linearProgressClasses.bar}`]: {
        backgroundColor: '#eba832',
    },
}));

export default function CustomizedProgressBars({ progressValue }: any) {
    return (
        <Box sx={{ width: '100%' }}>
            <BorderLinearProgress variant="determinate" value={progressValue} />
        </Box>
    );
}