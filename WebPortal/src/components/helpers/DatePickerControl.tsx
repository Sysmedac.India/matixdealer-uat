import React, { useEffect, useState } from 'react';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { textFieldStyle } from '../../services/HelperService';

interface props {
    label: string;
    active?: boolean;
    onchange?(date: Date | null): void | null;
    value?: Date | null;
    minDate?: any | null;
    maxDate?: any | null;
    error?: boolean;
    errorMsg?: string;
}

function DatePickerControl({
    label, active = true, onchange, value = null,
    minDate = null, maxDate = null, error = false, errorMsg = ''
}: props) {

    const [_validation, _setValidation] = useState({ error: error, errorMsg: errorMsg });
    const updateValue = (newValue: Date | null) => {
        if (onchange) {
            onchange(newValue);
        }
    }

    useEffect(() => _setValidation({ error: error, errorMsg: errorMsg }), [error, errorMsg]);

    return (<>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DatePicker className='w-100 bg-white'
                label={label}
                minDate={minDate}
                maxDate={maxDate}
                value={value}
            
                onChange={(value: any) => updateValue(value)}
                format="DD/MM/YYYY"
                slotProps={{
                    textField: {
                        size : "small",
                        style: {color:"blue"},
                        error: _validation.error,
                        helperText: _validation.errorMsg,
                    },
                }}
            />
        </LocalizationProvider>
    </>);
}

export default DatePickerControl;