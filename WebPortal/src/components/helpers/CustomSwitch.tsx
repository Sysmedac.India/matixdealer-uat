// import { DarkMode } from '@mui/icons-material';
import { Switch, styled } from '@mui/material';


export default function CustomSwitch({ checked, switchHandle }: any) {
    return <>
        <MaterialUISwitch checked={!checked} onChange={(e: any) => switchHandle(!e.target.checked)} />
    </>
}

const MaterialUISwitch = styled(Switch)(() => ({
    width: 70,
    height: 34,
    borderRadius: '50rem',
    padding: 7,
    '& .MuiSwitch-switchBase': {
        margin: 1,
        padding: 0,
        transform: 'translateX(4px)',
        '&.Mui-checked': {
            color: '#fff',
            transform: 'translateX(35px)',
            '& .MuiSwitch-thumb:before': {
                backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="14" width="14" viewBox="0 0 24 20"><path fill="${encodeURIComponent('#fc5314',)}" stroke="${encodeURIComponent('#fc5314',)}" stroke-width="1" d="M18.3 5.71a.9959.9959 0 0 0-1.41 0L12 10.59 7.11 5.7a.9959.9959 0 0 0-1.41 0c-.39.39-.39 1.02 0 1.41L10.59 12 5.7 16.89c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0L12 13.41l4.89 4.89c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L13.41 12l4.89-4.89c.38-.38.38-1.02 0-1.4z"/></svg>')`,
            },
            '& + .MuiSwitch-track': {
                opacity: 1,
                backgroundColor: '#fc5314',
            },
        },
    },
    '& .MuiSwitch-thumb': {
        backgroundColor: 'white',
        width: 28,
        height: 28,
        '&:before': {
            content: "''",
            position: 'absolute',
            width: '100%',
            height: '100%',
            left: 0,
            top: 0,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="14" width="14" viewBox="0 0 24 20"><path fill="${encodeURIComponent('#11b8b8',)}" stroke="${encodeURIComponent('#11b8b8',)}" stroke-width="1" d="M9 16.17 5.53 12.7a.9959.9959 0 0 0-1.41 0c-.39.39-.39 1.02 0 1.41l4.18 4.18c.39.39 1.02.39 1.41 0L20.29 7.71c.39-.39.39-1.02 0-1.41a.9959.9959 0 0 0-1.41 0L9 16.17z"/></svg>')`,
        },
    },
    '& .MuiSwitch-track': {
        opacity: 1,
        backgroundColor: '#11b8b8',
        borderRadius: 20 / 2,

        '&:before, &:after': {
            content: '""',
            position: 'absolute',
            top: '54%',
            transform: 'translateY(-50%)',
            width: 44,
            height: 26,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
        },
        '&:before': {
            backgroundImage: `url('data:image/svg+xml;charset=UTF-8,<svg xmlns="http://www.w3.org/2000/svg" width="34" height="20" viewBox="0 0 100 40"><text x="10" y="25" font-family="Epilogue, sans-serif" font-size="26" fill="white">No</text></svg>')`,
            left: 12,
        },
        '&:after': {
            backgroundImage: `url('data:image/svg+xml;charset=UTF-8,<svg xmlns="http://www.w3.org/2000/svg" width="34" height="20" viewBox="0 0 100 40"><text x="10" y="25" font-family="Epilogue, sans-serif" font-size="26" fill="white">Yes</text></svg>')`,
            right: -6,

        },
    },
}));