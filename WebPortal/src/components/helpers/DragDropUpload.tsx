import React, { MutableRefObject, useRef, useState } from 'react';
import { LoadingButton } from '@mui/lab';

export default function DragDropUpload({ fileName, sendUploadFiles, labelName, multiFile = false, acceptFileType = '', loadingButton = false, error = false, errorMsg = '' }: any) {
    const refDocument = useRef() as MutableRefObject<HTMLInputElement>;
    const [_dragOver, _setDragOver] = useState(false);

    const handleDragOver = (e: any) => {
        e.preventDefault();
        _setDragOver(true);
    };

    const handleDragLeave = () => {
        _setDragOver(false);
    };

    const sendFiles = (files: any) => {
        sendUploadFiles(files)
    }


    const handleDrop = (e: any) => {
        e.preventDefault();
        _setDragOver(false);

        const droppedImage = e.dataTransfer.files;
        if (droppedImage?.length) {
            sendFiles(droppedImage);
        }
    };

    // function for document upload
    const onUpload = (e: any) => {
        e.preventDefault();
        sendFiles(e.target.files);
    }

    return <React.Fragment>
        <div className={`fieldBorder rounded d-flex py-1 px-3 align-items-center justify-content-between ${_dragOver ? 'drag-over' : ''} ${error ? 'redBorder' : ''}`} onDrop={handleDrop} onDragOver={handleDragOver} onDragLeave={handleDragLeave}>
            <div className='d-flex gap-3 align-items-center text-nowrap text-truncate'>
                {/* <img src={IMAGES_ICON.UploadIcon} alt="icons" height={20} /> */}
                {!fileName ?
                    <div className='text-muted'>{labelName}</div> :
                    <div className='text-muted'>{fileName}</div>
                }
            </div>
            <div>
                <LoadingButton className="transformNone fw400 bg-success text-white" size="small" component="label" color="secondary" loading={loadingButton} loadingPosition="end" endIcon={<></>}>
                    <span className='text-nowrap'>Browse</span>
                    <input type="file" ref={refDocument} onChange={(e: any) => onUpload(e)} hidden multiple={multiFile} accept={acceptFileType} />
                </LoadingButton>
            </div>
        </div>
        {error && <div className="fs12 errorClr ms-3 mt-1">{errorMsg}</div>}

    </React.Fragment>
}