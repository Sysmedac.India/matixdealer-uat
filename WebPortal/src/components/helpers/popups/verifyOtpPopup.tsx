import React, { useEffect, useState } from 'react'
import { LoadingButton } from '@mui/lab';
import { CloseRounded } from '@mui/icons-material';
import { Dialog, DialogContent } from '@mui/material';
import './style.scss'

export function VerifyOtpPopup({ msg, changeFlag, _loadingOtp, validateOtp, validation }: any) {
    const [_flag, _setFlag] = useState(true);
    const [_otp, _setOtp] = useState<any>('');
    const [_digit1, _setDigit1] = useState<any>('');
    const [_digit2, _setDigit2] = useState<any>('');
    const [_digit3, _setDigit3] = useState<any>('');
    const [_digit4, _setDigit4] = useState<any>('');
    const [_digit5, _setDigit5] = useState<any>('');
    const [_digit6, _setDigit6] = useState<any>('');
    const [_otpArr, _setOtpArr] = useState<any>(['', '', '', '', '', '']);

    const handleClose = () => {
        _setFlag(false);
        changeFlag();
    }
    const verifyOtp = () => {
        const OTP = (_digit1 || '') + (_digit2 || '') + (_digit3 || '') + (_digit4 || '') + (_digit5 || '') + (_digit6 || '')
        validateOtp(OTP)
    }


    function OTPInput() {
        const inputs: any = document.querySelectorAll('#otp > *[id]');
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].addEventListener('keydown', function (event: any) {
                event.preventDefault();
                if (event.key === "Backspace") {
                    inputs[i].value = '';
                    if (i !== 0)
                        inputs[i - 1].focus();
                } else if (event.keyCode === 9) {
                    if (i !== 3)
                        inputs[i + 1].focus();
                } else {
                    if (i === inputs.length - 1 && inputs[i].value !== '') {
                        return true;
                    } else if (event.keyCode > 47 && event.keyCode < 58) {
                        event.preventDefault();
                        if (i === 0) {
                            _setDigit1(event.key)
                        }
                        if (i === 1) {
                            _setDigit2(event.key)
                        }
                        if (i === 2) {
                            _setDigit3(event.key)
                        }
                        if (i === 3) {
                            _setDigit4(event.key)
                        }
                        if (i === 4) {
                            _setDigit5(event.key)
                        }
                        if (i === 5) {
                            _setDigit6(event.key)
                        }

                        inputs[i].value = event.key;
                        if (i !== inputs.length - 1) {
                            inputs[i + 1].focus();
                        }
                    } else if (event.keyCode > 64 && event.keyCode < 91) {
                        event.preventDefault();
                        return true;
                    }
                }
            });
        }
    }

    useEffect(() => {
        setTimeout(() => {
            OTPInput();
        }, 500);
    }, [])



    return (<>
        <Dialog open={_flag} onClose={handleClose}>
            <div className="d-flex justify-content-end">
                <div className="triangle_top_right"><CloseRounded style={{
                    color: 'white',
                    position: 'absolute', right: '10px', top: '10px'
                }} role="button" onClick={handleClose} /></div>
            </div>
            <DialogContent className='col-10 mx-auto'>
                <div className="text-center fw500">Please enter the One-Time password to verify your account</div>
                <div className="text-center text-muted my-4 fs14">A One-Time password has been sent to <br/> {msg}</div>
                <div className='row justify-content-center'>
                    <div className="d-flex justify-content-center align-items-center">
                        <div className="position-relative">
                            <div className="p-2 text-center">
                                <div id="otp" className="inputs d-flex flex-row justify-content-center mt-2">
                                    <input className="m-2 text-center form-control rounded" type="text" id="first" maxLength={1} />
                                    <input className="m-2 text-center form-control rounded" type="text" id="second" maxLength={1} />
                                    <input className="m-2 text-center form-control rounded" type="text" id="third" maxLength={1} />
                                    <input className="m-2 text-center form-control rounded" type="text" id="fourth" maxLength={1} />
                                    <input className="m-2 text-center form-control rounded" type="text" id="fifth" maxLength={1} />
                                    <input className="m-2 text-center form-control rounded" type="text" id="sixth" maxLength={1} />
                                </div>
                            </div>
                        </div>
                    </div>
                    {validation?.otp?.error && <div className="text-danger text-center">{validation?.otp.message}</div>}

                    <LoadingButton className="transformNone fw400 my-4" endIcon={<></>} variant="contained" loadingPosition="end" color="secondary" style={{ width: '8rem' }}
                        loading={_loadingOtp} onClick={verifyOtp}>Validate</LoadingButton>
                    <div className="linkCreate text-center fw500 mb-4" role="button" style={{ fontSize: '14px' }}>Resend One-Time password</div>
                </div>
            </DialogContent>
        </Dialog >
    </>
    )
}


export function PopupSkeleton({width, closePopup, flag, component, title }: any) {
    const handleClose = () => {
        closePopup();
    }

    return (<>
        <Dialog fullWidth maxWidth={width} open={flag} onClose={handleClose} scroll='body'>
            <div className="d-flex justify-content-end">
                <div className="triangle_top_right"><CloseRounded style={{
                    color: 'white',
                    position: 'absolute', right: '10px', top: '10px'
                }} role="button" onClick={handleClose} /></div>
            </div>
            <div className="fw500 text-center" style={{ marginTop: '-44px' }}>{title}</div>
            <DialogContent className='col-10 mx-auto'>
                {component}
            </DialogContent>
        </Dialog >
    </>
    )
}
