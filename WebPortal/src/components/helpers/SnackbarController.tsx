import React, { useEffect, useState } from 'react';
import { Snackbar } from '@mui/material';
import { AlertColor } from '@mui/material/Alert/Alert';
import { CancelOutlined, CheckCircleOutlineOutlined } from '@mui/icons-material';

interface props {
    severity?: AlertColor;
    message?: string;
}

export default function SnackbarController({ severity = "success", message = "" }: props) {
    const [_open, _setOpen] = useState(false);
    const [_message, _setMessage] = useState('');

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        _setMessage('');
        _setOpen(false);
    };

    useEffect(() => {
        _setMessage(message);
        _setOpen(message !== '');
    }, [message]);

    return (
        <Snackbar className='snackBar' open={_open} onClose={handleClose} anchorOrigin={{ horizontal: "center", vertical: "top" }} autoHideDuration={3000} sx={{ minWidth: '100vw', borderRadius: 'none' }}>
            <div className="d-flex align-items-center justify-content-between w-100 p-3">
                <div className="d-flex gap-2 align-items-center">
                    <CheckCircleOutlineOutlined className='text-success' />
                    <div className="text-white fs14">{_message}</div>
                </div>
                <div role="button" onClick={handleClose}><CancelOutlined className='text-white'/></div>
            </div>
        </Snackbar>
    );
}