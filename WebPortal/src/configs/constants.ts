const baseRoute = '/';
export let baseApi = 'http://matixapi.sysmedac.com/api/';
if (process.env.NODE_ENV === "production") {
    baseApi = "http://matixapi.sysmedac.com/api/";
}
 
export const ROUTES = {
    // HOME: baseRoute,
    HOME: baseRoute + '',
    LOGIN: baseRoute + '',
    DASHBOARD: {
        HOME: baseRoute + 'dashboard',
        STATE_MANAGER: baseRoute + 'dashboard/state-manager',
        MANAGER: baseRoute + 'dashboard/manager',
        DEALER: baseRoute + 'dashboard/dealer',
        ANNOUNCEMENT: baseRoute + 'dashboard/announcement',
        ACCOUNT_STATEMENT: baseRoute + 'dashboard/account-statement',
        DOCUMENT_ACCESS: baseRoute + 'dashboard/document-access',
        SALES: baseRoute + 'dashboard/sales',
      //  MANAGEMENT : baseRoute + 'managment',
    },

    //API
    api: {
        userLogin : baseApi + 'Matix/LoginUser',
        gridBindStateManager : baseApi +'Matix/GetAll_Users_Details',
        getAllstatemanagername : baseApi +'Matix/GetAllManagers_ByUserTeams',
        deleteManager:baseApi+'Matix/Delete_Multiple_User',
        GetUsersByUserTeams:baseApi+'Matix/GetAllUserDetailsBy_UserTeams',
        GetDealersDetails:baseApi+'Matix/GetAllDealersBy_UserTeams',
        gridBindInvoiceDetails : baseApi +'Matix/Get_InvoiceDetails_ForDealers',
        getAllAnnouncement : baseApi +'Matix/GetAllAnnouncement',
        assignManager:baseApi+'Matix/AssighManager',
        createManager : baseApi + 'Matix/InsertOrUpdateUser',
        getManager : baseApi + 'Matix/Get_User_ByUserId',
        activeManager : baseApi + 'Matix/UpdateUserStatus',
        getAllDealers : baseApi + 'Matix/GetAllDealers',
        getdashboardDetails : baseApi + 'Matix/DashBoard',
        addAnnouncement : baseApi + 'Matix/InsertOrUpdateAnnouncement',
        insertDocumentAccess : baseApi + 'Matix/InsertOrUpdateDocuments',
        commonUpload : baseApi + 'Matix/CommonFileUpload',
        getAllDocuments : baseApi + 'Matix/GetAll_Document_Details',
        getsales:baseApi+'Matix/GetAllSalesDetails',
        getDesignation : baseApi + 'Matix/GetDesignationDetail',
        getAllAssignedUsersById: baseApi + "Matix/GetAllAssignedUsersById",
        getscrollannouncement : baseApi + "Matix/GetAnnouncementMessage",
        GetAllSalesDetailsByMaterialGroup : baseApi + "Matix/GetAllSalesDetailsByMaterialGroup",
        DownloadInvoice:baseApi+"Matix/downloadInvoice",
    }
};

