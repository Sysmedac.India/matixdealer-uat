import Logo from './logo-m.png';
import Profile from './Profile.png';
import LoginBackground from './Login Background.png';
import DocumentDownload from './Document Download.png';
import AddDocument from './Add Document.png';
import RemoveDocument from './Document Remove.png';
import Assign from './Assign.png';
import Remove from './Remove.png';
import Edit from './Edit.png';
import CalenderIcon from './Calender icon.png';
import AccountStatement from './Account Statement.png';
import DocumentAccess from './Document Access.png';
import Sales from './Sales.png';
import Announcement from './Annoiuncement.png';
import Dealers from './Dealers.png';
import StateManagers from './State Managers.png';
import ExportExcel from './Export Excel.png';
import whitebackground from './whitebackground.jpg';

export const IMAGES_ICON = {
    Logo, Profile,whitebackground, LoginBackground, DocumentDownload, AddDocument, RemoveDocument, Assign, Remove, Edit, CalenderIcon, AccountStatement, DocumentAccess,
    Sales, Announcement, Dealers, StateManagers, ExportExcel,
}